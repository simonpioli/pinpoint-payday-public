<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{applicant?}', 'ApplicationController@startApplicant')
    ->name('application.applicant.get');

Route::post('/submit-applicant', 'ApplicationController@createApplicant')
    ->name('application.applicant.post');

Route::get('/identity/{applicant}', 'ApplicationController@startIdentityCheck')
    ->name('application.identity.get');

Route::post('/identity', 'ApplicationController@postIdentity')
    ->name('application.identity.post');

Route::get('/lenders/{applicant}', 'ApplicationController@startLenders')
    ->name('application.lenders.get');

Route::post('/submit-lenders', 'ApplicationController@populateApplicantLenders')
    ->name('application.lenders.post');

Route::get('confirmation/{applicant}', 'ApplicationController@getConfirmation')
    ->name('application.confirmation.get');

Route::post('confirmation', 'ApplicationController@postConfirmation')
    ->name('application.confirmation.post');

Route::get('/documents/{applicant}', 'ApplicationController@startDocuments')
    ->name('application.documents.get');

Route::post('/thank-you', 'ApplicationController@signForm')
    ->name('application.documents.post');

Route::post('/hs-callback', function () {
    return response('Hello API Event Received', 200)
        ->header('Content-Type', 'text/plain');
});
