<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/address-lookup/{postcode}', 'AddressLookupController')
    ->name('api.address.lookup');

Route::get('/document/{applicantId}', 'DocumentSignatureController')
    ->name('api.document.fetch');

Route::get('/identity/{applicantId}', 'IdentityCheckController')
    ->name('api.identity.fetch');
