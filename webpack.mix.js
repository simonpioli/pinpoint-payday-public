const mix = require('laravel-mix');

require('laravel-mix-imagemin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('html');

mix
    .js('resources/js/app.js', 'js')
    .sass('resources/sass/app.scss', 'css')
    .sass('resources/sass/documents.scss', '../resources/views/css/documents.blade.php')
    .copy('resources/img/**/**.*', mix.config.publicPath + '/img')
    .imagemin(
        mix.config.publicPath + 'img/**/**.*',
        {
            context: 'resources',
        },
        {
            optipng: {
                optimizationLevel: 5
            },
            jpegtran: null,
            plugins: [
                require('imagemin-mozjpeg')({
                    quality: 100,
                    progressive: true,
                }),
            ],
        }
    );

if (mix.inProduction()) {
    mix.disableNotifications()
        .version();
} else {
    mix.sourceMaps()
        .browserSync({
            proxy: 'https://pinpoint-payday.sp',
            files: ['html/css/**/*.css', 'html/js/**/*.js']
        });
}
