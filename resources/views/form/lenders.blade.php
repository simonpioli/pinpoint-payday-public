@extends('layouts.app')

@section('content')
    <form class="container form-lenders" action="{{ route('application.lenders.post') }}" method="post">
        {{ csrf_field() }}
        @if(count($errors) > 0)
            <div class="row justify-content-center form-section">
                <div class="col-12">
                    <div class="card border-danger">
                        <section class="card-body">
                            <p>Please review your your responses and correct the following:</p>
                            <ul>
                                @foreach($errors->all() as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        </section>
                    </div>
                </div>
            </div>
        @endif
        <lenders-form
            questions="{{ $questions->toJson() }}"
            lender-types="{{ $lenderTypes->toJson() }}"
            form-data="{{ $formData }}"
        >
        </lenders-form>
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <section class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <button class="btn btn-success">Next</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </form>
@endsection
