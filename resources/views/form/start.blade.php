@extends('layouts.app')

@section('content')
    @php
    if (!empty($formData)) {
        Form::fill($formData);
    }
    @endphp
    {!! Form::open()->route('application.applicant.post')->post()->attrs(['class' => 'container needs-validation']) !!}
        <input type="hidden" name="id" value="{{ $applicantId ? $applicantId : '' }}">
        @if(count($errors) > 0)
            <div class="row justify-content-center form-section">
                <div class="col-12">
                    <div class="card border-danger">
                        <section class="card-body">
                            <p>Please review your your responses and correct the following:</p>
                            <ul>
                                @foreach($errors->all() as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        </section>
                    </div>
                </div>
            </div>
        @endif
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <h3 class="card-header">Personal Details</h3>
                    <section class="card-body">
                        <div class="row">
                            {!! Form::select('title', 'Title', [
                                    'Mr' => 'Mr',
                                    'Mrs' => 'Mrs',
                                    'Miss' => 'Miss',
                                    'Ms' => 'Ms',
                                    'Dr' => 'Dr'
                                ])->wrapperAttrs(['class' => 'col-md-4']) !!}
                            {!! Form::text('first_name', 'First Name')->wrapperAttrs(['class' => 'col-md-4']) !!}
                            {!! Form::text('last_name', 'Last Name')->wrapperAttrs(['class' => 'col-md-4']) !!}
                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label for="date_of_birth">Date of Birth</label>
                                <div class="form-row">
                                    <div class="col-3">
                                        <select name="date_of_birth[day]"
                                                class="form-control{{ $errors->has('date_of_birth.day') ? ' is-invalid' : '' }}">
                                            <option value="">Day</option>
                                            @for ($i = 1; $i < 32; $i++)
                                                <option value="{{ $i }}" {{ old('date_of_birth.day') == "$i" || (isset($formData['date_of_birth']['day']) && $formData['date_of_birth']['day'] === "$i") ? ' selected' : '' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <select name="date_of_birth[month]"
                                                id="date_of_birth-month"
                                                class="form-control{{ $errors->has('date_of_birth.month') ? ' is-invalid' : '' }}">
                                            <option value="">Month</option>
                                            <option value="1" {{ old('date_of_birth.month') === '1' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '1') ? ' selected' : '' }}>January</option>
                                            <option value="2" {{ old('date_of_birth.month') === '2' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '2') ? ' selected' : '' }}>February</option>
                                            <option value="3" {{ old('date_of_birth.month') === '3' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '3') ? ' selected' : '' }}>March</option>
                                            <option value="4" {{ old('date_of_birth.month') === '4' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '4') ? ' selected' : '' }}>April</option>
                                            <option value="5" {{ old('date_of_birth.month') === '5' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '5') ? ' selected' : '' }}>May</option>
                                            <option value="6" {{ old('date_of_birth.month') === '6' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '6') ? ' selected' : '' }}>June</option>
                                            <option value="7" {{ old('date_of_birth.month') === '7' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '7') ? ' selected' : '' }}>July</option>
                                            <option value="8" {{ old('date_of_birth.month') === '8' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '8') ? ' selected' : '' }}>August</option>
                                            <option value="9" {{ old('date_of_birth.month') === '9' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '9') ? ' selected' : '' }}>September</option>
                                            <option value="10" {{ old('date_of_birth.month') === '10' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '10') ? ' selected' : '' }}>October</option>
                                            <option value="11" {{ old('date_of_birth.month') === '11' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '11') ? ' selected' : '' }}>November</option>
                                            <option value="12" {{ old('date_of_birth.month') === '12' || (isset($formData['date_of_birth']['month']) && $formData['date_of_birth']['month'] === '12') ? ' selected' : '' }}>December</option>
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <select name="date_of_birth[year]"
                                                class="form-control{{ $errors->has('date_of_birth.year') ? ' is-invalid' : '' }}">
                                            <option value="">Year</option>
                                            @foreach($formYears as $year)
                                                <option value="{{ $year }}" {{ old('date_of_birth.year') == $year || (isset($formData['date_of_birth']['year']) && $formData['date_of_birth']['year'] == $year) ? ' selected' : '' }}>{{ $year }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <h3 class="card-header">Contact Details</h3>
                    <section class="card-body">
                        <div class="row">
                            {!! Form::text('email', 'Email Address')->wrapperAttrs(['class' => 'col-md-6'])->type('email') !!}
                            {!! Form::text('telephone', 'Telephone Number')->wrapperAttrs(['class' => 'col-md-6'])->type('tel') !!}
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <h3 class="card-header">Current Address</h3>
                    <section class="card-body">
                        <address-lookup name-prefix="primary-address" form-data="{{ json_encode($formData) }}"></address-lookup>
                    </section>
                </div>
            </div>
        </div>
        <previous-addresses form-data="{{ json_encode($formData) }}"></previous-addresses>
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <section class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <p>Have you ever been in an IVA or declared bankrupt?</p>
                                {!! Form::radio('1', 'Yes')->inline()->name('iva_bankrupt') !!}
                                {!! Form::radio('0', 'No')->inline()->name('iva_bankrupt') !!}
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <section class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <p>I confirm I have read and agree with the terms of the <a href="https://www.returnmymoney.com/privacy" target="_blank">Privacy Policy</a> and consent to you contacting me to discuss my pay day loan claim(s).</p>
                                {!! Form::checkbox('1', 'Yes')->inline()->name('t_and_c') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">
                                <p>Please untick this box if you do not wish to receive relevant news and updates about our services</p>
                                {!! Form::checkbox(1, 'Yes', null, 1)->inline()->name('marketing') !!}
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <section class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#paModal">Next</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="paModal" aria-labelledby="paModalTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="paModalTitle" class="modal-title">Have you entered all of your previous addresses?</h5>
                    </div>
                    <div class="modal-body">
                        <p>Providing all your previous addresses you lived at when you took out your loans will increase the chances of a successful claim.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Yes, I've provided all addresses</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No, I need to add previous addresses</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection
