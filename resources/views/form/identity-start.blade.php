@extends('layouts.app')

@section('content')
    <form id="identity-form" class="container form-identity" action="{{ route('application.identity.post') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" id="identity_result" name="identity_result" value="">
        <input type="hidden" id="identity_reason" name="identity_reason" value="">
        <identity-check-screen applicant-id="{!! $applicant->id !!}" :attempts={!! $applicant->check_attempts !!}></identity-check-screen>
    </form>
@endsection
