@extends('layouts.app')

@section('post-fb')
    <script>
        fbq('track', 'InitiateCheckout');
    </script>
@endsection

@section('content')
    {!! Form::open()->route('application.confirmation.post')->post()->attrs(['class' => 'container needs-validation']) !!}
        {{ csrf_field() }}
        {!! Form::hidden('confirm', true) !!}
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <h3 class="card-header">Great News!</h3>
                    <section class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p>Your information matches the criteria of previously successful claims. This now becomes a very simple process for yourself, all you need to do from here is sign our T&C’s then let us do the hard work!</p>
                                <p>Our vastly experienced team will communicate with the lender (and where required, the Ombudsman service) on your behalf.  We will be sure to keep you updated with the status of your claim along the way.</p>
                                <p>Please simply ensure the information below is correct to the best of your knowledge.</p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row form-section">
            <div class="col-12 col-md-6">
                <div class="card">
                    <h3 class="card-header">Personal Details</h3>
                    <section class="card-body">
                        <p>{{ $applicant->title }} {{ $applicant->first_name }} {{ $applicant->last_name }}</p>
                        <dl>
                            <dt>Email Address:</dt>
                            <dd>{{ $applicant->email }}</dd>
                            <dt>Telephone:</dt>
                            <dd>{{ $applicant->telephone ? $applicant->telephone : 'Not Supplied' }}</dd>
                        </dl>
                    </section>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card">
                    <h3 class="card-header">Current Address</h3>
                    <section class="card-body">
                        {{ $primaryAddress->flat_no }}<br>
                        {{ $primaryAddress->building_no_name }}<br>
                        {{ $primaryAddress->address_line_1 }}<br>
                        {{ $primaryAddress->address_line_2 }}<br>
                        {{ $primaryAddress->town }}<br>
                        {{ $primaryAddress->postcode }}
                    </section>
                </div>
            </div>
        </div>
        @if($addresses->count() > 0)
            <div class="row justify-content-center form-section">
                <div class="col-12">
                    <div class="card">
                        <h3 class="card-header">Previous Addresses</h3>
                        <section class="card-body">
                            <div class="address-list row">
                                @foreach($addresses as $address)
                                    @if($address->primary == 0)
                                        <div class="card">
                                            <div class="card-body">
                                                {{ $address->flat_no }}<br>
                                                {{ $address->building_no_name }}<br>
                                                {{ $address->address_line_1 }}<br>
                                                {{ $address->address_line_2 }}<br>
                                                {{ $address->town }}<br>
                                                {{ $address->postcode }}
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        @endif

        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <h3 class="card-header">Loans</h3>
                    <section class="card-body">
                        <div class="lenders-list row">
                            @foreach($applicantLenders as $applicantLender)
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card">
                                        <h5 class="card-header bg-light text-dark">{{ $applicantLender->lender->name }}</h5>
                                        <section class="card-body">
                                            <dl class="application-lender-data">
                                                <dt class="application-lender-label">Years Borrowed:</dt>
                                                @if ($applicantLender->last_year != 0)
                                                    <dd>{{ $applicantLender->first_year }} to {{ $applicantLender->last_year }}</dd>
                                                @else
                                                    <dd>{{ $applicantLender->first_year }}</dd>
                                                @endif

                                                <dt class="application-lender-label">Number of Loans:</dt>
                                                <dd>{{ $applicantLender->number_loans }}</dd>

                                                <dt class="application-lender-label">Previously claimed against this lender?</dt>
                                                <dd>{{ $applicantLender->previous_claim ? 'Yes' : 'No' }}</dd>

                                                <dt class="application-lender-label">Circumstances</dt>
                                                <dd>
                                                    <ul class="circumstances">
                                                        @foreach($applicantLender->applicantLenderQuestion as $alq)
                                                            <li>{{ $alq->question->question }}</li>
                                                        @endforeach
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </section>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row justify-content-center form-section">
            <div class="col-12">
                <div class="card">
                    <section class="card-body">
                        <div class="row">
                            <div class="form-group col-12">
                                <button class="btn btn-success">Confirm Details and Continue</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection
