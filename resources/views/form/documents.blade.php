@extends('layouts.app')

@section('content')
    <form id="confirmation-form" class="container form-confirmation" action="{{ route('application.documents.post') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" id="date" name="signed_date" value="{{ $today->format('d/m/Y') }}">
        <input type="hidden" id="signed" name="signature_id" value="">
        <hello-sign-screen applicant-id="{!! $applicant->id !!}"></hello-sign-screen>
    </form>
@endsection
