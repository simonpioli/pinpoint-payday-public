@extends('layouts.app')

@section('post-fb')
    <script>
        fbq(
            'track',
            'Purchase',
            {
                value: {{ $lenders->count() }},
                currency: 'GBP',
                contents: [
                    @foreach($lenders as $lender)
                    {
                        'id': '{{ $lender['id'] }}',
                        'name': '{{ $lender['name'] }}',
                        'quantity': {{ $lender['quantity'] }},
                        'item_price': {{ $lender['item_price'] }}
                    },
                    @endforeach
                ]
            }
        );
    </script>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <h3 class="card-header">Thank you</h3>
                    <section class="card-body">
                        <p>Thank you for submitting your details. You’ll shortly receive a welcome email with more information. In general we will communicate updates on your claim(s) via email; please add <a href="mailto:claims@returnmymoney.com">claims@returnmymoney.com</a> to your ‘whitelist’. A quick note to tell you how it normally works:</p>
                        <ol>
                            <li>We will send a Data Subject Access Request (DSAR) to your lender to get all the facts- the lender(s) get one month to reply to this. The likelihood is the lender will send this to your email address so please check your emails regularly and forward us any correspondence from the lender.</li>
                            <li>We will review your loan history</li>
                            <li>If we believe there is a valid claim we will submit it for you – the lender(s) get 8 weeks to reply to the complaint</li>
                            <li>If we are not happy with the lender response your claim(s) will go to the Financial Ombudsman (FOS) – the FOS can take a long time to review the complaint and make a decision</li>
                        </ol>
                        <p>Sometimes lenders and the FOS request more details such as previous addresses, ID, loan reference numbers etc. If this is the case we will likely email you, please reply as soon as you can.</p>
                        <p>Sit back, relax, leave everything to us. Our average pay-out per customer is around <strong>£1650</strong>.</p>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
