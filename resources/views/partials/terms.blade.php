<p><strong>This document contains important information and sets out the Terms of Engagement for the instruction of Return My Money to act on the client’s behalf in all aspects of negotiation and administration of your claim/claims.</strong></p>
<p><strong>Please read the following carefully and ensure you understand all terms before signing the agreement.</strong></p>
<ol class="t-and-c-sections">
    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Definitions</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">‘Claims Management Fees’ means such fees payable by the Client to the Claims Management Company upon conclusion of the claim.</li>
            <li class="t-and-c-section">‘Claims Management Company’ means any company that conducts claims on behalf of consumers.</li>
            <li class="t-and-c-section">‘Claim’ means any claim that the client could make against a third party.</li>
            <li class="t-and-c-section">‘Client’ means the client of the Company.</li>
            <li class="t-and-c-section">‘Compensation’ means any monies due, savings made or benefits received further to a claim against a third party.</li>
            <li class="t-and-c-section">‘Company’ means Return My Pension, a trading style of Pinpoint Call Solutions.</li>
            <li class="t-and-c-section">‘Contract’ means the contract between the Company and the Client for the provision of the services, comprising the signed Terms and Conditions.</li>
            <li class="t-and-c-section">‘DPA/GDPR’ means the Data Protection Act 2018, as amended from time to time.</li>
            <li class="t-and-c-section">‘Services’ means all or any of the services as specified in the Contract.</li>
        </ol>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Duration & Services</p>
        <p>The Contract shall commence on the date on which the Client’s signed Terms and Conditions are received by the Company and unless terminated earlier as provided below shall continue until the company has completed the claims process for the client. The Company agrees with the Client; to conduct a full claims management process and settle any claims as efficiently as possible, to act in the best interests of the Client at all times.</p>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Our Fee</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">We will endeavor to recover any mis-sold policies charged by the Third Party plus any interest charged on any premium in exchange for a fee of 30% + Vat, 36% in total.</li>
            <li class="t-and-c-section">You agree that we will receive your compensation into our Client Account, deduct our fee and send you the remaining balance</li>
            <li class="t-and-c-section">Where the client is more than one person the liability for the fee remains joint and several which means that the Company can recover any fees from either or both persons.</li>
            <li class="t-and-c-section">Example 1: The Client paid £1,000 compensation.  The Company’s fee would be for £360 (36% inc. VAT The Client would receive £640. Example 2: The Clients offer of compensation is £1,000, however the bank deducted £300 owed to them (offset) was £300, leaving £700 in total.  The company’s fee would be £252.  The Client would receive £448. Example 3: The Client is paid £3,000 compensation. The company’s fee is £1080, the Client would receive £1920.</li>
            <li class="t-and-c-section">Payment of the Company’s fee becomes due and payable within 14 days upon the client’s receipt of compensation.</li>
            <li class="t-and-c-section">If the Lender uses their right to offset any of the compensation to any arrears, then our fee will reflect on the amount received in cash to the client and not the full compensation amount</li>
        </ol>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">General Obligations of the Client the Client agrees with the Company:</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">To supply all paperwork and or statements related to the Client’s claim and to provide promptly all such information as the Company may from time to time reasonably request and to deal with all correspondence from the Company within 14 days.</li>
            <li class="t-and-c-section">To ensure that all information sent to the Company is true, accurate, not misleading and shall not contain any relevant omissions.</li>
            <li class="t-and-c-section">To authorise the Company to act on their behalf to contact the Third Party or such other persons, firms or companies as the Company considers necessary to perform the services and to authorise the release of any such information as the Company deems appropriate.</li>
            <li class="t-and-c-section">Not to appoint any other Company or other person, firm or company to provide the services during the term of the contract without the prior written consent of the Company.</li>
        </ol>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Termination/Cancellation of this agreement</p>
        <p>The Company shall have the right by giving written notice to the Client at any time to immediately terminate or cancel the contract if:</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">There occurs any material breach by the Client of any term of contract (in respect of any one or more claims) which is irremediable or, if remediable, is not remedied to the Companies satisfaction within 15 days of a written notice by the Company specifying the breach and requiring it to be remedied; or</li>
            <li class="t-and-c-section">The Client is adjudicated bankrupt, enters into a voluntary arrangement with its creditors or;</li>
            <li class="t-and-c-section">If the Company advises that the Client’s claim is unlikely to succeed and if the Client has not breached their duties set out at section 4 above.</li>
            <li class="t-and-c-section">If the Client has breached their duties set out at paragraph 4;</li>
            <li class="t-and-c-section">Either party is entitled to cancel this agreement at any time by the following method;</li>
        </ol>
        <p>Telephone, ​email, letter or cancellation form. ​If you cancel within the 14 days of the date of this agreement, you will not be charged anything. If you cancel after 14 days, we reserve the right to make a reasonable charge for the work we have done which will be displayed with a breakdown. Once compensation is offered or paid, our success fee is payable. The 14-day cancellation period will commence when we receive these terms and conditions back. Any of the above methods for cancellation will be accepted.</p>
        <p>Example Cancellation form</p>
        <p>
            Pinpoint Call Solutions (trading as Return My Money), Second Floor, Dale House, Tiviot Dale, Stockport, SK1 1TA, 0161 268 8868, claims@pinpointcallsolutions.com : I/We [*] herby give notice that I/We [*] cancel my/our [*] contract of sale of the following goods [*]/ for the supply of the following service [*], Ordered on [*]/ received on [*], Name of client(s), Address of client(s), Signature of client(s) (only if this form is notified on paper), Date
        </p>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Confidentiality</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">Both parties agree to keep confidential the subject matter of the contract any information whether written or oral) acquired by that party in connection with the contract and not to use any such information except for the purpose of performing its obligations under the contract.</li>
            <li class="t-and-c-section">Both parties agree that the provisions of condition 6.1 shall not apply to information already in the public domain other than as a breach of condition 6.1.</li>
        </ol>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Privacy Policy and Data Protection</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">All personal data will be held in accordance with the terms of the Company’s privacy policy which can be found on the Company’s website www.returnmypension.co.uk. All data is held in accordance with the provisions of the DPA and GDPR.</li>
            <li class="t-and-c-section">The Company agrees to comply with any written Data Subject Access Request under the GDPR made by the Client for the personal data that it holds subject to any exemptions that may apply from time to time.</li>
        </ol>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Notices, severability, variations, complaints, waiver</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">The Company operates a complaints mechanism, which can be initiated by any means (letter, telephone, e-mail or in person), by giving full details of any complaint. For more information please visit www.pinpointcallsolutions.com/complaints.</li>
            <li class="t-and-c-section">You do not have to instruct us, you do have the ability to pursue this claim without our advice if you wish.</li>
        </ol>
    </li>

    <li class="t-and-c-section">
        <p class="t-and-c-section-header">Law and Jurisdiction</p>
        <ol class="t-and-c-sections">
            <li class="t-and-c-section">The law applicable to this Contract shall be English Law and the parties’ consent to the jurisdiction of the English Courts in all matters affecting the contract.  Definitions used have the meaning given to them in these Terms and Conditions.</li>
            <li class="t-and-c-section">The parties agree that in the event of non-payment of fees by the Client resulting in litigation between the parties, legal costs shall be payable in keeping with the appropriate Court Guidelines.</li>
        </ol>
    </li>
</ol>
