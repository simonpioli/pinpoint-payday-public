<footer class="rmm-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <figure><img class="rmm-logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAAAXCAYAAABOMABkAAAKTElEQVR4nO1bC5SVVRX+mGGweA0yMWCLiAjNR1hmUaCkZCBF4ApN14pljWZFiBRiCiRoE4SihZZWyDJw0PABqTEgKg1kkwELH6gIPknC4SEjg4gI47Bbp/ud5b77nP/eYdYfTav7rXXWuf/e5+xz/vPaj/PfNiKClHECgAkATgPwEQBvAVgLoArAA2k3VkAB/0mkvUHOBvAnAO0CTgbXA5gUUAsooJUizQ3SDcB2AEV8fhfAIwB6Afi0KjcOwK1B7QIKaIUoSrFL45W8vQBOBXAOgFMAzFHlegc1CyiglaJtit0aqn7fDeB59TyeG8NpjiVBzQKOFNwBdihPW25NvBdQ00UbAGUA9gHY/1+a/WKORU4TKk0N8iH1+wnDOwhgWJ7NMRbAagBbAOwC8BqAewEMDkpm8Au2s42pDsBOAFsZHHCYC6CevNcB1AL4K3/XsZ0/KpmXAngawPFBaxnMBrAGQAdDn0a668cbAF4BMB/AZwIJGf9sIfv+w4ALlHMcrjF0t6CWsX8/DWploweApQCeBPAdxbkIwD8AnBvUAD4O4Bn6iR5uzh7nmA0LamTja+z3KgCfCrgZuMDN/VyYbpzeAfB3AN8MSmZwHOXV0CKx+DqAdeZwdvgqgMcAbAawg3Pt0psArmKZrhyLuwKpGThfehOcD5JS2ijv44oWyPwza68UkbtEpFbJq4iUf5e8B5mqRWSpiCwRkXKWmSoiK0TkIRFZruQ9LCLL2OaNSuZc8r8Yac+lJ8kvM/TNpLs+3C0iT6i2vmzKlko2TjT8T6hx0PRTTb1+kf75NF+Vq1b0Y0nbF6nzCHnDFG26krMrUkenBlV2bIQ/VvHvo+y5ijY3Umek4tdF+JPJG2foM0l383WvWhsufZtlikVkNcsNNPWH+LUVe9GWpnXqZapaIOMB1m2raCeR9p6IHGXKvykir0Tk5Equ/LYc/Flsr3+EB24shy6G/pyIHDS0L7Gsa7ONoncUkbe5qQ6JyGOmXh+1iDS9H+krWO/OSP9cak/+SvbJyrmBci5QtN6krTJlp5A+h/nwSHsunUf+AuYXGf7JapP1NbyjReRZ8kcb3lDSX2B+meGPIf3ChH7bw8cmP9ZrDf1F0julaWI1qN/dA25+eNtYO/EbADxKe/GjRkITgJLDbKMkZb/L4xDtao0amnJ9ImFvZ6LdCWA6gEEA+gcSQ/jxuQ/AbwGMjsgFTVXXlx/ThLbvO435rxTtJlVXw6+PO9j+T4LWMqikz+nNVevn/Iz5CAAvG95uZSJZs9KP6SwAzwK4gWvB8u3Ye7/iw0FPs/EqgIcAfE6Zw6cDOJZjvDfNDbJG/T4q4OaH78suU9L7A3WG3pa+TWtAUYKzV86L0lg/+9CPcpgXcJPRHsBt5I6OlJoM4CkeLsURh3s/23V9+yxp59A/e96UbWJeT59qAICjI+9xAhd3PWl6LNrRJ9hLfyOGbfQX3MI8RvH9RnP+yne5rqZE6lv4tfTPgBPiMlJmMr+O+b/v69LYINfwhvw8RXP3Hg8CqAawImEiLfxE9ufdiZu8e+hwu5PrbVO+gXcvk5jcwF3LsPKRxkGO5cnUdIPo2JbQ8Y9tHlduD0PgJwIYQnq+OXH11nPxTDa8AXQ+p+aJDnkt4vp2OX9b7aFxQDnvPzA834fFJlDj8TEeZmsDTjY8v1fAydDW0MmuVAew1RwejcyvAHAl14cLbFRE6riAyl+oxQYykFDFgy1Qvy3BKC4MjVIAI9XzizmiBR5+Azxs6GfzwtGigebYTEN/iyfokUQ9T+v1ps2BOU5NvxHcAv0+gN/zMNgblMxGRz4t5MHTV5ktPrq1lHOQBBc9ullF0ZbThEnCMXwPN0cTAfyc5dw7X0KeJGyQTsx3BJxs7OZTzGz07/I9AIuoAcclHDxQGvsSQ3+Zi9/WG0uNW0ut+SPPSEODXMCFcJaZ3Ik8zQdHFnEMPnRawYlfxueekbKgfbmVKr+U2qQbJ76l8KdL0l2Bp9tTqAs14EiGVf9GelkgIZTlFuuNfM8zm7GQvA0+i/mVqg9DGBpHxBSymMyvHaC0SBL8pryVGuoMPn+L+dXMYweuP83z9cdvgpjm8wt6MU2xS/lszW6PDzJ3h3dnbtxyhp9jc+tMywWc1+vUZk1lg2ziCVJjOryeMftVzbQFvcPtzKo/ABjOGPztAD4ZlH7/QquBWmMXU1NQsvnwE9yYUMPLtoPcnrQl1ASn87ObJXk2iccUyp6nTtCkufGL5RmO98V8rmBeybw4qJmN/bwncdgYcLPhD4TfMB/DfCq1So3pm8br/H1SwMnGF/j0asDJluvvdSaYwJCG728dD+16de+ShOWkP675SZPQUuiF1f4wZfhFp21QbwJMC0pnyudbBIcLf3IkaS0fMLD+UBPHUo/nBObjAykhGunL9eYCONDMuZnNMRhFM+01mgpIWKwWfvxiJ7+Gl7WF8s+ln+n8i1uC0tlwh9ZLnNeuATeD7pT1NP2MXFjJw8FFtD7PcnYd+P7mMjMtvJbspOlpbxAd3s3nlCVBT+w6nsTfiNxeFydEh3Ih36KpZj4j4ABf4W3z4mZqqXusPZsHM7hBb+GCtZswhtv5TlXcvNMjZXLBz3+Ssxsbr+up7VeQPztS3tbz0bqqQFqmbf+FxdUBNy6vgvPvvwy3kTr/PpsDScmIjkXSydGPJ0XMXkvCIPoA4OcEMVu6lp8jzNB2HuF3rr3b+CXt7UlU6R4f4IKt5Eu14aA1MTIUM+u6Kvs0Bmdy/I4mxEb+rmecfDxPt1i0p5RjqQdXGBp1GuFC2rhQp13nQEqmDV/O9tOPS0dF281NPYJaaL7i+bnNOhENfB+SDkrflg7bL+QnPGWMVO5UvA4m95hDrTOcJvk8hnZ78gBx6+bXDC5oeHPTvsNT3FQjEtrzz+7+5QU15h25sasRwls82eNubhBP4c2uvX3Nl9qJyAbePm5IKHuTumlfE+G7z0v2i0gvQ+/Mzxi0XHczfb+IHJA4BkTku7RJRLZE6DZdJSJNRvJS3rzGyq9mH4sNvS8/idGfe7ib7ndE5NGIHJcWkX+toR9HWTcb+mDSZxp6T47nHZE2fHJzvUdESiI8lyZS9hmR23hHP83QLyb9/Igsl6bxdl9ja+Tm3aczKe/yCK8H59ON1VmGN5pfWsRwW0QW2AfX1ihN9/8HKePprMN+M7iDk9Sv1y59WK8fHb8x1BLduGMd/3xGaDymRCJbHVg+pnnKeIrZqEWp8Xu8Bklyxrrwfaz2iqEdHcsOjJVvi5TRcovVRZlGd7a5XdHK6WfsCUpn0I1OpUUP9v1ApI3YuHWnqbYv4OTvt0NbRoB2RMycWJvF7HusvJZ5PKNa2+mfJKGIY/VGgllbRDmx/nvtp62gEo5HTJbv+05dx22QcVRvaaCRocNcar2W5lgBBbR6FDEMV5NSR0vybI5FkU+TCyig1UL/5XYoQ40DGV9eQEc4ycRqDhqp0l7iXclzhaVQwP8SYv9Jr6Q9lvTlZgEF/H8AwL8Aa+KcZbddEtEAAAAASUVORK5CYII=
" alt="Return My Money Logo"></figure>
                <p class="small">
                    © Return My Money<br>
                    Dale House, Tiviot Dale<br>
                    Stockport, SK1 1TA
                </p>
                <p class="small">Monday-Thursday: 09:30 - 18:00 Friday: 09:30 - 16:00</p>
                <p class="small">
                    <a href="https://www.returnmymoney.com/terms" target="_blank">Terms &amp; Conditions</a><br>
                    <a href="https://www.returnmymoney.com/privacy" target="_blank">Privacy Policy</a>
                </p>
            </div>
            <div class="col-12 col-md-6">
                <p class="text-right">
                    <a href="https://www.returnmymoney.com/post/wonga-forced-into-administratio" target="_blank">
                        Wonga Forced Into Administration
                    </a>
                </p>
            </div>
        </div>
    </div>
</footer>
<footer class="rmm-footer rmm-footer-dark">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="small">Return My Money is a trading style of Pinpoint (Call Solutions) Limited, authorised and regulated by the Financial Conduct Authority, registration number 830621. Registered under the Data Protection Act 2018, Reference Z2879273.</p>
            </div>
        </div>
    </div>
</footer>
