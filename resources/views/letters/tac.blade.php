@extends('layouts.letter')

@section('document-title')Terms Letter - {{ $applicant->first_name . ' ' . $applicant->last_name . ' ' . $applicant->signed_date->format('Y-m-d') }}@endsection
@section('content')
    @include('partials.document-header')
    @include('partials.terms')
    <section class="signature-section">
        <p class="signature-line">
            <strong>Signed:</strong> <span class="signature">{{ substr($applicant->first_name, 0, 1) . '.' . $applicant->last_name }}</span>
        </p>
        <p><strong>Name: </strong>{{ $applicant->title . ' ' . $applicant->first_name . ' ' . $applicant->last_name }}</p>
        <p><strong>Date:</strong> {{ $applicant->signed_date->format('d/m/Y') }}</p>
        <p><strong>Client IP Address:</strong> {{ $applicant->client_ip }}</p>
    </section>
@endsection

@section('footer')
    @include('partials.document-footer')
@endsection
