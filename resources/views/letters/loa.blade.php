@extends('layouts.letter')

@section('document-title')Letter of Authority - {{ $applicant->first_name . ' ' . $applicant->last_name . ' ' . $applicant->signed_date->format('Y-m-d') }}@endsection
@section('content')
    @include('partials.document-header')
    <h1 class="h4">Letter of Authority</h1>
    <p>Please accept this letter as my/our instruction to you, (the “company”) to deal directly with Pinpoint Call Solutions (T/A Return My Money) as my/our authorised claims representative in respect of the complaint and to provide them with any information they request and which they require to pursue my/our complaint.
        I/we request that as of the date of this letter any further correspondence or communication regarding this matter is to be addressed to Return My Money. For the avoidance of doubt communication includes telephonic and written correspondence whether by fax, email or post.
        This letter of authority will be in force for the duration of this claim unless otherwise determined by Return My Money and/or the client.
    </p>
    <table class="table table-sm table-bordered">
        <tr>
            <th scope="row">Client Name</th>
            <td>{{ $applicant->title . ' ' . $applicant->first_name . ' ' . $applicant->last_name }}</td>
        </tr>
        <tr>
            <th scope="row">Date of Birth</th>
            <td>{{ $applicant->date_of_birth->format('d/m/Y') }}</td>
        </tr>
        @foreach($addresses as $address)
            @if($address->primary === 1)
                <tr>
                    <th scope="row">Address</th>
                    <td>
                        {{ $address->address_line_1 }}<br>
                        {{ $address->address_line_2 }}<br>
                        {{ $address->town }}<br>
                        {{ $address->county }}
                    </td>
                </tr>
                <tr>
                    <th scope="row">Postcode</th>
                    <td>{{ $address->postcode }}</td>
                </tr>
            @endif
        @endforeach
    </table>
    <table class="table table-sm table-bordered">
        <tr>
            <th scope="row">CMC Name</th>
            <td>Pinpoint Call Solutions Limited (T/A Return My Money)</td>
        </tr>
        <tr>
            <th scope="row">CMC Reg Number</th>
            <td>CRM28413</td>
        </tr>
        <tr>
            <th scope="row">CMC Address</th>
            <td>Second Floor, Dale House, Tiviot Dale, Stockport, SK1 1TA</td>
        </tr>
        <tr>
            <th scope="row">CMC Contact Number</th>
            <td>0161 268 8868</td>
        </tr>
        <tr>
            <th scope="row">CMC Email</th>
            <td>claims@returnmymoney.com</td>
        </tr>
    </table>
    <table class="table table-sm table-bordered">
        <tr>
            <td><strong>Name:</strong> {{ $applicant->title . ' ' . $applicant->first_name . ' ' . $applicant->last_name }}</td>
            <td><strong>Signed:</strong> <span class="signature">{{ substr($applicant->first_name, 0, 1) . '.' . $applicant->last_name }}</span></td>
            <td><strong>Date:</strong> {{ $applicant->signed_date->format('d/m/Y') }}</td>
        </tr>
        <tr>
            <td colspan="3"><strong>Client IP Address:</strong> {{ $applicant->client_ip }}</td>
        </tr>
    </table>
@endsection

@section('footer')
    @include('partials.document-footer')
@endsection
