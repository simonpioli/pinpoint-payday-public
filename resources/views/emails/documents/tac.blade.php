<p>Attached Terms and Conditions letter for the following applicant:</p>

<p>
    Form Reference: {{ $applicant->id }}<br>
    {{ $applicant->title . ' ' . $applicant->first_name . ' '. $applicant->last_name }}<br>
    {{ $applicant->email }}<br>
    {{ $applicant->telephone }}
</p>
