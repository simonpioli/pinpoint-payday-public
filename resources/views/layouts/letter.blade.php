<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>@yield('document-title')</title>

    <!-- Styles -->
    <style>
        @include('css.documents')
    </style>
</head>
<body>
<header class="header-section">
    @yield('header')
</header>
@yield('content')
<htmlpagefooter name="page-footer">
    <div class="footer-gapper"></div>
    <footer class="footer-section">
        @yield('footer')
    </footer>
</htmlpagefooter>
</body>
</html>
