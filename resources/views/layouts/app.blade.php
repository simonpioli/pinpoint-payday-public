<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    @yield('pre-js')
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2272852929625188');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                     src="https://www.facebook.com/tr?id=2272852929625188&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    @yield('post-fb')
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,800" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <meta property="og:image" content="{{ asset('img/rmm-poster.jpg') }}">
    <meta
        property="og:description"
        content="Get started with your refund. You could be owed over £1500* start your claim today and find out. The process is 100% online. No Win, No Fee.">
</head>
<body>
    @include('partials.web-header')
    <div class="app-container" id="app">
        <section class="py-4">
            @yield('content')
        </section>
    </div>
    @include('partials.web-footer')
</body>
</html>
