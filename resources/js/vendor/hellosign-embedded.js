(function webpackUniversalModuleDefinition(root, factory) {
    if(typeof exports === 'object' && typeof module === 'object')
        module.exports = factory();
    else if(typeof define === 'function' && define.amd)
        define([], factory);
    else if(typeof exports === 'object')
        exports["HelloSign"] = factory();
    else
        root["HelloSign"] = factory();
})(window, function() {
    return /******/ (function(modules) { // webpackBootstrap
        /******/ 	// The module cache
        /******/ 	var installedModules = {};
        /******/
        /******/ 	// The require function
        /******/ 	function __webpack_require__(moduleId) {
            /******/
            /******/ 		// Check if module is in cache
            /******/ 		if(installedModules[moduleId]) {
                /******/ 			return installedModules[moduleId].exports;
                /******/ 		}
            /******/ 		// Create a new module (and put it into the cache)
            /******/ 		var module = installedModules[moduleId] = {
                /******/ 			i: moduleId,
                /******/ 			l: false,
                /******/ 			exports: {}
                /******/ 		};
            /******/
            /******/ 		// Execute the module function
            /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
            /******/
            /******/ 		// Flag the module as loaded
            /******/ 		module.l = true;
            /******/
            /******/ 		// Return the exports of the module
            /******/ 		return module.exports;
            /******/ 	}
        /******/
        /******/
        /******/ 	// expose the modules object (__webpack_modules__)
        /******/ 	__webpack_require__.m = modules;
        /******/
        /******/ 	// expose the module cache
        /******/ 	__webpack_require__.c = installedModules;
        /******/
        /******/ 	// define getter function for harmony exports
        /******/ 	__webpack_require__.d = function(exports, name, getter) {
            /******/ 		if(!__webpack_require__.o(exports, name)) {
                /******/ 			Object.defineProperty(exports, name, {
                    /******/ 				configurable: false,
                    /******/ 				enumerable: true,
                    /******/ 				get: getter
                    /******/ 			});
                /******/ 		}
            /******/ 	};
        /******/
        /******/ 	// define __esModule on exports
        /******/ 	__webpack_require__.r = function(exports) {
            /******/ 		Object.defineProperty(exports, '__esModule', { value: true });
            /******/ 	};
        /******/
        /******/ 	// getDefaultExport function for compatibility with non-harmony modules
        /******/ 	__webpack_require__.n = function(module) {
            /******/ 		var getter = module && module.__esModule ?
                /******/ 			function getDefault() { return module['default']; } :
                /******/ 			function getModuleExports() { return module; };
            /******/ 		__webpack_require__.d(getter, 'a', getter);
            /******/ 		return getter;
            /******/ 	};
        /******/
        /******/ 	// Object.prototype.hasOwnProperty.call
        /******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
        /******/
        /******/ 	// __webpack_public_path__
        /******/ 	__webpack_require__.p = "";
        /******/
        /******/
        /******/ 	// Load entry module and return exports
        /******/ 	return __webpack_require__(__webpack_require__.s = "./src/embedded.js");
        /******/ })
    /************************************************************************/
    /******/ ({

        /***/ "./package.json":
        /*!**********************!*\
          !*** ./package.json ***!
          \**********************/
        /*! exports provided: name, version, description, homepage, main, license, scripts, repository, author, devDependencies, files, default */
        /***/ (function(module) {

            module.exports = {"name":"hellosign-embedded","version":"1.6.1","description":"Embed HelloSign signature requests and templates from within your web application.","homepage":"https://github.com/HelloFax/hellosign-embedded","main":"index.js","license":"ISC","scripts":{"build":"webpack","demo":"npm run build && npm start --prefix=demo","prerelease":"node scripts/pre-release.js","setup":"npm install && npm run build && npm run setup --prefix=demo","test":"mocha test"},"repository":{"type":"git","url":"https://github.com/HelloFax/hellosign-embedded.git"},"author":{"name":"HelloSign","email":"api@hellosign.com","url":"https://hellosign.com"},"devDependencies":{"@babel/core":"^7.0.0-beta.42","@babel/plugin-proposal-object-rest-spread":"^7.0.0-beta.42","@babel/plugin-transform-object-assign":"^7.0.0-beta.42","@babel/preset-env":"^7.0.0-beta.42","babel-loader":"^8.0.0-beta.2","chai":"^4.1.2","jsdom":"^11.6.2","jsdom-global":"^3.0.2","mocha":"^5.0.4","webpack":"^4.2.0","webpack-cli":"^2.0.13"},"files":["umd","index.js"]};

            /***/ }),

        /***/ "./src/embedded.js":
        /*!*************************!*\
          !*** ./src/embedded.js ***!
          \*************************/
        /*! no static exports found */
        /***/ (function(module, exports, __webpack_require__) {

            "use strict";


            function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

            function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

            /**
             * HelloSign JS library for embeddables
             * Copyright (c) 2016 HelloSign
             *
             * XWM - Cross-window messaging inspired by Ben Alman's
             * jQuery postMessage plugin:
             * http://benalman.com/projects/jquery-postmessage-plugin/
             *
             *    Copyright (c) 2009 "Cowboy" Ben Alman
             *    Dual licensed under the MIT and GPL licenses.
             *    http://benalman.com/about/license/
             */
            (function () {
                function getUrlVars() {
                    var vars = {};
                    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                        vars[key] = value;
                    });
                    return vars;
                } // Underscore.js debounce implementation.
                //
                // Returns a function, that, as long as it continues to be invoked, will not
                // be triggered. The function will be called after it stops being called for
                // N milliseconds. If `immediate` is passed, trigger the function on the
                // leading edge, instead of the trailing.


                function debounce(func, wait, immediate) {
                    var timeout;
                    return function () {
                        var context = this,
                            args = arguments;

                        var later = function later() {
                            timeout = null;
                            if (!immediate) func.apply(context, args);
                        };

                        var callNow = immediate && !timeout;
                        clearTimeout(timeout);
                        timeout = setTimeout(later, wait);
                        if (callNow) func.apply(context, args);
                    };
                }

                var urlVars = getUrlVars();
                window.isDebugEnabled = urlVars.debug ? urlVars.debug === 'true' : false;
                var userAgent = navigator.userAgent.toLowerCase();
                var XWM = {
                    cacheBust: 0,
                    lastHash: 0,
                    intervalId: 0,
                    rmCallback: null,
                    defaultDelay: 500,
                    hasPostMessage: window['postMessage'] !== undefined,
                    _serializeMessageValue: function _serializeMessageValue(value) {
                        if (_typeof(value) === 'object') {
                            value = JSON.stringify(value);
                        }

                        return encodeURIComponent(value);
                    },
                    send: function send(message, targetUrl, target) {
                        l('XWM Send: Sending Message.');
                        l('  targetUrl: ' + targetUrl);
                        var self = XWM;

                        if (!targetUrl) {
                            return;
                        } // Serialize the message into a string


                        if (typeof message != 'string') {
                            var parts = [];

                            for (var k in message) {
                                parts.push(k + '=' + this._serializeMessageValue(message[k]));
                            }

                            message = parts.join('&');
                        }

                        l('  message: ' + message);

                        if (self.hasPostMessage) {
                            // The browser supports window.postMessage, so call it with a targetOrigin
                            // set appropriately, based on the targetUrl parameter.
                            target = target || parent;
                            target['postMessage'](message, targetUrl.replace(/([^:]+:\/\/[^\/]+).*/, '$1'));
                        } else if (targetUrl) {
                            // The browser does not support window.postMessage, so set the location
                            // of the target to targetUrl#message. A bit ugly, but it works! A cache
                            // bust parameter is added to ensure that repeat messages trigger the
                            // callback.
                            var t = new Date().getTime();
                            var c = ++self.cacheBust;
                            var targetFrame = document.getElementById(target); // target is the window id in this case
                            // targetWindow.location = targetUrl.replace( /#.*$/, '' ) + '#' + t + c + '&' + message;

                            if (targetFrame) {
                                targetFrame.setAttribute('src', targetUrl.replace(/#.*$/, '') + '#' + t + c + '&' + message);
                            } else {
                                parent.location = targetUrl.replace(/#.*$/, '') + '#' + t + c + '&' + message;
                            }
                        }

                        l('XWM Send: Message sent.');
                    },
                    receive: function receive(callback, sourceOrigin, delay) {
                        if (typeof callback !== 'function') {
                            error('callback must be a function');
                        }

                        if (typeof sourceOrigin !== 'string') {
                            error('sourceOrigin must be a string');
                        }

                        l('XWM Receive: Initialize receiver.');
                        l('  callback: ' + (callback.name ? callback.name : 'Anonymous function'));
                        l('  sourceOrigin: ' + sourceOrigin);
                        var self = XWM;

                        if (self.hasPostMessage) {
                            // Since the browser supports window.postMessage, the callback will be
                            // bound to the actual event associated with window.postMessage.
                            if (callback) {
                                if (self.rmCallback) {
                                    // Unbind previous callback
                                    if (window['addEventListener']) {
                                        window['removeEventListener']('message', self.rmCallback, false);
                                    } else {
                                        //IE8 doesn't support removeEventListener
                                        window['detachEvent']('onmessage', self.rmCallback);
                                    }
                                } // Bind the callback. A reference to the callback is stored for ease of unbinding


                                self.rmCallback = function (evt) {
                                    // Ensure the event is originating from the source domain, accounting
                                    // for subdomains (evt.origin must end with a dot and the sourceOrigin string).
                                    if (evt.origin !== sourceOrigin) {
                                        var subdomainTest = new RegExp('[\/|\.]' + sourceOrigin + '$', 'i');

                                        if (!subdomainTest.test(evt.origin)) {
                                            return false;
                                        }
                                    }

                                    l('XWM Receive: Message received!');
                                    l('  data: ' + evt.data);
                                    l('  sourceOrigin: ' + sourceOrigin);
                                    callback(evt);
                                };
                            }

                            if (window['addEventListener']) {
                                window['addEventListener']('message', self.rmCallback, false);
                            } else {
                                //IE8 doesn't support addEventListener
                                window['attachEvent']('onmessage', self.rmCallback);
                            }
                        } else {
                            // Since the browser sucks, a polling loop will be started, and the
                            // callback will be called whenever the location.hash changes.
                            l('XWM Receive: Starting poll...');

                            if (self.intervalId) {
                                clearInterval(self.intervalId);
                                self.intervalId = null;
                            }

                            if (typeof delay === 'undefined') {
                                delay = self.defaultDelay;
                            }

                            if (callback) {
                                delay = delay !== undefined ? delay : 200;
                                self.intervalId = setInterval(function () {
                                    var hash = document.location.hash;
                                    var re = /^#?\d+&/;

                                    if (hash !== self.lastHash && re.test(hash)) {
                                        self.lastHash = hash;
                                        var data = hash.replace(re, '');
                                        l('XWM Receive: Message received!');
                                        l('  data: ' + data);
                                        l('  sourceOrigin: ' + sourceOrigin);
                                        callback({
                                            data: data
                                        });
                                    }
                                }, delay);
                            }
                        }
                    }
                };
                /**
                 * Helper functions to manage the "viewport" meta tag.
                 * This allows us to dynamically control the display
                 * and placement of the iFrame in a mobile context.
                 */

                var MetaTagHelper = {
                    savedViewportContent: '',
                    set: function set() {
                        l('Optimizing viewport meta tag for mobile'); // Save off the current viewport meta tag content

                        this.savedViewportContent = this._getElement().getAttribute('content'); // Add mobile-optimized settings

                        var contentPairs = this._explodePairs(this.savedViewportContent);

                        contentPairs['width'] = 'device-width';
                        contentPairs['maximum-scale'] = '1.0';
                        contentPairs['user-scalable'] = 'no';

                        this._getElement().setAttribute('content', this._joinPairs(contentPairs));
                    },
                    restore: function restore() {
                        l('Restoring viewport meta tag');

                        this._getElement().setAttribute('content', this.savedViewportContent);
                    },
                    _getElement: function _getElement() {
                        var el = document.querySelector('meta[name=viewport]');

                        if (!el) {
                            el = document.createElement('meta');
                            el.setAttribute('name', 'viewport');
                            el.setAttribute('content', 'initial-scale=1.0');
                            document.head.appendChild(el);
                        }

                        return el;
                    },
                    _joinPairs: function _joinPairs(keyed) {
                        var pairs = [];

                        for (var key in keyed) {
                            pairs.push(key + '=' + keyed[key]);
                        }

                        return pairs.join(', ');
                    },
                    _explodePairs: function _explodePairs(metaString) {
                        var pairs = metaString.split(',');
                        var obj = {};
                        pairs.forEach(function (pair) {
                            pair = pair.trim();
                            var kv = pair.split('=');
                            obj[kv[0]] = kv[1];
                        });
                        return obj;
                    }
                };
                var HelloSign = {
                    VERSION: __webpack_require__(/*! ../package.json */ "./package.json").version,
                    IFRAME_WIDTH_RATIO: 0.8,
                    DEFAULT_WIDTH: 900,
                    DEFAULT_HEIGHT: 900,
                    MIN_HEIGHT: 480,
                    wrapper: null,
                    iframe: null,
                    overlay: null,
                    cancelButton: null,
                    clientId: null,
                    isOldIE: /msie (8|7|6|5)/gi.test(userAgent),
                    isFF: /firefox/gi.test(userAgent),
                    isOpera: /opera/gi.test(userAgent),
                    isMobile: /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(userAgent),
                    baseUrl: 'https://www.hellosign.com',
                    cdnBaseUrl: 'https://s3.amazonaws.com/cdn.hellofax.com',
                    XWM: XWM,
                    CULTURES: {
                        EN_US: 'en_US',
                        FR_FR: 'fr_FR',
                        DE_DE: 'de_DE',
                        SV_SE: 'sv_SE',
                        ZH_CN: 'zh_CN',
                        DA_DK: 'da_DK',
                        NL_NL: 'nl_NL',
                        ES_ES: 'es_ES',
                        ES_MX: 'es_MX',
                        PT_BR: 'pt_BR',
                        PL_PL: 'pl_PL',
                        init: function init() {
                            this.supportedCultures = [this.EN_US, this.FR_FR, this.DE_DE, this.SV_SE, this.ZH_CN, this.DA_DK, this.NL_NL, this.ES_ES, this.ES_MX, this.PT_BR, this.PL_PL];
                            return this;
                        }
                    }.init(),
                    isDebugEnabled: window.isDebugEnabled,
                    // PUBLIC EVENTS
                    // ---------------------------
                    // - error                          An error occurred in the iFrame
                    // - signature_request_signed       The signature request was signed
                    // - signature_request_canceled     The user closed the iFrame before completing
                    // THESE EVENT CODES ARE ACTUALLY USED IN TWO PLACES
                    // IF YOU CHANGE THEM MAKE SURE TO CHANGE THE OTHERS
                    // IN HFACTIONS.PHP TO STAY CONSISTENT.
                    EVENT_SIGNED: 'signature_request_signed',
                    EVENT_DECLINED: 'signature_request_declined',
                    EVENT_CANCELED: 'signature_request_canceled',
                    EVENT_REASSIGNED: 'signature_request_reassigned',
                    EVENT_SENT: 'signature_request_sent',
                    EVENT_TEMPLATE_CREATED: 'template_created',
                    EVENT_ERROR: 'error',
                    //  ----  PUBLIC METHODS  -----------------------------
                    init: function init(appClientId) {
                        this.clientId = appClientId;
                    },
                    open: function open(params) {
                        var self = this; // PARAMETERS:
                        // ----------------------
                        // - url                      String. The url to open in the child frame
                        // - redirectUrl              String. Where to go after the signature is completed
                        // - allowCancel              Boolean. Whether a cancel button should be displayed (default = true)
                        // - messageListener          Function. A listener for X-window messages coming from the child frame
                        // - userCulture              HelloSign.CULTURE. One of the HelloSign.CULTURES.supportedCultures (default = HelloSign.CULTURES.EN_US)
                        // - debug                    Boolean. When true, debugging statements will be written to the console (default = false)
                        // - skipDomainVerification   Boolean. When true, domain verification step will be skipped if and only if the Signature Request was created with test_mode=1 (default = false)
                        // - container                DOM element that will contain the iframe on the page (default = document.body)
                        // - height                   Height of the iFrame (only applicable when a container is specified)
                        // - hideHeader               Boolean. When true, the header will be hidden (default = false). This is only functional for customers with embedded branding enabled.
                        // - requester                String. The email of the person issuing a signature request. Required for allowing 'Me + Others' requests
                        // - whiteLabelingOptions     Object. An associative array to be used to customize the app's signer page
                        // - healthCheckTimeoutMs     Integer. The number of milliseconds to wait for a response from the iframe. If no response after that time the iframe will be closed. 15000 milliseconds is recommended.
                        // - finalButtonText          String. The text to use for the final send/continue button if you'd like to override it's default text. May only be set to "Send" or "Continue".

                        var redirectUrl = this.safeUrl(params['redirectUrl']);
                        var messageListener = params['messageListener'];
                        var frameUrl = this.safeUrl(params['url']);
                        this.healthCheckTimeoutMs = params['healthCheckTimeoutMs'];

                        if (!frameUrl) {
                            throw new TypeError('Must provide arguments to open()');
                        }

                        if (typeof params['debug'] !== 'undefined') {
                            this.isDebugEnabled = params['debug'] === true || params['debug'] == 'true';
                        }

                        if (typeof params['skipDomainVerification'] !== 'undefined') {
                            this.skipDomainVerification = params['skipDomainVerification'] === true || params['skipDomainVerification'] == 'true';
                        }

                        if (typeof params['hideHeader'] !== 'undefined') {
                            this.hideHeader = params['hideHeader'] === true || params['hideHeader'] == 'true';
                        }

                        if (typeof params['finalButtonText'] !== 'undefined') {
                            this.finalButtonText = params['finalButtonText'];
                        }

                        if (_typeof(params['whiteLabelingOptions']) === 'object') {
                            this.whiteLabelingOptions = JSON.stringify(params['whiteLabelingOptions']);
                            this.whiteLabelingOptions = this.whiteLabelingOptions.replace(/#/g, '');
                        } else if (typeof params['whiteLabelingOptions'] !== 'undefined') {
                            l("Invalid white labeling options supplied, option will be ignored: " + params['whiteLabelingOptions']);
                        }

                        this.isInPage = params['container'] !== undefined;
                        this.container = params['container'] || document.body; // Validate parameters

                        if (this.isInPage && params['height'] !== undefined && (isNaN(parseInt(params['height'], 10)) || params['height'] <= 0)) {
                            throw new Error('Invalid iFrame height (' + params['height'] + ') it must be a valid positive number');
                        }

                        l('Opening HelloSign embedded iFrame with the following params:');
                        l(params);

                        if (!frameUrl) {
                            throw new Error('No url specified');
                        }

                        var userCulture = typeof params['userCulture'] === 'undefined' ? this.CULTURES.EN_US : params['userCulture'];

                        if (this.inArray(userCulture, this.CULTURES.supportedCultures) === -1) {
                            throw new Error('Invalid userCulture specified: ' + userCulture);
                        }

                        frameUrl += frameUrl.indexOf('?') > 0 ? '&' : '?';

                        if (redirectUrl) {
                            frameUrl += 'redirect_url=' + encodeURIComponent(redirectUrl) + '&';
                        }

                        frameUrl += 'parent_url=' + encodeURIComponent(document.location.href.replace(/\?.*/, '')) + '&';
                        frameUrl += this.skipDomainVerification === true ? 'skip_domain_verification=1&' : '';
                        frameUrl += 'client_id=' + this.clientId + '&';
                        frameUrl += typeof params['requester'] !== 'undefined' ? 'requester=' + encodeURIComponent(params['requester']) + '&' : '';
                        frameUrl += 'user_culture=' + userCulture;

                        if (this.isDebugEnabled) {
                            frameUrl += '&debug=true';
                        }

                        if (this.hideHeader) {
                            frameUrl += '&hideHeader=true';
                        }

                        if (this.whiteLabelingOptions) {
                            frameUrl += '&white_labeling_options=' + encodeURI(this.whiteLabelingOptions);
                        }

                        if (this.finalButtonText) {
                            frameUrl += '&final_button_text=' + this.finalButtonText;
                        }

                        frameUrl += '&js_version=' + this.VERSION;
                        var origin = frameUrl.replace(/([^:]+:\/\/[^\/]+).*/, '$1');
                        var windowDims = this.getWindowDimensions(params['height']);
                        var styles = {
                            'overlay': {
                                'position': 'fixed',
                                'top': '0px',
                                'left': '0px',
                                'bottom': '0px',
                                'right': '0px',
                                'z-index': 9997,
                                'display': 'block',
                                'background-color': '#222',
                                'opacity': 0.4,
                                '-khtml-opacity': 0.4,
                                '-moz-opacity': 0.4,
                                'filter': 'alpha(opacity=40)',
                                '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=40)'
                            },
                            'wrapper': this.isInPage ? {} : {
                                'position': 'absolute',
                                'top': windowDims.top,
                                'left': windowDims.left,
                                'z-index': 9998
                            },
                            'iframe': this.isInPage ? {} : {
                                'margin': '1px',
                                'background-color': '#FFF',
                                'z-index': 9998
                            },
                            'cancelButton': {
                                'position': 'absolute',
                                'top': '-13px',
                                'right': '-13px',
                                'width': '30px',
                                'height': '30px',
                                'background-image': 'url(' + this.cdnBaseUrl + '/css/fancybox/fancybox.png)',
                                'background-position': '-40px 0px',
                                'cursor': 'pointer',
                                'z-index': 9999
                            }
                        };

                        var resizeIFrame = function _resizeIFrame() {
                            if (self.iframe) {
                                var dims = {};

                                if (self.isMobile) {
                                    dims = self.getMobileDimensions();
                                } else {
                                    dims = self.getWindowDimensions();
                                }

                                self.wrapper.style['top'] = dims.top;
                                self.wrapper.style['left'] = dims.left;
                                self.wrapper.style['width'] = dims.widthString;
                                self.iframe.style['height'] = dims.heightString;
                                self.iframe.style['width'] = dims.widthString;
                            }
                        };

                        if (this.isInPage) {
                            // Adjust the iFrame style to fit the in-page container
                            styles['wrapper']['width'] = '100%';
                            styles['wrapper']['height'] = windowDims.heightString;
                            styles['iframe']['width'] = '100%';
                            styles['iframe']['height'] = windowDims.heightString;
                            styles['iframe']['border'] = 'none';
                            styles['iframe']['box-shadow'] = 'none';
                            styles['cancelButton']['display'] = 'none'; // This is an iOS hack.  Apparently iOS ignores widths set
                            // with a non-pixel value, which means iFrames get expanded
                            // to the full width of their content.  Setting a pixel
                            // value and then using `min-width` is the workaround for
                            // this.
                            // See:  http://stackoverflow.com/questions/23083462/how-to-get-an-iframe-to-be-responsive-in-ios-safari

                            if (this.isMobile) {
                                styles['iframe']['width'] = '1px';
                                styles['iframe']['min-width'] = '100%';
                            }
                        } else if (this.isMobile) {
                            var mobileDims = this.getMobileDimensions(); // Adjust the iFrame style to fit the whole screen

                            styles['wrapper']['position'] = 'absolute';
                            styles['wrapper']['top'] = '0';
                            styles['wrapper']['left'] = '0';
                            styles['wrapper']['width'] = mobileDims.widthString;
                            styles['wrapper']['height'] = mobileDims.heightString;
                            styles['iframe']['position'] = 'absolute';
                            styles['iframe']['top'] = 0;
                            styles['iframe']['left'] = 0;
                            styles['iframe']['width'] = mobileDims.widthString;
                            styles['iframe']['height'] = mobileDims.heightString;
                            styles['iframe']['border'] = 'none';
                            styles['iframe']['box-shadow'] = 'none';
                            styles['cancelButton']['display'] = 'none';
                        } // Build overlay


                        if (!this.isInPage) {
                            if (!this.overlay) {
                                this.overlay = document.createElement('div');
                                this.overlay.setAttribute('id', 'hsEmbeddedOverlay');
                                document.body.appendChild(this.overlay);
                            }

                            this.overlay.setAttribute('style', 'display: block;');
                        } // Build the wrapper


                        if (!this.wrapper) {
                            this.wrapper = document.createElement('div');
                            this.wrapper.setAttribute('id', 'hsEmbeddedWrapper'); // Hack.  We need this on mobile before we insert the DOM
                            // element, otherwise the modal appears above the fold

                            if (this.isMobile) {
                                window.scrollTo(0, 0);
                            }

                            this.container.appendChild(this.wrapper);
                        }

                        if (!this.isInPage) {
                            // When the window is resized, also resize the iframe if necessary
                            // NOTE: Only do this when the iFrame is displayed as a popup, it does not really make sense when it's in-page
                            // Also used for new mobile ux
                            window.onresize = resizeIFrame;
                        } // Build the iFrame


                        if (!this.iframe) {
                            this.iframe = document.createElement('iframe');
                            this.iframe.setAttribute('id', 'hsEmbeddedFrame');
                            this.wrapper.appendChild(this.iframe);
                        }

                        this.iframe.setAttribute('src', frameUrl);
                        this.iframe.setAttribute('scrolling', 'no'); // This needs to stay as 'no' or else iPads, etc. get broken

                        this.iframe.setAttribute('frameborder', '0');
                        this.iframe.setAttribute('height', windowDims.heightRaw); // TODO: Detecting 'embeddedSign' in the frameUrl is a hack. Clean
                        // this up once the embedded close button has been implemented for
                        // embedded requesting and templates.

                        if (frameUrl.indexOf('embeddedSign') === -1) {
                            if (!this.isInPage && (params['allowCancel'] === true || params['allowCancel'] === undefined) && !this.cancelButton) {
                                this.cancelButton = document.createElement('a');
                                this.cancelButton.setAttribute('id', 'hsEmbeddedCancel');
                                this.cancelButton.setAttribute('href', 'javascript:;');

                                this.cancelButton.onclick = function () {
                                    // Close iFrame
                                    HelloSign.close(); // Send 'cancel' message

                                    if (messageListener) {
                                        l('Reporting cancelation');
                                        messageListener({
                                            'event': HelloSign.EVENT_CANCELED
                                        });
                                    }
                                };

                                this.wrapper.appendChild(this.cancelButton);
                            } else if (!params['allowCancel'] && this.cancelButton) {
                                this.wrapper.removeChild(this.cancelButton);
                            }
                        } // Add inline styling


                        for (var k in styles) {
                            var el = this[k];

                            if (el) {
                                for (var i in styles[k]) {
                                    try {
                                        el.style[i] = styles[k][i];
                                    } catch (e) {
                                        // Ignore - exceptions get thrown when the given style is not supported
                                        l(e);
                                    }
                                }
                            }
                        }

                        if (this.cancelButton && (this.isFF || this.isOpera)) {
                            // Firefox is weird with bg images
                            var s = this.cancelButton.getAttribute('style');
                            s += s ? '; ' : '';
                            s += 'background-image: ' + styles.cancelButton['background-image'] + '; ';
                            s += 'background-position: ' + styles.cancelButton['background-position'] + ';';
                            this.cancelButton.setAttribute('style', s);
                        }

                        if (!this.isInPage && !this.isMobile) {
                            // Run resizeIFrame to make sure it fits best from the beginning
                            resizeIFrame();
                        }

                        if (this.isMobile && window === window.top) {
                            // Only set the meta tags for the top window
                            MetaTagHelper.set();
                        }

                        if (this.isMobile && !this.isInPage) {
                            this.fixIframe = debounce(function () {
                                window.scrollTo(0, 0);
                            }, 1000);
                            this.fixIframe();
                            window.addEventListener('scroll', this.fixIframe);
                        } // Close the iframe if page fails to initialize within 15 seconds


                        if (this.healthCheckTimeoutMs) {
                            this._healthCheckTimeoutHandle = setTimeout(function () {
                                var message = 'Signer page failed to initialize within ' + self.healthCheckTimeoutMs + ' milliseconds.';
                                self.reportError(message, document.location.href);
                                self.close();
                            }, this.healthCheckTimeoutMs);
                        } // Start listening for messages from the iFrame


                        XWM.receive(function _parentWindowCallback(evt) {
                            var source = evt.source || 'hsEmbeddedFrame';

                            if (evt.data === 'initialize') {
                                if (self.healthCheckTimeoutMs) clearTimeout(self._healthCheckTimeoutHandle); // remove container from payload to prevent circular reference error

                                var payload = _extends({}, params);

                                delete payload.container;
                                XWM.send(JSON.stringify({
                                    type: 'embeddedConfig',
                                    payload: payload
                                }), evt.origin, source);
                            } else if (evt.data == 'close') {
                                // Close iFrame
                                HelloSign.close();

                                if (messageListener) {
                                    messageListener({
                                        'event': HelloSign.EVENT_CANCELED
                                    });
                                }
                            } else if (evt.data == 'decline') {
                                // Close iFrame
                                HelloSign.close();
                                messageListener({
                                    'event': HelloSign.EVENT_DECLINED
                                });
                            } else if (evt.data == 'reassign') {
                                messageListener({
                                    'event': HelloSign.EVENT_REASSIGNED
                                });
                            } else if (evt.data == 'user-done') {
                                // Close iFrame
                                HelloSign.close();
                            } else if (typeof evt.data === 'string' && evt.data.indexOf('hello:') === 0) {
                                // Hello message - Extract token and send it back
                                var parts = evt.data.split(':');
                                var token = parts[1];
                                XWM.send('helloback:' + token, frameUrl, source);
                            } else if (messageListener && evt.data && typeof evt.data === 'string') {
                                // Forward to message callback
                                var eventData = {};
                                var p,
                                    pairs = evt.data.split('&'); // Recursive helper function to deserialize the event data.

                                var deserializeEventData = function deserializeEventData(str) {
                                    var obj = str;

                                    try {
                                        // Safely parse the string
                                        obj = JSON.parse(str);

                                        if (_typeof(obj) === 'object') {
                                            for (var key in obj) {
                                                obj[key] = parseJson(obj[key]);
                                            }
                                        }
                                    } catch (e) {
                                        /* ignore */
                                    }

                                    return obj;
                                };

                                for (var i = 0; i < pairs.length; i++) {
                                    p = pairs[i].split('=');

                                    if (p.length === 2) {
                                        eventData[p[0]] = deserializeEventData(decodeURIComponent(p[1]));
                                    }
                                }

                                messageListener(eventData);
                            }
                        }, origin);
                    },
                    close: function close() {
                        // Reset viewport settings
                        if (this.isMobile && window === window.top) {
                            MetaTagHelper.restore();
                        }

                        l('Closing HelloSign embedded iFrame'); // Close the child iframe from the parent window

                        if (this.iframe) {
                            var self = this;

                            if (this.cancelButton) {
                                this.wrapper.removeChild(this.cancelButton);
                                this.cancelButton = null;
                            }

                            this._fadeOutIFrame();
                        }

                        if (this.isMobile) {
                            window.removeEventListener('scroll', this.fixIframe);
                        }
                    },
                    //  ----  PRIVATE METHODS  ----------------------------
                    _fadeOutIFrame: function _fadeOutIFrame(currentOpacity) {
                        var self = this;

                        if (self.iframe) {
                            if (!currentOpacity) {
                                currentOpacity = 1.0;
                            } else {
                                currentOpacity -= 0.1;
                            }

                            self.iframe.style.opacity = currentOpacity;
                            self.iframe.style.filter = 'alpha(opacity=' + parseInt(currentOpacity * 100, 10) + ')';

                            if (currentOpacity <= 0.0) {
                                self.iframe.style.opacity = 0;
                                self.iframe.style.filter = 'alpha(opacity=0)';
                                self.iframe.style.display = 'none';
                                clearTimeout(animationTimer);

                                if (self.overlay) {
                                    self.container.removeChild(self.overlay);
                                }

                                self.container.removeChild(self.wrapper);
                                self.wrapper.removeChild(self.iframe);
                                self.overlay = null;
                                self.iframe = null;
                                self.wrapper = null;
                                return false;
                            }

                            var animationTimer = setTimeout(function (currentOpacity) {
                                return function () {
                                    self._fadeOutIFrame(currentOpacity);
                                };
                            }(currentOpacity), 10);
                        }
                    },
                    reportError: function reportError(errorMessage, parentUrl) {
                        XWM.send({
                            'event': HelloSign.EVENT_ERROR,
                            'description': errorMessage
                        }, parentUrl);
                    },
                    ensureParentDomain: function ensureParentDomain(domainName, parentUrl, token, skipDomainVerification, callback) {
                        // domainName:  Domain to match against the parent window location
                        // parentUrl:   Url of the parent window to check (provided to us but not reliable)
                        // callback:    Method to call with the decision, it should take only one boolean parameter.
                        if (window.top == window) {
                            // Not in an iFrame, no need to go further
                            callback(true);
                            return;
                        }

                        if (typeof token !== 'string') {
                            error('Token not supplied by HelloSign. Please contact support.');
                            return;
                        }

                        if (typeof callback !== 'function') {
                            error('Callback not supplied by HelloSign. Please contact support.');
                            return;
                        }

                        if (skipDomainVerification === true) {
                            var warningMsg = 'Domain verification has been skipped. Before requesting approval for your app, please be sure to test domain verification by setting skipDomainVerification to false.';
                            l(warningMsg);
                            alert(warningMsg);
                            callback(true);
                        } else {
                            // Starts waiting for the hello back message
                            XWM.receive(function _ensureParentDomainCallback(evt) {
                                if (evt.data.indexOf('helloback:') === 0) {
                                    var parts = evt.data.split(':');
                                    var valid = parts[1] == token;
                                    callback(valid);
                                }
                            }, domainName);
                        } // Send hello message


                        XWM.send('hello:' + token, parentUrl);
                    },
                    getWindowDimensions: function getWindowDimensions(customHeight) {
                        var scrollX = getScrollX();
                        var scrollY = getScrollY();
                        var windowWidth, windowHeight;

                        if (this.isOldIE) {
                            windowWidth = document.body.clientWidth;
                            windowHeight = document.body.clientHeight;
                        } else {
                            windowWidth = window.innerWidth;
                            windowHeight = window.innerHeight;
                        }

                        var height = this.isInPage && customHeight ? customHeight : Math.max(this.MIN_HEIGHT, windowHeight - 60);
                        var width = Math.min(this.DEFAULT_WIDTH, windowWidth * this.IFRAME_WIDTH_RATIO);
                        return {
                            'widthString': width + 'px',
                            'heightString': height + 'px',
                            'heightRaw': height,
                            'scrollX': scrollX,
                            'scrollY': scrollY,
                            'top': Math.max(0, scrollY + parseInt((windowHeight - height) / 2, 10)) + 'px',
                            'left': Math.max(0, parseInt((windowWidth - this.DEFAULT_WIDTH) / 2, 10)) + 'px'
                        };
                    },
                    getMobileDimensions: function getMobileDimensions() {
                        var dims;
                        var screenWidth = screen.width;
                        var screenHeight = screen.height;
                        var windowWidth = window.innerWidth;
                        var windowHeight = window.innerHeight;
                        var isPortrait = windowHeight > windowWidth;

                        if (isPortrait) {
                            dims = {
                                'widthString': screenWidth + 'px',
                                'heightString': '100%'
                            };
                        } else {
                            // Landscape
                            dims = {
                                'widthString': windowWidth + 'px',
                                'heightString': '100%'
                            };
                        } // Always fill screen on mobile


                        dims.top = '0';
                        dims.left = '0';
                        return dims;
                    },
                    inArray: function inArray(v, array) {
                        if (this.hasJQuery) {
                            return $.inArray(v, array);
                        } else if (array) {
                            for (var i = 0; i < array.length; i++) {
                                if (array[i] == v) {
                                    return i;
                                }
                            }
                        }

                        return -1;
                    },
                    safeUrl: function safeUrl(url) {
                        if (url) {
                            try {
                                // Security: remove script tags from URLs before processing
                                url = url.replace(/</g, "&lt;");
                                url = url.replace(/>/g, "&gt;"); // HTML-Decode the given url if necessary, by rendering to the page

                                var el = document.createElement('div');
                                el.innerHTML = url;
                                var decodedUrl = el.innerText; // Fall back to just replacing '&amp;' in case of failure

                                if (!decodedUrl) {
                                    url = url.replace(/\&amp\;/g, '&');
                                } else {
                                    url = decodedUrl;
                                }
                            } catch (e) {
                                l('Could not decode url: ' + e);
                            }
                        }

                        return url;
                    }
                };
                /**
                 * Wrapper that will ensure an error message is displayed, either in console.log
                 * or as a browser alert.
                 * @param message String error message
                 */

                function error(message) {
                    if (typeof message !== 'undefined') {
                        if (window.console && console.log) {
                            console.log(message);
                        } else {
                            alert(message);
                        }
                    }
                }
                /**
                 * Custom wrapper that conditionally logs messages to console.log.
                 * @param messageObj String or Object to log
                 */


                function l(messageObj) {
                    if (HelloSign.isDebugEnabled && typeof messageObj !== 'undefined' && window.console && console.log) {
                        console.log(messageObj);
                    }
                }
                /**
                 *  Getter functions for determining scroll position that work on all
                 *  browsers.
                 */


                function getScrollX() {
                    return _supportPageOffset() ? window.pageXOffset : _isCSS1Compat() ? document.documentElement.scrollLeft : document.body.scrollLeft;
                }

                function getScrollY() {
                    return _supportPageOffset() ? window.pageYOffset : _isCSS1Compat() ? document.documentElement.scrollTop : document.body.scrollTop;
                }

                function _isCSS1Compat() {
                    return (document.compatMode || '') === 'CSS1Compat';
                }

                function _supportPageOffset() {
                    return window.pageXOffset !== undefined;
                } // Also add HelloSign as global variable for AMD implementations.
                // This will prevent older integrations from breaking.


                if (true) {
                    window.HelloSign = HelloSign;
                } // Export the HS object


                module.exports = HelloSign;
            })();

            /***/ })

        /******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9IZWxsb1NpZ24vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovL0hlbGxvU2lnbi93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9IZWxsb1NpZ24vLi9zcmMvZW1iZWRkZWQuanMiXSwibmFtZXMiOlsiZ2V0VXJsVmFycyIsInZhcnMiLCJwYXJ0cyIsIndpbmRvdyIsImxvY2F0aW9uIiwiaHJlZiIsInJlcGxhY2UiLCJtIiwia2V5IiwidmFsdWUiLCJkZWJvdW5jZSIsImZ1bmMiLCJ3YWl0IiwiaW1tZWRpYXRlIiwidGltZW91dCIsImNvbnRleHQiLCJhcmdzIiwiYXJndW1lbnRzIiwibGF0ZXIiLCJhcHBseSIsImNhbGxOb3ciLCJjbGVhclRpbWVvdXQiLCJzZXRUaW1lb3V0IiwidXJsVmFycyIsImlzRGVidWdFbmFibGVkIiwiZGVidWciLCJ1c2VyQWdlbnQiLCJuYXZpZ2F0b3IiLCJ0b0xvd2VyQ2FzZSIsIlhXTSIsImNhY2hlQnVzdCIsImxhc3RIYXNoIiwiaW50ZXJ2YWxJZCIsInJtQ2FsbGJhY2siLCJkZWZhdWx0RGVsYXkiLCJoYXNQb3N0TWVzc2FnZSIsInVuZGVmaW5lZCIsIl9zZXJpYWxpemVNZXNzYWdlVmFsdWUiLCJKU09OIiwic3RyaW5naWZ5IiwiZW5jb2RlVVJJQ29tcG9uZW50Iiwic2VuZCIsIm1lc3NhZ2UiLCJ0YXJnZXRVcmwiLCJ0YXJnZXQiLCJsIiwic2VsZiIsImsiLCJwdXNoIiwiam9pbiIsInBhcmVudCIsInQiLCJEYXRlIiwiZ2V0VGltZSIsImMiLCJ0YXJnZXRGcmFtZSIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJzZXRBdHRyaWJ1dGUiLCJyZWNlaXZlIiwiY2FsbGJhY2siLCJzb3VyY2VPcmlnaW4iLCJkZWxheSIsImVycm9yIiwibmFtZSIsImV2dCIsIm9yaWdpbiIsInN1YmRvbWFpblRlc3QiLCJSZWdFeHAiLCJ0ZXN0IiwiZGF0YSIsImNsZWFySW50ZXJ2YWwiLCJzZXRJbnRlcnZhbCIsImhhc2giLCJyZSIsIk1ldGFUYWdIZWxwZXIiLCJzYXZlZFZpZXdwb3J0Q29udGVudCIsInNldCIsIl9nZXRFbGVtZW50IiwiZ2V0QXR0cmlidXRlIiwiY29udGVudFBhaXJzIiwiX2V4cGxvZGVQYWlycyIsIl9qb2luUGFpcnMiLCJyZXN0b3JlIiwiZWwiLCJxdWVyeVNlbGVjdG9yIiwiY3JlYXRlRWxlbWVudCIsImhlYWQiLCJhcHBlbmRDaGlsZCIsImtleWVkIiwicGFpcnMiLCJtZXRhU3RyaW5nIiwic3BsaXQiLCJvYmoiLCJmb3JFYWNoIiwicGFpciIsInRyaW0iLCJrdiIsIkhlbGxvU2lnbiIsIlZFUlNJT04iLCJyZXF1aXJlIiwidmVyc2lvbiIsIklGUkFNRV9XSURUSF9SQVRJTyIsIkRFRkFVTFRfV0lEVEgiLCJERUZBVUxUX0hFSUdIVCIsIk1JTl9IRUlHSFQiLCJ3cmFwcGVyIiwiaWZyYW1lIiwib3ZlcmxheSIsImNhbmNlbEJ1dHRvbiIsImNsaWVudElkIiwiaXNPbGRJRSIsImlzRkYiLCJpc09wZXJhIiwiaXNNb2JpbGUiLCJiYXNlVXJsIiwiY2RuQmFzZVVybCIsIkNVTFRVUkVTIiwiRU5fVVMiLCJGUl9GUiIsIkRFX0RFIiwiU1ZfU0UiLCJaSF9DTiIsIkRBX0RLIiwiTkxfTkwiLCJFU19FUyIsIkVTX01YIiwiUFRfQlIiLCJQTF9QTCIsImluaXQiLCJzdXBwb3J0ZWRDdWx0dXJlcyIsIkVWRU5UX1NJR05FRCIsIkVWRU5UX0RFQ0xJTkVEIiwiRVZFTlRfQ0FOQ0VMRUQiLCJFVkVOVF9SRUFTU0lHTkVEIiwiRVZFTlRfU0VOVCIsIkVWRU5UX1RFTVBMQVRFX0NSRUFURUQiLCJFVkVOVF9FUlJPUiIsImFwcENsaWVudElkIiwib3BlbiIsInBhcmFtcyIsInJlZGlyZWN0VXJsIiwic2FmZVVybCIsIm1lc3NhZ2VMaXN0ZW5lciIsImZyYW1lVXJsIiwiaGVhbHRoQ2hlY2tUaW1lb3V0TXMiLCJUeXBlRXJyb3IiLCJza2lwRG9tYWluVmVyaWZpY2F0aW9uIiwiaGlkZUhlYWRlciIsImZpbmFsQnV0dG9uVGV4dCIsIndoaXRlTGFiZWxpbmdPcHRpb25zIiwiaXNJblBhZ2UiLCJjb250YWluZXIiLCJib2R5IiwiaXNOYU4iLCJwYXJzZUludCIsIkVycm9yIiwidXNlckN1bHR1cmUiLCJpbkFycmF5IiwiaW5kZXhPZiIsImVuY29kZVVSSSIsIndpbmRvd0RpbXMiLCJnZXRXaW5kb3dEaW1lbnNpb25zIiwic3R5bGVzIiwidG9wIiwibGVmdCIsInJlc2l6ZUlGcmFtZSIsIl9yZXNpemVJRnJhbWUiLCJkaW1zIiwiZ2V0TW9iaWxlRGltZW5zaW9ucyIsInN0eWxlIiwid2lkdGhTdHJpbmciLCJoZWlnaHRTdHJpbmciLCJtb2JpbGVEaW1zIiwic2Nyb2xsVG8iLCJvbnJlc2l6ZSIsImhlaWdodFJhdyIsIm9uY2xpY2siLCJjbG9zZSIsInJlbW92ZUNoaWxkIiwiaSIsImUiLCJzIiwiZml4SWZyYW1lIiwiYWRkRXZlbnRMaXN0ZW5lciIsIl9oZWFsdGhDaGVja1RpbWVvdXRIYW5kbGUiLCJyZXBvcnRFcnJvciIsIl9wYXJlbnRXaW5kb3dDYWxsYmFjayIsInNvdXJjZSIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJldmVudERhdGEiLCJwIiwiZGVzZXJpYWxpemVFdmVudERhdGEiLCJzdHIiLCJwYXJzZSIsInBhcnNlSnNvbiIsImxlbmd0aCIsImRlY29kZVVSSUNvbXBvbmVudCIsIl9mYWRlT3V0SUZyYW1lIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImN1cnJlbnRPcGFjaXR5Iiwib3BhY2l0eSIsImZpbHRlciIsImRpc3BsYXkiLCJhbmltYXRpb25UaW1lciIsImVycm9yTWVzc2FnZSIsInBhcmVudFVybCIsImVuc3VyZVBhcmVudERvbWFpbiIsImRvbWFpbk5hbWUiLCJ3YXJuaW5nTXNnIiwiYWxlcnQiLCJfZW5zdXJlUGFyZW50RG9tYWluQ2FsbGJhY2siLCJ2YWxpZCIsImN1c3RvbUhlaWdodCIsInNjcm9sbFgiLCJnZXRTY3JvbGxYIiwic2Nyb2xsWSIsImdldFNjcm9sbFkiLCJ3aW5kb3dXaWR0aCIsIndpbmRvd0hlaWdodCIsImNsaWVudFdpZHRoIiwiY2xpZW50SGVpZ2h0IiwiaW5uZXJXaWR0aCIsImlubmVySGVpZ2h0IiwiaGVpZ2h0IiwiTWF0aCIsIm1heCIsIndpZHRoIiwibWluIiwic2NyZWVuV2lkdGgiLCJzY3JlZW4iLCJzY3JlZW5IZWlnaHQiLCJpc1BvcnRyYWl0IiwidiIsImFycmF5IiwiaGFzSlF1ZXJ5IiwiJCIsInVybCIsImlubmVySFRNTCIsImRlY29kZWRVcmwiLCJpbm5lclRleHQiLCJjb25zb2xlIiwibG9nIiwibWVzc2FnZU9iaiIsIl9zdXBwb3J0UGFnZU9mZnNldCIsInBhZ2VYT2Zmc2V0IiwiX2lzQ1NTMUNvbXBhdCIsImRvY3VtZW50RWxlbWVudCIsInNjcm9sbExlZnQiLCJwYWdlWU9mZnNldCIsInNjcm9sbFRvcCIsImNvbXBhdE1vZGUiLCJtb2R1bGUiLCJleHBvcnRzIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztBQ1ZBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25FQTs7Ozs7Ozs7Ozs7O0FBYUEsQ0FBQyxZQUFVO0FBRVAsV0FBU0EsVUFBVCxHQUFzQjtBQUNsQixRQUFJQyxPQUFPLEVBQVg7QUFDQSxRQUFJQyxRQUFRQyxPQUFPQyxRQUFQLENBQWdCQyxJQUFoQixDQUFxQkMsT0FBckIsQ0FBNkIseUJBQTdCLEVBQ0osVUFBU0MsQ0FBVCxFQUFZQyxHQUFaLEVBQWlCQyxLQUFqQixFQUF3QjtBQUNwQlIsV0FBS08sR0FBTCxJQUFZQyxLQUFaO0FBQ0gsS0FIRyxDQUFaO0FBSUEsV0FBT1IsSUFBUDtBQUNILEdBVE0sQ0FXUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFdBQVNTLFFBQVQsQ0FBa0JDLElBQWxCLEVBQXdCQyxJQUF4QixFQUE4QkMsU0FBOUIsRUFBeUM7QUFDckMsUUFBSUMsT0FBSjtBQUNBLFdBQU8sWUFBVztBQUNkLFVBQUlDLFVBQVUsSUFBZDtBQUFBLFVBQW9CQyxPQUFPQyxTQUEzQjs7QUFDQSxVQUFJQyxRQUFRLFNBQVJBLEtBQVEsR0FBVztBQUNuQkosa0JBQVUsSUFBVjtBQUNBLFlBQUksQ0FBQ0QsU0FBTCxFQUFnQkYsS0FBS1EsS0FBTCxDQUFXSixPQUFYLEVBQW9CQyxJQUFwQjtBQUNuQixPQUhEOztBQUlBLFVBQUlJLFVBQVVQLGFBQWEsQ0FBQ0MsT0FBNUI7QUFDQU8sbUJBQWFQLE9BQWI7QUFDQUEsZ0JBQVVRLFdBQVdKLEtBQVgsRUFBa0JOLElBQWxCLENBQVY7QUFDQSxVQUFJUSxPQUFKLEVBQWFULEtBQUtRLEtBQUwsQ0FBV0osT0FBWCxFQUFvQkMsSUFBcEI7QUFDaEIsS0FWRDtBQVdIOztBQUVELE1BQUlPLFVBQVV2QixZQUFkO0FBQ0FHLFNBQU9xQixjQUFQLEdBQXlCRCxRQUFRRSxLQUFSLEdBQWdCRixRQUFRRSxLQUFSLEtBQWtCLE1BQWxDLEdBQTJDLEtBQXBFO0FBRUEsTUFBSUMsWUFBWUMsVUFBVUQsU0FBVixDQUFvQkUsV0FBcEIsRUFBaEI7QUFFQSxNQUFJQyxNQUFNO0FBRU5DLGVBQVcsQ0FGTDtBQUdOQyxjQUFVLENBSEo7QUFJTkMsZ0JBQVksQ0FKTjtBQUtOQyxnQkFBWSxJQUxOO0FBTU5DLGtCQUFjLEdBTlI7QUFPTkMsb0JBQWlCaEMsT0FBTyxhQUFQLE1BQTBCaUMsU0FQckM7QUFTTkMsNEJBQXdCLGdDQUFTNUIsS0FBVCxFQUFnQjtBQUNwQyxVQUFJLFFBQU9BLEtBQVAsTUFBaUIsUUFBckIsRUFBK0I7QUFDM0JBLGdCQUFRNkIsS0FBS0MsU0FBTCxDQUFlOUIsS0FBZixDQUFSO0FBQ0g7O0FBQ0QsYUFBTytCLG1CQUFtQi9CLEtBQW5CLENBQVA7QUFDSCxLQWRLO0FBZ0JOZ0MsVUFBTSxjQUFTQyxPQUFULEVBQWtCQyxTQUFsQixFQUE2QkMsTUFBN0IsRUFBcUM7QUFFdkNDLFFBQUUsNEJBQUY7QUFDQUEsUUFBRSxrQkFBa0JGLFNBQXBCO0FBRUEsVUFBSUcsT0FBT2pCLEdBQVg7O0FBRUEsVUFBSSxDQUFDYyxTQUFMLEVBQWdCO0FBQ1o7QUFDSCxPQVRzQyxDQVd2Qzs7O0FBQ0EsVUFBSSxPQUFPRCxPQUFQLElBQWtCLFFBQXRCLEVBQWdDO0FBQzVCLFlBQUl4QyxRQUFRLEVBQVo7O0FBQ0EsYUFBSyxJQUFJNkMsQ0FBVCxJQUFjTCxPQUFkLEVBQXVCO0FBQ25CeEMsZ0JBQU04QyxJQUFOLENBQVdELElBQUksR0FBSixHQUFVLEtBQUtWLHNCQUFMLENBQTRCSyxRQUFRSyxDQUFSLENBQTVCLENBQXJCO0FBQ0g7O0FBQ0RMLGtCQUFVeEMsTUFBTStDLElBQU4sQ0FBVyxHQUFYLENBQVY7QUFDSDs7QUFFREosUUFBRSxnQkFBZ0JILE9BQWxCOztBQUVBLFVBQUlJLEtBQUtYLGNBQVQsRUFBeUI7QUFDckI7QUFDQTtBQUNBUyxpQkFBU0EsVUFBVU0sTUFBbkI7QUFDQU4sZUFBTyxhQUFQLEVBQXNCRixPQUF0QixFQUErQkMsVUFBVXJDLE9BQVYsQ0FBbUIsc0JBQW5CLEVBQTJDLElBQTNDLENBQS9CO0FBQ0gsT0FMRCxNQU1LLElBQUlxQyxTQUFKLEVBQWU7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFJUSxJQUFJLElBQUlDLElBQUosR0FBV0MsT0FBWCxFQUFSO0FBQ0EsWUFBSUMsSUFBSSxFQUFFUixLQUFLaEIsU0FBZjtBQUNBLFlBQUl5QixjQUFjQyxTQUFTQyxjQUFULENBQXdCYixNQUF4QixDQUFsQixDQVBnQixDQU9tQztBQUNuRDs7QUFDQSxZQUFJVyxXQUFKLEVBQWlCO0FBQ2JBLHNCQUFZRyxZQUFaLENBQXlCLEtBQXpCLEVBQWdDZixVQUFVckMsT0FBVixDQUFtQixNQUFuQixFQUEyQixFQUEzQixJQUFrQyxHQUFsQyxHQUF3QzZDLENBQXhDLEdBQTRDRyxDQUE1QyxHQUFnRCxHQUFoRCxHQUFzRFosT0FBdEY7QUFDSCxTQUZELE1BR0s7QUFDRFEsaUJBQU85QyxRQUFQLEdBQWtCdUMsVUFBVXJDLE9BQVYsQ0FBbUIsTUFBbkIsRUFBMkIsRUFBM0IsSUFBa0MsR0FBbEMsR0FBd0M2QyxDQUF4QyxHQUE0Q0csQ0FBNUMsR0FBZ0QsR0FBaEQsR0FBc0RaLE9BQXhFO0FBQ0g7QUFDSjs7QUFFREcsUUFBRSx5QkFBRjtBQUNILEtBOURLO0FBZ0VOYyxhQUFTLGlCQUFTQyxRQUFULEVBQW1CQyxZQUFuQixFQUFpQ0MsS0FBakMsRUFBd0M7QUFDN0MsVUFBSSxPQUFPRixRQUFQLEtBQW9CLFVBQXhCLEVBQW9DO0FBQ2hDRyxjQUFNLDZCQUFOO0FBQ0g7O0FBQ0QsVUFBSSxPQUFPRixZQUFQLEtBQXdCLFFBQTVCLEVBQXNDO0FBQ2xDRSxjQUFNLCtCQUFOO0FBQ0g7O0FBRURsQixRQUFFLG1DQUFGO0FBQ0FBLFFBQUUsa0JBQWtCZSxTQUFTSSxJQUFULEdBQWdCSixTQUFTSSxJQUF6QixHQUFnQyxvQkFBbEQsQ0FBRjtBQUNBbkIsUUFBRSxxQkFBcUJnQixZQUF2QjtBQUVBLFVBQUlmLE9BQU9qQixHQUFYOztBQUVBLFVBQUlpQixLQUFLWCxjQUFULEVBQXlCO0FBRXJCO0FBQ0E7QUFFQSxZQUFJeUIsUUFBSixFQUFjO0FBRVYsY0FBSWQsS0FBS2IsVUFBVCxFQUFxQjtBQUNqQjtBQUNBLGdCQUFJOUIsT0FBTyxrQkFBUCxDQUFKLEVBQWlDO0FBQzdCQSxxQkFBTyxxQkFBUCxFQUE4QixTQUE5QixFQUF5QzJDLEtBQUtiLFVBQTlDLEVBQTBELEtBQTFEO0FBQ0gsYUFGRCxNQUdLO0FBQ0Q7QUFDQTlCLHFCQUFPLGFBQVAsRUFBc0IsV0FBdEIsRUFBbUMyQyxLQUFLYixVQUF4QztBQUNIO0FBQ0osV0FYUyxDQWFWOzs7QUFDQWEsZUFBS2IsVUFBTCxHQUFrQixVQUFTZ0MsR0FBVCxFQUFjO0FBQzVCO0FBQ0E7QUFDQSxnQkFBSUEsSUFBSUMsTUFBSixLQUFlTCxZQUFuQixFQUFpQztBQUM3QixrQkFBSU0sZ0JBQWdCLElBQUlDLE1BQUosQ0FBVyxZQUFZUCxZQUFaLEdBQTJCLEdBQXRDLEVBQTJDLEdBQTNDLENBQXBCOztBQUNBLGtCQUFJLENBQUNNLGNBQWNFLElBQWQsQ0FBbUJKLElBQUlDLE1BQXZCLENBQUwsRUFBcUM7QUFDakMsdUJBQU8sS0FBUDtBQUNIO0FBQ0o7O0FBRURyQixjQUFFLGdDQUFGO0FBQ0FBLGNBQUUsYUFBYW9CLElBQUlLLElBQW5CO0FBQ0F6QixjQUFFLHFCQUFxQmdCLFlBQXZCO0FBQ0FELHFCQUFTSyxHQUFUO0FBQ0gsV0FkRDtBQWVIOztBQUVELFlBQUk5RCxPQUFPLGtCQUFQLENBQUosRUFBZ0M7QUFDNUJBLGlCQUFPLGtCQUFQLEVBQTJCLFNBQTNCLEVBQXNDMkMsS0FBS2IsVUFBM0MsRUFBdUQsS0FBdkQ7QUFDSCxTQUZELE1BR0s7QUFDRDtBQUNBOUIsaUJBQU8sYUFBUCxFQUFzQixXQUF0QixFQUFtQzJDLEtBQUtiLFVBQXhDO0FBQ0g7QUFFSixPQTVDRCxNQTZDSztBQUVEO0FBQ0E7QUFDQVksVUFBRSwrQkFBRjs7QUFFQSxZQUFJQyxLQUFLZCxVQUFULEVBQXFCO0FBQ2pCdUMsd0JBQWN6QixLQUFLZCxVQUFuQjtBQUNBYyxlQUFLZCxVQUFMLEdBQWtCLElBQWxCO0FBQ0g7O0FBRUQsWUFBSSxPQUFPOEIsS0FBUCxLQUFpQixXQUFyQixFQUFrQztBQUM5QkEsa0JBQVFoQixLQUFLWixZQUFiO0FBQ0g7O0FBRUQsWUFBSTBCLFFBQUosRUFBYztBQUVWRSxrQkFBU0EsVUFBVTFCLFNBQVYsR0FBc0IwQixLQUF0QixHQUE4QixHQUF2QztBQUVBaEIsZUFBS2QsVUFBTCxHQUFrQndDLFlBQVksWUFBVTtBQUNwQyxnQkFBSUMsT0FBT2pCLFNBQVNwRCxRQUFULENBQWtCcUUsSUFBN0I7QUFDQSxnQkFBSUMsS0FBSyxTQUFUOztBQUNBLGdCQUFJRCxTQUFTM0IsS0FBS2YsUUFBZCxJQUEwQjJDLEdBQUdMLElBQUgsQ0FBUUksSUFBUixDQUE5QixFQUE2QztBQUN6QzNCLG1CQUFLZixRQUFMLEdBQWdCMEMsSUFBaEI7QUFDQSxrQkFBSUgsT0FBT0csS0FBS25FLE9BQUwsQ0FBYW9FLEVBQWIsRUFBaUIsRUFBakIsQ0FBWDtBQUNBN0IsZ0JBQUUsZ0NBQUY7QUFDQUEsZ0JBQUUsYUFBYXlCLElBQWY7QUFDQXpCLGdCQUFFLHFCQUFxQmdCLFlBQXZCO0FBQ0FELHVCQUFTO0FBQUVVLHNCQUFNQTtBQUFSLGVBQVQ7QUFDSDtBQUNKLFdBWGlCLEVBV2ZSLEtBWGUsQ0FBbEI7QUFZSDtBQUVKO0FBQ0o7QUE3SkssR0FBVjtBQWlLQTs7Ozs7O0FBS0EsTUFBSWEsZ0JBQWdCO0FBRWhCQywwQkFBc0IsRUFGTjtBQUloQkMsU0FBSyxlQUFXO0FBQ1poQyxRQUFFLHlDQUFGLEVBRFksQ0FHWjs7QUFDQSxXQUFLK0Isb0JBQUwsR0FBNEIsS0FBS0UsV0FBTCxHQUFtQkMsWUFBbkIsQ0FBZ0MsU0FBaEMsQ0FBNUIsQ0FKWSxDQU1aOztBQUNBLFVBQUlDLGVBQWUsS0FBS0MsYUFBTCxDQUFtQixLQUFLTCxvQkFBeEIsQ0FBbkI7O0FBQ0FJLG1CQUFhLE9BQWIsSUFBd0IsY0FBeEI7QUFDQUEsbUJBQWEsZUFBYixJQUFnQyxLQUFoQztBQUNBQSxtQkFBYSxlQUFiLElBQWdDLElBQWhDOztBQUNBLFdBQUtGLFdBQUwsR0FBbUJwQixZQUFuQixDQUFnQyxTQUFoQyxFQUEyQyxLQUFLd0IsVUFBTCxDQUFnQkYsWUFBaEIsQ0FBM0M7QUFDSCxLQWhCZTtBQWtCaEJHLGFBQVMsbUJBQVc7QUFDaEJ0QyxRQUFFLDZCQUFGOztBQUNBLFdBQUtpQyxXQUFMLEdBQW1CcEIsWUFBbkIsQ0FBZ0MsU0FBaEMsRUFBMkMsS0FBS2tCLG9CQUFoRDtBQUNILEtBckJlO0FBdUJoQkUsaUJBQWEsdUJBQVc7QUFDcEIsVUFBSU0sS0FBSzVCLFNBQVM2QixhQUFULENBQXVCLHFCQUF2QixDQUFUOztBQUNBLFVBQUksQ0FBQ0QsRUFBTCxFQUFTO0FBQ0xBLGFBQUs1QixTQUFTOEIsYUFBVCxDQUF1QixNQUF2QixDQUFMO0FBQ0FGLFdBQUcxQixZQUFILENBQWdCLE1BQWhCLEVBQXdCLFVBQXhCO0FBQ0EwQixXQUFHMUIsWUFBSCxDQUFnQixTQUFoQixFQUEyQixtQkFBM0I7QUFDQUYsaUJBQVMrQixJQUFULENBQWNDLFdBQWQsQ0FBMEJKLEVBQTFCO0FBQ0g7O0FBQ0QsYUFBT0EsRUFBUDtBQUNILEtBaENlO0FBa0NoQkYsZ0JBQVksb0JBQVNPLEtBQVQsRUFBZTtBQUN2QixVQUFJQyxRQUFRLEVBQVo7O0FBQ0EsV0FBSyxJQUFJbEYsR0FBVCxJQUFnQmlGLEtBQWhCLEVBQXVCO0FBQ25CQyxjQUFNMUMsSUFBTixDQUFXeEMsTUFBTSxHQUFOLEdBQVlpRixNQUFNakYsR0FBTixDQUF2QjtBQUNIOztBQUNELGFBQU9rRixNQUFNekMsSUFBTixDQUFXLElBQVgsQ0FBUDtBQUNILEtBeENlO0FBMENoQmdDLG1CQUFlLHVCQUFTVSxVQUFULEVBQW9CO0FBQy9CLFVBQUlELFFBQVFDLFdBQVdDLEtBQVgsQ0FBaUIsR0FBakIsQ0FBWjtBQUNBLFVBQUlDLE1BQU0sRUFBVjtBQUNBSCxZQUFNSSxPQUFOLENBQWMsVUFBU0MsSUFBVCxFQUFlO0FBQ3pCQSxlQUFPQSxLQUFLQyxJQUFMLEVBQVA7QUFDQSxZQUFJQyxLQUFLRixLQUFLSCxLQUFMLENBQVcsR0FBWCxDQUFUO0FBQ0FDLFlBQUlJLEdBQUcsQ0FBSCxDQUFKLElBQWFBLEdBQUcsQ0FBSCxDQUFiO0FBQ0gsT0FKRDtBQUtBLGFBQU9KLEdBQVA7QUFDSDtBQW5EZSxHQUFwQjtBQXNEQSxNQUFJSyxZQUFZO0FBRVpDLGFBQVMsbUJBQUFDLENBQVEsdUNBQVIsRUFBMkJDLE9BRnhCO0FBR1pDLHdCQUFvQixHQUhSO0FBSVpDLG1CQUFlLEdBSkg7QUFLWkMsb0JBQWdCLEdBTEo7QUFNWkMsZ0JBQVksR0FOQTtBQU9aQyxhQUFTLElBUEc7QUFRWkMsWUFBUSxJQVJJO0FBU1pDLGFBQVMsSUFURztBQVVaQyxrQkFBYyxJQVZGO0FBV1pDLGNBQVUsSUFYRTtBQVlaQyxhQUFVLG1CQUFtQjFDLElBQW5CLENBQXdCM0MsU0FBeEIsQ0FaRTtBQWFac0YsVUFBTyxZQUFZM0MsSUFBWixDQUFpQjNDLFNBQWpCLENBYks7QUFjWnVGLGFBQVUsVUFBVTVDLElBQVYsQ0FBZTNDLFNBQWYsQ0FkRTtBQWVad0YsY0FBVyxpRUFBaUU3QyxJQUFqRSxDQUFzRTNDLFNBQXRFLENBZkM7QUFnQlp5RixhQUFTLDJCQWhCRztBQWlCWkMsZ0JBQVksMkNBakJBO0FBa0JadkYsU0FBS0EsR0FsQk87QUFvQlp3RixjQUFVO0FBQ05DLGFBQU8sT0FERDtBQUVOQyxhQUFPLE9BRkQ7QUFHTkMsYUFBTyxPQUhEO0FBSU5DLGFBQU8sT0FKRDtBQUtOQyxhQUFPLE9BTEQ7QUFNTkMsYUFBTyxPQU5EO0FBT05DLGFBQU8sT0FQRDtBQVFOQyxhQUFPLE9BUkQ7QUFTTkMsYUFBTyxPQVREO0FBVU5DLGFBQU8sT0FWRDtBQVdOQyxhQUFPLE9BWEQ7QUFZTkMsWUFBTSxnQkFBVztBQUNiLGFBQUtDLGlCQUFMLEdBQXlCLENBQUMsS0FBS1osS0FBTixFQUFhLEtBQUtDLEtBQWxCLEVBQXlCLEtBQUtDLEtBQTlCLEVBQXFDLEtBQUtDLEtBQTFDLEVBQWlELEtBQUtDLEtBQXRELEVBQTZELEtBQUtDLEtBQWxFLEVBQXlFLEtBQUtDLEtBQTlFLEVBQXFGLEtBQUtDLEtBQTFGLEVBQWlHLEtBQUtDLEtBQXRHLEVBQTZHLEtBQUtDLEtBQWxILEVBQXlILEtBQUtDLEtBQTlILENBQXpCO0FBQ0EsZUFBTyxJQUFQO0FBQ0g7QUFmSyxNQWdCUkMsSUFoQlEsRUFwQkU7QUFzQ1p6RyxvQkFBZ0JyQixPQUFPcUIsY0F0Q1g7QUF3Q1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBMkcsa0JBQWMsMEJBbERGO0FBbURaQyxvQkFBZ0IsNEJBbkRKO0FBb0RaQyxvQkFBZ0IsNEJBcERKO0FBcURaQyxzQkFBa0IsOEJBckROO0FBc0RaQyxnQkFBWSx3QkF0REE7QUF1RFpDLDRCQUF3QixrQkF2RFo7QUF3RFpDLGlCQUFhLE9BeEREO0FBMkRaO0FBRUFSLFVBQU0sY0FBU1MsV0FBVCxFQUFzQjtBQUN4QixXQUFLNUIsUUFBTCxHQUFnQjRCLFdBQWhCO0FBQ0gsS0EvRFc7QUFpRVpDLFVBQU0sY0FBU0MsTUFBVCxFQUFpQjtBQUVuQixVQUFJOUYsT0FBTyxJQUFYLENBRm1CLENBSW5CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFVBQUkrRixjQUFjLEtBQUtDLE9BQUwsQ0FBYUYsT0FBTyxhQUFQLENBQWIsQ0FBbEI7QUFDQSxVQUFJRyxrQkFBa0JILE9BQU8saUJBQVAsQ0FBdEI7QUFDQSxVQUFJSSxXQUFXLEtBQUtGLE9BQUwsQ0FBYUYsT0FBTyxLQUFQLENBQWIsQ0FBZjtBQUNBLFdBQUtLLG9CQUFMLEdBQTRCTCxPQUFPLHNCQUFQLENBQTVCOztBQUVBLFVBQUksQ0FBQ0ksUUFBTCxFQUFlO0FBQ2IsY0FBTSxJQUFJRSxTQUFKLENBQWMsa0NBQWQsQ0FBTjtBQUNEOztBQUVELFVBQUksT0FBT04sT0FBTyxPQUFQLENBQVAsS0FBMkIsV0FBL0IsRUFBNEM7QUFDeEMsYUFBS3BILGNBQUwsR0FBdUJvSCxPQUFPLE9BQVAsTUFBb0IsSUFBcEIsSUFBNEJBLE9BQU8sT0FBUCxLQUFtQixNQUF0RTtBQUNIOztBQUNELFVBQUksT0FBT0EsT0FBTyx3QkFBUCxDQUFQLEtBQTRDLFdBQWhELEVBQTZEO0FBQ3pELGFBQUtPLHNCQUFMLEdBQStCUCxPQUFPLHdCQUFQLE1BQXFDLElBQXJDLElBQTZDQSxPQUFPLHdCQUFQLEtBQW9DLE1BQWhIO0FBQ0g7O0FBQ0QsVUFBSSxPQUFPQSxPQUFPLFlBQVAsQ0FBUCxLQUFnQyxXQUFwQyxFQUFpRDtBQUM3QyxhQUFLUSxVQUFMLEdBQW1CUixPQUFPLFlBQVAsTUFBeUIsSUFBekIsSUFBaUNBLE9BQU8sWUFBUCxLQUF3QixNQUE1RTtBQUNIOztBQUNELFVBQUksT0FBT0EsT0FBTyxpQkFBUCxDQUFQLEtBQXFDLFdBQXpDLEVBQXNEO0FBQ2xELGFBQUtTLGVBQUwsR0FBdUJULE9BQU8saUJBQVAsQ0FBdkI7QUFDSDs7QUFDRCxVQUFJLFFBQU9BLE9BQU8sc0JBQVAsQ0FBUCxNQUEwQyxRQUE5QyxFQUF3RDtBQUNwRCxhQUFLVSxvQkFBTCxHQUE0QmhILEtBQUtDLFNBQUwsQ0FBZXFHLE9BQU8sc0JBQVAsQ0FBZixDQUE1QjtBQUNBLGFBQUtVLG9CQUFMLEdBQTRCLEtBQUtBLG9CQUFMLENBQTBCaEosT0FBMUIsQ0FBa0MsSUFBbEMsRUFBd0MsRUFBeEMsQ0FBNUI7QUFDSCxPQUhELE1BR08sSUFBSSxPQUFPc0ksT0FBTyxzQkFBUCxDQUFQLEtBQTBDLFdBQTlDLEVBQTJEO0FBQzlEL0YsVUFBRSxzRUFBc0UrRixPQUFPLHNCQUFQLENBQXhFO0FBQ0g7O0FBQ0QsV0FBS1csUUFBTCxHQUFpQlgsT0FBTyxXQUFQLE1BQXdCeEcsU0FBekM7QUFDQSxXQUFLb0gsU0FBTCxHQUFpQlosT0FBTyxXQUFQLEtBQXVCcEYsU0FBU2lHLElBQWpELENBakRtQixDQW1EbkI7O0FBQ0EsVUFBSSxLQUFLRixRQUFMLElBQWlCWCxPQUFPLFFBQVAsTUFBcUJ4RyxTQUF0QyxLQUFvRHNILE1BQU1DLFNBQVNmLE9BQU8sUUFBUCxDQUFULEVBQTJCLEVBQTNCLENBQU4sS0FBeUNBLE9BQU8sUUFBUCxLQUFvQixDQUFqSCxDQUFKLEVBQXlIO0FBQ3JILGNBQU0sSUFBSWdCLEtBQUosQ0FBVSw0QkFBNEJoQixPQUFPLFFBQVAsQ0FBNUIsR0FBK0Msc0NBQXpELENBQU47QUFDSDs7QUFFRC9GLFFBQUUsOERBQUY7QUFDQUEsUUFBRStGLE1BQUY7O0FBRUEsVUFBSSxDQUFDSSxRQUFMLEVBQWU7QUFDWCxjQUFNLElBQUlZLEtBQUosQ0FBVSxrQkFBVixDQUFOO0FBQ0g7O0FBRUQsVUFBSUMsY0FBYyxPQUFPakIsT0FBTyxhQUFQLENBQVAsS0FBaUMsV0FBakMsR0FBK0MsS0FBS3ZCLFFBQUwsQ0FBY0MsS0FBN0QsR0FBcUVzQixPQUFPLGFBQVAsQ0FBdkY7O0FBQ0EsVUFBSSxLQUFLa0IsT0FBTCxDQUFhRCxXQUFiLEVBQTBCLEtBQUt4QyxRQUFMLENBQWNhLGlCQUF4QyxNQUErRCxDQUFDLENBQXBFLEVBQXVFO0FBQ25FLGNBQU0sSUFBSTBCLEtBQUosQ0FBVSxvQ0FBb0NDLFdBQTlDLENBQU47QUFDSDs7QUFFRGIsa0JBQWFBLFNBQVNlLE9BQVQsQ0FBaUIsR0FBakIsSUFBd0IsQ0FBeEIsR0FBNEIsR0FBNUIsR0FBa0MsR0FBL0M7O0FBQ0EsVUFBSWxCLFdBQUosRUFBaUI7QUFDYkcsb0JBQVksa0JBQWtCeEcsbUJBQW1CcUcsV0FBbkIsQ0FBbEIsR0FBb0QsR0FBaEU7QUFDSDs7QUFDREcsa0JBQVksZ0JBQWdCeEcsbUJBQW1CZ0IsU0FBU3BELFFBQVQsQ0FBa0JDLElBQWxCLENBQXVCQyxPQUF2QixDQUErQixNQUEvQixFQUF1QyxFQUF2QyxDQUFuQixDQUFoQixHQUFpRixHQUE3RjtBQUNBMEksa0JBQWEsS0FBS0csc0JBQUwsS0FBZ0MsSUFBaEMsR0FBdUMsNkJBQXZDLEdBQXVFLEVBQXBGO0FBQ0FILGtCQUFZLGVBQWUsS0FBS2xDLFFBQXBCLEdBQStCLEdBQTNDO0FBQ0FrQyxrQkFBYSxPQUFPSixPQUFPLFdBQVAsQ0FBUCxLQUErQixXQUEvQixHQUE2QyxlQUFlcEcsbUJBQW1Cb0csT0FBTyxXQUFQLENBQW5CLENBQWYsR0FBeUQsR0FBdEcsR0FBNEcsRUFBekg7QUFDQUksa0JBQVksa0JBQWtCYSxXQUE5Qjs7QUFDQSxVQUFJLEtBQUtySSxjQUFULEVBQXlCO0FBQ3JCd0gsb0JBQVksYUFBWjtBQUNIOztBQUNELFVBQUksS0FBS0ksVUFBVCxFQUFxQjtBQUNqQkosb0JBQVksa0JBQVo7QUFDSDs7QUFDRCxVQUFJLEtBQUtNLG9CQUFULEVBQStCO0FBQzNCTixvQkFBWSw2QkFBNkJnQixVQUFVLEtBQUtWLG9CQUFmLENBQXpDO0FBQ0g7O0FBQ0QsVUFBSSxLQUFLRCxlQUFULEVBQTBCO0FBQ3RCTCxvQkFBWSx3QkFBd0IsS0FBS0ssZUFBekM7QUFDSDs7QUFFREwsa0JBQVksaUJBQWlCLEtBQUs3QyxPQUFsQztBQUVBLFVBQUlqQyxTQUFTOEUsU0FBUzFJLE9BQVQsQ0FBaUIsc0JBQWpCLEVBQXlDLElBQXpDLENBQWI7QUFDQSxVQUFJMkosYUFBYSxLQUFLQyxtQkFBTCxDQUF5QnRCLE9BQU8sUUFBUCxDQUF6QixDQUFqQjtBQUNBLFVBQUl1QixTQUFTO0FBQ1QsbUJBQVc7QUFDUCxzQkFBWSxPQURMO0FBRVAsaUJBQU8sS0FGQTtBQUdQLGtCQUFRLEtBSEQ7QUFJUCxvQkFBVSxLQUpIO0FBS1AsbUJBQVMsS0FMRjtBQU1QLHFCQUFXLElBTko7QUFPUCxxQkFBVyxPQVBKO0FBUVAsOEJBQW9CLE1BUmI7QUFTUCxxQkFBVyxHQVRKO0FBVVAsNEJBQWtCLEdBVlg7QUFXUCwwQkFBZ0IsR0FYVDtBQVlQLG9CQUFVLG1CQVpIO0FBYVAsd0JBQWM7QUFiUCxTQURGO0FBZ0JULG1CQUFXLEtBQUtaLFFBQUwsR0FBZ0IsRUFBaEIsR0FBcUI7QUFDNUIsc0JBQVksVUFEZ0I7QUFFNUIsaUJBQU9VLFdBQVdHLEdBRlU7QUFHNUIsa0JBQVFILFdBQVdJLElBSFM7QUFJNUIscUJBQVc7QUFKaUIsU0FoQnZCO0FBc0JULGtCQUFVLEtBQUtkLFFBQUwsR0FBZ0IsRUFBaEIsR0FBcUI7QUFDM0Isb0JBQVUsS0FEaUI7QUFFM0IsOEJBQW9CLE1BRk87QUFHM0IscUJBQVc7QUFIZ0IsU0F0QnRCO0FBMkJULHdCQUFnQjtBQUNaLHNCQUFZLFVBREE7QUFFWixpQkFBTyxPQUZLO0FBR1osbUJBQVMsT0FIRztBQUlaLG1CQUFTLE1BSkc7QUFLWixvQkFBVSxNQUxFO0FBTVosOEJBQW9CLFNBQVMsS0FBS25DLFVBQWQsR0FBMkIsNkJBTm5DO0FBT1osaUNBQXVCLFdBUFg7QUFRWixvQkFBVSxTQVJFO0FBU1oscUJBQVc7QUFUQztBQTNCUCxPQUFiOztBQXdDQSxVQUFJa0QsZUFBZSxTQUFTQyxhQUFULEdBQXlCO0FBQ3hDLFlBQUl6SCxLQUFLNkQsTUFBVCxFQUFpQjtBQUViLGNBQUk2RCxPQUFPLEVBQVg7O0FBRUEsY0FBSTFILEtBQUtvRSxRQUFULEVBQW1CO0FBQ2ZzRCxtQkFBTzFILEtBQUsySCxtQkFBTCxFQUFQO0FBQ0gsV0FGRCxNQUVPO0FBQ0hELG1CQUFPMUgsS0FBS29ILG1CQUFMLEVBQVA7QUFDSDs7QUFFRHBILGVBQUs0RCxPQUFMLENBQWFnRSxLQUFiLENBQW1CLEtBQW5CLElBQTRCRixLQUFLSixHQUFqQztBQUNBdEgsZUFBSzRELE9BQUwsQ0FBYWdFLEtBQWIsQ0FBbUIsTUFBbkIsSUFBNkJGLEtBQUtILElBQWxDO0FBQ0F2SCxlQUFLNEQsT0FBTCxDQUFhZ0UsS0FBYixDQUFtQixPQUFuQixJQUE4QkYsS0FBS0csV0FBbkM7QUFDQTdILGVBQUs2RCxNQUFMLENBQVkrRCxLQUFaLENBQWtCLFFBQWxCLElBQThCRixLQUFLSSxZQUFuQztBQUNBOUgsZUFBSzZELE1BQUwsQ0FBWStELEtBQVosQ0FBa0IsT0FBbEIsSUFBNkJGLEtBQUtHLFdBQWxDO0FBRUg7QUFDSixPQWxCRDs7QUFvQkEsVUFBSSxLQUFLcEIsUUFBVCxFQUFtQjtBQUNmO0FBQ0FZLGVBQU8sU0FBUCxFQUFrQixPQUFsQixJQUE2QixNQUE3QjtBQUNBQSxlQUFPLFNBQVAsRUFBa0IsUUFBbEIsSUFBOEJGLFdBQVdXLFlBQXpDO0FBQ0FULGVBQU8sUUFBUCxFQUFpQixPQUFqQixJQUE0QixNQUE1QjtBQUNBQSxlQUFPLFFBQVAsRUFBaUIsUUFBakIsSUFBNkJGLFdBQVdXLFlBQXhDO0FBQ0FULGVBQU8sUUFBUCxFQUFpQixRQUFqQixJQUE2QixNQUE3QjtBQUNBQSxlQUFPLFFBQVAsRUFBaUIsWUFBakIsSUFBaUMsTUFBakM7QUFDQUEsZUFBTyxjQUFQLEVBQXVCLFNBQXZCLElBQW9DLE1BQXBDLENBUmUsQ0FVZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsWUFBSSxLQUFLakQsUUFBVCxFQUFtQjtBQUNmaUQsaUJBQU8sUUFBUCxFQUFpQixPQUFqQixJQUE0QixLQUE1QjtBQUNBQSxpQkFBTyxRQUFQLEVBQWlCLFdBQWpCLElBQWdDLE1BQWhDO0FBQ0g7QUFDSixPQXBCRCxNQXFCSyxJQUFJLEtBQUtqRCxRQUFULEVBQW1CO0FBQ3BCLFlBQUkyRCxhQUFhLEtBQUtKLG1CQUFMLEVBQWpCLENBRG9CLENBRXBCOztBQUNBTixlQUFPLFNBQVAsRUFBa0IsVUFBbEIsSUFBZ0MsVUFBaEM7QUFDQUEsZUFBTyxTQUFQLEVBQWtCLEtBQWxCLElBQTJCLEdBQTNCO0FBQ0FBLGVBQU8sU0FBUCxFQUFrQixNQUFsQixJQUE0QixHQUE1QjtBQUNBQSxlQUFPLFNBQVAsRUFBa0IsT0FBbEIsSUFBNkJVLFdBQVdGLFdBQXhDO0FBQ0FSLGVBQU8sU0FBUCxFQUFrQixRQUFsQixJQUE4QlUsV0FBV0QsWUFBekM7QUFDQVQsZUFBTyxRQUFQLEVBQWlCLFVBQWpCLElBQStCLFVBQS9CO0FBQ0FBLGVBQU8sUUFBUCxFQUFpQixLQUFqQixJQUEwQixDQUExQjtBQUNBQSxlQUFPLFFBQVAsRUFBaUIsTUFBakIsSUFBMkIsQ0FBM0I7QUFDQUEsZUFBTyxRQUFQLEVBQWlCLE9BQWpCLElBQTRCVSxXQUFXRixXQUF2QztBQUNBUixlQUFPLFFBQVAsRUFBaUIsUUFBakIsSUFBNkJVLFdBQVdELFlBQXhDO0FBQ0FULGVBQU8sUUFBUCxFQUFpQixRQUFqQixJQUE2QixNQUE3QjtBQUNBQSxlQUFPLFFBQVAsRUFBaUIsWUFBakIsSUFBaUMsTUFBakM7QUFDQUEsZUFBTyxjQUFQLEVBQXVCLFNBQXZCLElBQW9DLE1BQXBDO0FBQ0gsT0EvTGtCLENBaU1uQjs7O0FBQ0EsVUFBSSxDQUFDLEtBQUtaLFFBQVYsRUFBb0I7QUFDaEIsWUFBSSxDQUFDLEtBQUszQyxPQUFWLEVBQW1CO0FBQ2YsZUFBS0EsT0FBTCxHQUFlcEQsU0FBUzhCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZjtBQUNBLGVBQUtzQixPQUFMLENBQWFsRCxZQUFiLENBQTBCLElBQTFCLEVBQWdDLG1CQUFoQztBQUNBRixtQkFBU2lHLElBQVQsQ0FBY2pFLFdBQWQsQ0FBMEIsS0FBS29CLE9BQS9CO0FBQ0g7O0FBQ0QsYUFBS0EsT0FBTCxDQUFhbEQsWUFBYixDQUEwQixPQUExQixFQUFtQyxpQkFBbkM7QUFDSCxPQXpNa0IsQ0EyTW5COzs7QUFDQSxVQUFJLENBQUMsS0FBS2dELE9BQVYsRUFBbUI7QUFDZixhQUFLQSxPQUFMLEdBQWVsRCxTQUFTOEIsYUFBVCxDQUF1QixLQUF2QixDQUFmO0FBQ0EsYUFBS29CLE9BQUwsQ0FBYWhELFlBQWIsQ0FBMEIsSUFBMUIsRUFBZ0MsbUJBQWhDLEVBRmUsQ0FJZjtBQUNBOztBQUNBLFlBQUksS0FBS3dELFFBQVQsRUFBbUI7QUFDZi9HLGlCQUFPMkssUUFBUCxDQUFnQixDQUFoQixFQUFtQixDQUFuQjtBQUNIOztBQUVELGFBQUt0QixTQUFMLENBQWVoRSxXQUFmLENBQTJCLEtBQUtrQixPQUFoQztBQUNIOztBQUVELFVBQUksQ0FBQyxLQUFLNkMsUUFBVixFQUFvQjtBQUNoQjtBQUNBO0FBQ0E7QUFDQXBKLGVBQU80SyxRQUFQLEdBQWtCVCxZQUFsQjtBQUNILE9BOU5rQixDQWdPbkI7OztBQUNBLFVBQUksQ0FBQyxLQUFLM0QsTUFBVixFQUFrQjtBQUNkLGFBQUtBLE1BQUwsR0FBY25ELFNBQVM4QixhQUFULENBQXVCLFFBQXZCLENBQWQ7QUFDQSxhQUFLcUIsTUFBTCxDQUFZakQsWUFBWixDQUF5QixJQUF6QixFQUErQixpQkFBL0I7QUFDQSxhQUFLZ0QsT0FBTCxDQUFhbEIsV0FBYixDQUF5QixLQUFLbUIsTUFBOUI7QUFDSDs7QUFDRCxXQUFLQSxNQUFMLENBQVlqRCxZQUFaLENBQXlCLEtBQXpCLEVBQWdDc0YsUUFBaEM7QUFDQSxXQUFLckMsTUFBTCxDQUFZakQsWUFBWixDQUF5QixXQUF6QixFQUFzQyxJQUF0QyxFQXZPbUIsQ0F1TzBCOztBQUM3QyxXQUFLaUQsTUFBTCxDQUFZakQsWUFBWixDQUF5QixhQUF6QixFQUF3QyxHQUF4QztBQUNBLFdBQUtpRCxNQUFMLENBQVlqRCxZQUFaLENBQXlCLFFBQXpCLEVBQW1DdUcsV0FBV2UsU0FBOUMsRUF6T21CLENBMk9uQjtBQUNBO0FBQ0E7O0FBQ0EsVUFBSWhDLFNBQVNlLE9BQVQsQ0FBaUIsY0FBakIsTUFBcUMsQ0FBQyxDQUExQyxFQUE2QztBQUMzQyxZQUFJLENBQUMsS0FBS1IsUUFBTixLQUFtQlgsT0FBTyxhQUFQLE1BQTBCLElBQTFCLElBQWtDQSxPQUFPLGFBQVAsTUFBMEJ4RyxTQUEvRSxLQUE2RixDQUFDLEtBQUt5RSxZQUF2RyxFQUFxSDtBQUNqSCxlQUFLQSxZQUFMLEdBQW9CckQsU0FBUzhCLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBcEI7QUFDQSxlQUFLdUIsWUFBTCxDQUFrQm5ELFlBQWxCLENBQStCLElBQS9CLEVBQXFDLGtCQUFyQztBQUNBLGVBQUttRCxZQUFMLENBQWtCbkQsWUFBbEIsQ0FBK0IsTUFBL0IsRUFBdUMsY0FBdkM7O0FBQ0EsZUFBS21ELFlBQUwsQ0FBa0JvRSxPQUFsQixHQUE0QixZQUFVO0FBQ2xDO0FBQ0EvRSxzQkFBVWdGLEtBQVYsR0FGa0MsQ0FHbEM7O0FBQ0EsZ0JBQUluQyxlQUFKLEVBQXFCO0FBQ2pCbEcsZ0JBQUUsdUJBQUY7QUFDQWtHLDhCQUFnQjtBQUNaLHlCQUFTN0MsVUFBVW1DO0FBRFAsZUFBaEI7QUFHSDtBQUNKLFdBVkQ7O0FBV0EsZUFBSzNCLE9BQUwsQ0FBYWxCLFdBQWIsQ0FBeUIsS0FBS3FCLFlBQTlCO0FBQ0gsU0FoQkQsTUFpQkssSUFBSSxDQUFDK0IsT0FBTyxhQUFQLENBQUQsSUFBMEIsS0FBSy9CLFlBQW5DLEVBQWlEO0FBQ2xELGVBQUtILE9BQUwsQ0FBYXlFLFdBQWIsQ0FBeUIsS0FBS3RFLFlBQTlCO0FBQ0g7QUFDRixPQW5Ra0IsQ0FxUW5COzs7QUFDQSxXQUFLLElBQUk5RCxDQUFULElBQWNvSCxNQUFkLEVBQXNCO0FBQ2xCLFlBQUkvRSxLQUFLLEtBQUtyQyxDQUFMLENBQVQ7O0FBQ0EsWUFBSXFDLEVBQUosRUFBUTtBQUNKLGVBQUssSUFBSWdHLENBQVQsSUFBY2pCLE9BQU9wSCxDQUFQLENBQWQsRUFBeUI7QUFDckIsZ0JBQUk7QUFDQXFDLGlCQUFHc0YsS0FBSCxDQUFTVSxDQUFULElBQWNqQixPQUFPcEgsQ0FBUCxFQUFVcUksQ0FBVixDQUFkO0FBQ0gsYUFGRCxDQUVFLE9BQU9DLENBQVAsRUFBVTtBQUNSO0FBQ0F4SSxnQkFBRXdJLENBQUY7QUFDSDtBQUNKO0FBQ0o7QUFDSjs7QUFDRCxVQUFJLEtBQUt4RSxZQUFMLEtBQXNCLEtBQUtHLElBQUwsSUFBYSxLQUFLQyxPQUF4QyxDQUFKLEVBQXNEO0FBQ2xEO0FBQ0EsWUFBSXFFLElBQUksS0FBS3pFLFlBQUwsQ0FBa0I5QixZQUFsQixDQUErQixPQUEvQixDQUFSO0FBQ0F1RyxhQUFNQSxJQUFJLElBQUosR0FBVyxFQUFqQjtBQUNBQSxhQUFLLHVCQUF1Qm5CLE9BQU90RCxZQUFQLENBQW9CLGtCQUFwQixDQUF2QixHQUFpRSxJQUF0RTtBQUNBeUUsYUFBSywwQkFBMEJuQixPQUFPdEQsWUFBUCxDQUFvQixxQkFBcEIsQ0FBMUIsR0FBdUUsR0FBNUU7QUFDQSxhQUFLQSxZQUFMLENBQWtCbkQsWUFBbEIsQ0FBK0IsT0FBL0IsRUFBd0M0SCxDQUF4QztBQUNIOztBQUVELFVBQUksQ0FBQyxLQUFLL0IsUUFBTixJQUFrQixDQUFDLEtBQUtyQyxRQUE1QixFQUFzQztBQUNsQztBQUNBb0Q7QUFDSDs7QUFFRCxVQUFJLEtBQUtwRCxRQUFMLElBQWlCL0csV0FBV0EsT0FBT2lLLEdBQXZDLEVBQTRDO0FBQ3hDO0FBQ0F6RixzQkFBY0UsR0FBZDtBQUNIOztBQUVELFVBQUksS0FBS3FDLFFBQUwsSUFBaUIsQ0FBQyxLQUFLcUMsUUFBM0IsRUFBcUM7QUFDakMsYUFBS2dDLFNBQUwsR0FBaUI3SyxTQUFTLFlBQVc7QUFDakNQLGlCQUFPMkssUUFBUCxDQUFnQixDQUFoQixFQUFtQixDQUFuQjtBQUNILFNBRmdCLEVBRWQsSUFGYyxDQUFqQjtBQUdBLGFBQUtTLFNBQUw7QUFDQXBMLGVBQU9xTCxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxLQUFLRCxTQUF2QztBQUNILE9BNVNrQixDQThTbkI7OztBQUNBLFVBQUksS0FBS3RDLG9CQUFULEVBQStCO0FBQzNCLGFBQUt3Qyx5QkFBTCxHQUFpQ25LLFdBQVcsWUFBVztBQUNuRCxjQUFJb0IsVUFBVSw2Q0FBNkNJLEtBQUttRyxvQkFBbEQsR0FBeUUsZ0JBQXZGO0FBQ0FuRyxlQUFLNEksV0FBTCxDQUFpQmhKLE9BQWpCLEVBQTBCYyxTQUFTcEQsUUFBVCxDQUFrQkMsSUFBNUM7QUFDQXlDLGVBQUtvSSxLQUFMO0FBQ0gsU0FKZ0MsRUFJOUIsS0FBS2pDLG9CQUp5QixDQUFqQztBQUtILE9BclRrQixDQXVUbkI7OztBQUNBcEgsVUFBSThCLE9BQUosQ0FBWSxTQUFTZ0kscUJBQVQsQ0FBK0IxSCxHQUEvQixFQUFtQztBQUMzQyxZQUFJMkgsU0FBUzNILElBQUkySCxNQUFKLElBQWMsaUJBQTNCOztBQUVBLFlBQUkzSCxJQUFJSyxJQUFKLEtBQWEsWUFBakIsRUFBK0I7QUFDM0IsY0FBSXhCLEtBQUttRyxvQkFBVCxFQUErQjVILGFBQWF5QixLQUFLMkkseUJBQWxCLEVBREosQ0FFM0I7O0FBQ0EsY0FBSUksVUFBVSxTQUFjLEVBQWQsRUFBa0JqRCxNQUFsQixDQUFkOztBQUNBLGlCQUFPaUQsUUFBUXJDLFNBQWY7QUFDQTNILGNBQUlZLElBQUosQ0FBU0gsS0FBS0MsU0FBTCxDQUFlO0FBQUV1SixrQkFBTSxnQkFBUjtBQUEwQkQscUJBQVNBO0FBQW5DLFdBQWYsQ0FBVCxFQUF1RTVILElBQUlDLE1BQTNFLEVBQW1GMEgsTUFBbkY7QUFDSCxTQU5ELE1BTU8sSUFBSTNILElBQUlLLElBQUosSUFBWSxPQUFoQixFQUF5QjtBQUM1QjtBQUNBNEIsb0JBQVVnRixLQUFWOztBQUVBLGNBQUluQyxlQUFKLEVBQXFCO0FBQ2pCQSw0QkFBZ0I7QUFDWix1QkFBUzdDLFVBQVVtQztBQURQLGFBQWhCO0FBR0g7QUFDSixTQVRNLE1BU0EsSUFBSXBFLElBQUlLLElBQUosSUFBWSxTQUFoQixFQUEyQjtBQUM5QjtBQUNBNEIsb0JBQVVnRixLQUFWO0FBQ0FuQywwQkFBZ0I7QUFDWixxQkFBUzdDLFVBQVVrQztBQURQLFdBQWhCO0FBR0gsU0FOTSxNQU1BLElBQUluRSxJQUFJSyxJQUFKLElBQVksVUFBaEIsRUFBNEI7QUFDakN5RSwwQkFBZ0I7QUFDZCxxQkFBUzdDLFVBQVVvQztBQURMLFdBQWhCO0FBR0QsU0FKTSxNQUlBLElBQUlyRSxJQUFJSyxJQUFKLElBQVksV0FBaEIsRUFBNkI7QUFDaEM7QUFDQTRCLG9CQUFVZ0YsS0FBVjtBQUNILFNBSE0sTUFHQSxJQUFJLE9BQU9qSCxJQUFJSyxJQUFYLEtBQW9CLFFBQXBCLElBQWdDTCxJQUFJSyxJQUFKLENBQVN5RixPQUFULENBQWlCLFFBQWpCLE1BQStCLENBQW5FLEVBQXNFO0FBQ3pFO0FBQ0EsY0FBSTdKLFFBQVErRCxJQUFJSyxJQUFKLENBQVNzQixLQUFULENBQWUsR0FBZixDQUFaO0FBQ0EsY0FBSW1HLFFBQVE3TCxNQUFNLENBQU4sQ0FBWjtBQUNBMkIsY0FBSVksSUFBSixDQUFTLGVBQWVzSixLQUF4QixFQUErQi9DLFFBQS9CLEVBQXlDNEMsTUFBekM7QUFDSCxTQUxNLE1BS0EsSUFBSTdDLG1CQUFtQjlFLElBQUlLLElBQXZCLElBQStCLE9BQU9MLElBQUlLLElBQVgsS0FBb0IsUUFBdkQsRUFBaUU7QUFFcEU7QUFDQSxjQUFJMEgsWUFBWSxFQUFoQjtBQUNBLGNBQUlDLENBQUo7QUFBQSxjQUFPdkcsUUFBUXpCLElBQUlLLElBQUosQ0FBU3NCLEtBQVQsQ0FBZSxHQUFmLENBQWYsQ0FKb0UsQ0FNcEU7O0FBQ0EsY0FBSXNHLHVCQUF1QixTQUF2QkEsb0JBQXVCLENBQVNDLEdBQVQsRUFBYztBQUNyQyxnQkFBSXRHLE1BQU1zRyxHQUFWOztBQUNBLGdCQUFJO0FBQ0E7QUFDQXRHLG9CQUFNdkQsS0FBSzhKLEtBQUwsQ0FBV0QsR0FBWCxDQUFOOztBQUNBLGtCQUFJLFFBQU90RyxHQUFQLE1BQWUsUUFBbkIsRUFBNkI7QUFDekIscUJBQUssSUFBSXJGLEdBQVQsSUFBZ0JxRixHQUFoQixFQUFxQjtBQUNqQkEsc0JBQUlyRixHQUFKLElBQVc2TCxVQUFVeEcsSUFBSXJGLEdBQUosQ0FBVixDQUFYO0FBQ0g7QUFDSjtBQUNKLGFBUkQsQ0FRRSxPQUFPNkssQ0FBUCxFQUFVO0FBQUU7QUFBYzs7QUFDNUIsbUJBQU94RixHQUFQO0FBQ0gsV0FaRDs7QUFjQSxlQUFLLElBQUl1RixJQUFFLENBQVgsRUFBY0EsSUFBRTFGLE1BQU00RyxNQUF0QixFQUE4QmxCLEdBQTlCLEVBQW1DO0FBQy9CYSxnQkFBSXZHLE1BQU0wRixDQUFOLEVBQVN4RixLQUFULENBQWUsR0FBZixDQUFKOztBQUNBLGdCQUFJcUcsRUFBRUssTUFBRixLQUFhLENBQWpCLEVBQW9CO0FBQ2hCTix3QkFBVUMsRUFBRSxDQUFGLENBQVYsSUFBa0JDLHFCQUFxQkssbUJBQW1CTixFQUFFLENBQUYsQ0FBbkIsQ0FBckIsQ0FBbEI7QUFDSDtBQUNKOztBQUNEbEQsMEJBQWdCaUQsU0FBaEI7QUFDSDtBQUNKLE9BakVELEVBaUVHOUgsTUFqRUg7QUFrRUgsS0EzYlc7QUE2YlpnSCxXQUFPLGlCQUFXO0FBRWQ7QUFDQSxVQUFJLEtBQUtoRSxRQUFMLElBQWlCL0csV0FBV0EsT0FBT2lLLEdBQXZDLEVBQTRDO0FBQ3hDekYsc0JBQWNRLE9BQWQ7QUFDSDs7QUFFRHRDLFFBQUUsbUNBQUYsRUFQYyxDQVFkOztBQUNBLFVBQUksS0FBSzhELE1BQVQsRUFBaUI7QUFDYixZQUFJN0QsT0FBTyxJQUFYOztBQUNBLFlBQUksS0FBSytELFlBQVQsRUFBdUI7QUFDbkIsZUFBS0gsT0FBTCxDQUFheUUsV0FBYixDQUF5QixLQUFLdEUsWUFBOUI7QUFDQSxlQUFLQSxZQUFMLEdBQW9CLElBQXBCO0FBQ0g7O0FBQ0QsYUFBSzJGLGNBQUw7QUFDSDs7QUFFRCxVQUFJLEtBQUt0RixRQUFULEVBQW1CO0FBQ2YvRyxlQUFPc00sbUJBQVAsQ0FBMkIsUUFBM0IsRUFBcUMsS0FBS2xCLFNBQTFDO0FBQ0g7QUFDSixLQWxkVztBQXFkWjtBQUVBaUIsb0JBQWdCLFNBQVNBLGNBQVQsQ0FBd0JFLGNBQXhCLEVBQXdDO0FBQ3BELFVBQUk1SixPQUFPLElBQVg7O0FBQ0EsVUFBSUEsS0FBSzZELE1BQVQsRUFBaUI7QUFDYixZQUFJLENBQUMrRixjQUFMLEVBQXFCO0FBQ2pCQSwyQkFBaUIsR0FBakI7QUFDSCxTQUZELE1BRU87QUFDSEEsNEJBQWtCLEdBQWxCO0FBQ0g7O0FBQ0Q1SixhQUFLNkQsTUFBTCxDQUFZK0QsS0FBWixDQUFrQmlDLE9BQWxCLEdBQTRCRCxjQUE1QjtBQUNBNUosYUFBSzZELE1BQUwsQ0FBWStELEtBQVosQ0FBa0JrQyxNQUFsQixHQUEyQixtQkFBbUJqRCxTQUFTK0MsaUJBQWlCLEdBQTFCLEVBQStCLEVBQS9CLENBQW5CLEdBQXdELEdBQW5GOztBQUNBLFlBQUlBLGtCQUFrQixHQUF0QixFQUEyQjtBQUN2QjVKLGVBQUs2RCxNQUFMLENBQVkrRCxLQUFaLENBQWtCaUMsT0FBbEIsR0FBNEIsQ0FBNUI7QUFDQTdKLGVBQUs2RCxNQUFMLENBQVkrRCxLQUFaLENBQWtCa0MsTUFBbEIsR0FBMkIsa0JBQTNCO0FBQ0E5SixlQUFLNkQsTUFBTCxDQUFZK0QsS0FBWixDQUFrQm1DLE9BQWxCLEdBQTRCLE1BQTVCO0FBQ0F4TCx1QkFBYXlMLGNBQWI7O0FBQ0EsY0FBSWhLLEtBQUs4RCxPQUFULEVBQWtCO0FBQ2Q5RCxpQkFBSzBHLFNBQUwsQ0FBZTJCLFdBQWYsQ0FBMkJySSxLQUFLOEQsT0FBaEM7QUFDSDs7QUFDRDlELGVBQUswRyxTQUFMLENBQWUyQixXQUFmLENBQTJCckksS0FBSzRELE9BQWhDO0FBQ0E1RCxlQUFLNEQsT0FBTCxDQUFheUUsV0FBYixDQUF5QnJJLEtBQUs2RCxNQUE5QjtBQUNBN0QsZUFBSzhELE9BQUwsR0FBZSxJQUFmO0FBQ0E5RCxlQUFLNkQsTUFBTCxHQUFjLElBQWQ7QUFDQTdELGVBQUs0RCxPQUFMLEdBQWUsSUFBZjtBQUNBLGlCQUFPLEtBQVA7QUFDSDs7QUFDRCxZQUFJb0csaUJBQWlCeEwsV0FBWSxVQUFTb0wsY0FBVCxFQUF5QjtBQUN0RCxpQkFBTyxZQUFXO0FBQ2Q1SixpQkFBSzBKLGNBQUwsQ0FBb0JFLGNBQXBCO0FBQ0gsV0FGRDtBQUdILFNBSitCLENBSTdCQSxjQUo2QixDQUFYLEVBSUQsRUFKQyxDQUFyQjtBQUtIO0FBQ0osS0F0Zlc7QUF3ZlpoQixpQkFBYSxxQkFBU3FCLFlBQVQsRUFBdUJDLFNBQXZCLEVBQWtDO0FBQzNDbkwsVUFBSVksSUFBSixDQUFTO0FBQ0wsaUJBQVN5RCxVQUFVdUMsV0FEZDtBQUVMLHVCQUFlc0U7QUFGVixPQUFULEVBR0dDLFNBSEg7QUFJSCxLQTdmVztBQStmWkMsd0JBQW9CLDRCQUFTQyxVQUFULEVBQXFCRixTQUFyQixFQUFnQ2pCLEtBQWhDLEVBQXVDNUMsc0JBQXZDLEVBQStEdkYsUUFBL0QsRUFBeUU7QUFFekY7QUFDQTtBQUNBO0FBRUEsVUFBSXpELE9BQU9pSyxHQUFQLElBQWNqSyxNQUFsQixFQUEwQjtBQUN0QjtBQUNBeUQsaUJBQVMsSUFBVDtBQUNBO0FBQ0g7O0FBRUQsVUFBSSxPQUFPbUksS0FBUCxLQUFpQixRQUFyQixFQUErQjtBQUMzQmhJLGNBQU0sMERBQU47QUFDQTtBQUNIOztBQUVELFVBQUksT0FBT0gsUUFBUCxLQUFvQixVQUF4QixFQUFvQztBQUNoQ0csY0FBTSw2REFBTjtBQUNBO0FBQ0g7O0FBR0QsVUFBSW9GLDJCQUEyQixJQUEvQixFQUFxQztBQUNqQyxZQUFJZ0UsYUFBYSx1S0FBakI7QUFDQXRLLFVBQUVzSyxVQUFGO0FBQ0FDLGNBQU1ELFVBQU47QUFDQXZKLGlCQUFTLElBQVQ7QUFDSCxPQUxELE1BTUs7QUFDRDtBQUNBL0IsWUFBSThCLE9BQUosQ0FBWSxTQUFTMEosMkJBQVQsQ0FBcUNwSixHQUFyQyxFQUF5QztBQUNqRCxjQUFJQSxJQUFJSyxJQUFKLENBQVN5RixPQUFULENBQWlCLFlBQWpCLE1BQW1DLENBQXZDLEVBQTBDO0FBQ3RDLGdCQUFJN0osUUFBUStELElBQUlLLElBQUosQ0FBU3NCLEtBQVQsQ0FBZSxHQUFmLENBQVo7QUFDQSxnQkFBSTBILFFBQVNwTixNQUFNLENBQU4sS0FBWTZMLEtBQXpCO0FBQ0FuSSxxQkFBUzBKLEtBQVQ7QUFDSDtBQUNKLFNBTkQsRUFNR0osVUFOSDtBQU9ILE9BdEN3RixDQXdDekY7OztBQUNBckwsVUFBSVksSUFBSixDQUFTLFdBQVdzSixLQUFwQixFQUEyQmlCLFNBQTNCO0FBQ0gsS0F6aUJXO0FBMmlCWjlDLHlCQUFxQiw2QkFBU3FELFlBQVQsRUFBdUI7QUFDeEMsVUFBSUMsVUFBVUMsWUFBZDtBQUNBLFVBQUlDLFVBQVVDLFlBQWQ7QUFDQSxVQUFJQyxXQUFKLEVBQWlCQyxZQUFqQjs7QUFFQSxVQUFJLEtBQUs5RyxPQUFULEVBQWtCO0FBQ2Q2RyxzQkFBZ0JwSyxTQUFTaUcsSUFBVCxDQUFjcUUsV0FBOUI7QUFDQUQsdUJBQWdCckssU0FBU2lHLElBQVQsQ0FBY3NFLFlBQTlCO0FBQ0gsT0FIRCxNQUdPO0FBQ0hILHNCQUFnQnpOLE9BQU82TixVQUF2QjtBQUNBSCx1QkFBZ0IxTixPQUFPOE4sV0FBdkI7QUFDSDs7QUFDRCxVQUFJQyxTQUFTLEtBQUszRSxRQUFMLElBQWlCZ0UsWUFBakIsR0FBZ0NBLFlBQWhDLEdBQStDWSxLQUFLQyxHQUFMLENBQVMsS0FBSzNILFVBQWQsRUFBMEJvSCxlQUFlLEVBQXpDLENBQTVEO0FBRUEsVUFBSVEsUUFBUUYsS0FBS0csR0FBTCxDQUFTLEtBQUsvSCxhQUFkLEVBQTZCcUgsY0FBYyxLQUFLdEgsa0JBQWhELENBQVo7QUFFQSxhQUFPO0FBQ0gsdUJBQWdCK0gsUUFBUSxJQURyQjtBQUVILHdCQUFnQkgsU0FBUyxJQUZ0QjtBQUdILHFCQUFnQkEsTUFIYjtBQUlILG1CQUFnQlYsT0FKYjtBQUtILG1CQUFnQkUsT0FMYjtBQU1ILGVBQWdCUyxLQUFLQyxHQUFMLENBQVMsQ0FBVCxFQUFZVixVQUFVL0QsU0FBUyxDQUFDa0UsZUFBZUssTUFBaEIsSUFBMEIsQ0FBbkMsRUFBc0MsRUFBdEMsQ0FBdEIsSUFBbUUsSUFOaEY7QUFPSCxnQkFBZ0JDLEtBQUtDLEdBQUwsQ0FBUyxDQUFULEVBQVl6RSxTQUFTLENBQUNpRSxjQUFjLEtBQUtySCxhQUFwQixJQUFxQyxDQUE5QyxFQUFpRCxFQUFqRCxDQUFaLElBQW9FO0FBUGpGLE9BQVA7QUFTSCxLQXBrQlc7QUFza0Jaa0UseUJBQXFCLCtCQUFVO0FBQzNCLFVBQUlELElBQUo7QUFFQSxVQUFJK0QsY0FBY0MsT0FBT0gsS0FBekI7QUFDQSxVQUFJSSxlQUFlRCxPQUFPTixNQUExQjtBQUNBLFVBQUlOLGNBQWN6TixPQUFPNk4sVUFBekI7QUFDQSxVQUFJSCxlQUFlMU4sT0FBTzhOLFdBQTFCO0FBRUEsVUFBSVMsYUFBYWIsZUFBZUQsV0FBaEM7O0FBRUEsVUFBSWMsVUFBSixFQUFnQjtBQUNabEUsZUFBTztBQUNILHlCQUFlK0QsY0FBYyxJQUQxQjtBQUVILDBCQUFnQjtBQUZiLFNBQVA7QUFJSCxPQUxELE1BS087QUFDSDtBQUNBL0QsZUFBTztBQUNILHlCQUFlb0QsY0FBYyxJQUQxQjtBQUVILDBCQUFnQjtBQUZiLFNBQVA7QUFJSCxPQXJCMEIsQ0FzQjNCOzs7QUFDQXBELFdBQUtKLEdBQUwsR0FBVyxHQUFYO0FBQ0FJLFdBQUtILElBQUwsR0FBWSxHQUFaO0FBQ0EsYUFBT0csSUFBUDtBQUNILEtBaG1CVztBQWttQlpWLGFBQVMsaUJBQVM2RSxDQUFULEVBQVlDLEtBQVosRUFBbUI7QUFDeEIsVUFBSSxLQUFLQyxTQUFULEVBQW9CO0FBQ2hCLGVBQU9DLEVBQUVoRixPQUFGLENBQVU2RSxDQUFWLEVBQWFDLEtBQWIsQ0FBUDtBQUNILE9BRkQsTUFHSyxJQUFJQSxLQUFKLEVBQVc7QUFDWixhQUFLLElBQUl4RCxJQUFFLENBQVgsRUFBY0EsSUFBRXdELE1BQU10QyxNQUF0QixFQUE4QmxCLEdBQTlCLEVBQW1DO0FBQy9CLGNBQUl3RCxNQUFNeEQsQ0FBTixLQUFZdUQsQ0FBaEIsRUFBbUI7QUFDZixtQkFBT3ZELENBQVA7QUFDSDtBQUNKO0FBQ0o7O0FBQ0QsYUFBTyxDQUFDLENBQVI7QUFDSCxLQTltQlc7QUFnbkJadEMsYUFBUyxpQkFBU2lHLEdBQVQsRUFBYztBQUNuQixVQUFJQSxHQUFKLEVBQVM7QUFDTCxZQUFJO0FBRUE7QUFDQUEsZ0JBQU1BLElBQUl6TyxPQUFKLENBQVksSUFBWixFQUFrQixNQUFsQixDQUFOO0FBQ0F5TyxnQkFBTUEsSUFBSXpPLE9BQUosQ0FBWSxJQUFaLEVBQWtCLE1BQWxCLENBQU4sQ0FKQSxDQU1BOztBQUNBLGNBQUk4RSxLQUFLNUIsU0FBUzhCLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVDtBQUNBRixhQUFHNEosU0FBSCxHQUFlRCxHQUFmO0FBQ0EsY0FBSUUsYUFBYTdKLEdBQUc4SixTQUFwQixDQVRBLENBV0E7O0FBQ0EsY0FBSSxDQUFDRCxVQUFMLEVBQWlCO0FBQ2JGLGtCQUFNQSxJQUFJek8sT0FBSixDQUFZLFVBQVosRUFBd0IsR0FBeEIsQ0FBTjtBQUNILFdBRkQsTUFHSztBQUNEeU8sa0JBQU1FLFVBQU47QUFDSDtBQUNKLFNBbEJELENBbUJBLE9BQU81RCxDQUFQLEVBQVU7QUFDTnhJLFlBQUUsMkJBQTJCd0ksQ0FBN0I7QUFDSDtBQUNKOztBQUNELGFBQU8wRCxHQUFQO0FBQ0g7QUExb0JXLEdBQWhCO0FBNm9CQTs7Ozs7O0FBTUEsV0FBU2hMLEtBQVQsQ0FBZXJCLE9BQWYsRUFBd0I7QUFDcEIsUUFBSSxPQUFPQSxPQUFQLEtBQW1CLFdBQXZCLEVBQW9DO0FBQ2hDLFVBQUl2QyxPQUFPZ1AsT0FBUCxJQUFrQkEsUUFBUUMsR0FBOUIsRUFBbUM7QUFDL0JELGdCQUFRQyxHQUFSLENBQVkxTSxPQUFaO0FBQ0gsT0FGRCxNQUVPO0FBQ0gwSyxjQUFNMUssT0FBTjtBQUNIO0FBQ0o7QUFDSjtBQUVEOzs7Ozs7QUFJQSxXQUFTRyxDQUFULENBQVd3TSxVQUFYLEVBQXVCO0FBQ25CLFFBQUluSixVQUFVMUUsY0FBVixJQUE0QixPQUFPNk4sVUFBUCxLQUFzQixXQUFsRCxJQUNBbFAsT0FBT2dQLE9BRFAsSUFDa0JBLFFBQVFDLEdBRDlCLEVBQ21DO0FBQy9CRCxjQUFRQyxHQUFSLENBQVlDLFVBQVo7QUFDSDtBQUNKO0FBRUQ7Ozs7OztBQUtBLFdBQVM1QixVQUFULEdBQXNCO0FBQ2xCLFdBQU82Qix1QkFBdUJuUCxPQUFPb1AsV0FBOUIsR0FBNENDLGtCQUFrQmhNLFNBQVNpTSxlQUFULENBQXlCQyxVQUEzQyxHQUF3RGxNLFNBQVNpRyxJQUFULENBQWNpRyxVQUF6SDtBQUNIOztBQUVELFdBQVMvQixVQUFULEdBQXNCO0FBQ2xCLFdBQU8yQix1QkFBdUJuUCxPQUFPd1AsV0FBOUIsR0FBNENILGtCQUFrQmhNLFNBQVNpTSxlQUFULENBQXlCRyxTQUEzQyxHQUF1RHBNLFNBQVNpRyxJQUFULENBQWNtRyxTQUF4SDtBQUNIOztBQUVELFdBQVNKLGFBQVQsR0FBeUI7QUFDckIsV0FBUSxDQUFDaE0sU0FBU3FNLFVBQVQsSUFBdUIsRUFBeEIsTUFBZ0MsWUFBeEM7QUFDSDs7QUFFRCxXQUFTUCxrQkFBVCxHQUE4QjtBQUMxQixXQUFPblAsT0FBT29QLFdBQVAsS0FBdUJuTixTQUE5QjtBQUNILEdBNTdCTSxDQTg3QlA7QUFDQTs7O0FBQ0EsTUFBSSxJQUFKLEVBQWdEO0FBQzlDakMsV0FBTytGLFNBQVAsR0FBbUJBLFNBQW5CO0FBQ0QsR0FsOEJNLENBbzhCUDs7O0FBQ0E0SixTQUFPQyxPQUFQLEdBQWlCN0osU0FBakI7QUFFSCxDQXY4QkQsSSIsImZpbGUiOiJlbWJlZGRlZC5kZXZlbG9wbWVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFtdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcIkhlbGxvU2lnblwiXSA9IGZhY3RvcnkoKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJIZWxsb1NpZ25cIl0gPSBmYWN0b3J5KCk7XG59KSh3aW5kb3csIGZ1bmN0aW9uKCkge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9lbWJlZGRlZC5qc1wiKTtcbiIsIi8qKlxuICogSGVsbG9TaWduIEpTIGxpYnJhcnkgZm9yIGVtYmVkZGFibGVzXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTYgSGVsbG9TaWduXG4gKlxuICogWFdNIC0gQ3Jvc3Mtd2luZG93IG1lc3NhZ2luZyBpbnNwaXJlZCBieSBCZW4gQWxtYW4nc1xuICogalF1ZXJ5IHBvc3RNZXNzYWdlIHBsdWdpbjpcbiAqIGh0dHA6Ly9iZW5hbG1hbi5jb20vcHJvamVjdHMvanF1ZXJ5LXBvc3RtZXNzYWdlLXBsdWdpbi9cbiAqXG4gKiAgICBDb3B5cmlnaHQgKGMpIDIwMDkgXCJDb3dib3lcIiBCZW4gQWxtYW5cbiAqICAgIER1YWwgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBhbmQgR1BMIGxpY2Vuc2VzLlxuICogICAgaHR0cDovL2JlbmFsbWFuLmNvbS9hYm91dC9saWNlbnNlL1xuICovXG5cbihmdW5jdGlvbigpe1xuXG4gICAgZnVuY3Rpb24gZ2V0VXJsVmFycygpIHtcbiAgICAgICAgdmFyIHZhcnMgPSB7fTtcbiAgICAgICAgdmFyIHBhcnRzID0gd2luZG93LmxvY2F0aW9uLmhyZWYucmVwbGFjZSgvWz8mXSsoW149Jl0rKT0oW14mXSopL2dpLFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uKG0sIGtleSwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyc1trZXldID0gdmFsdWU7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiB2YXJzO1xuICAgIH1cblxuICAgIC8vIFVuZGVyc2NvcmUuanMgZGVib3VuY2UgaW1wbGVtZW50YXRpb24uXG4gICAgLy9cbiAgICAvLyBSZXR1cm5zIGEgZnVuY3Rpb24sIHRoYXQsIGFzIGxvbmcgYXMgaXQgY29udGludWVzIHRvIGJlIGludm9rZWQsIHdpbGwgbm90XG4gICAgLy8gYmUgdHJpZ2dlcmVkLiBUaGUgZnVuY3Rpb24gd2lsbCBiZSBjYWxsZWQgYWZ0ZXIgaXQgc3RvcHMgYmVpbmcgY2FsbGVkIGZvclxuICAgIC8vIE4gbWlsbGlzZWNvbmRzLiBJZiBgaW1tZWRpYXRlYCBpcyBwYXNzZWQsIHRyaWdnZXIgdGhlIGZ1bmN0aW9uIG9uIHRoZVxuICAgIC8vIGxlYWRpbmcgZWRnZSwgaW5zdGVhZCBvZiB0aGUgdHJhaWxpbmcuXG4gICAgZnVuY3Rpb24gZGVib3VuY2UoZnVuYywgd2FpdCwgaW1tZWRpYXRlKSB7XG4gICAgICAgIHZhciB0aW1lb3V0O1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgY29udGV4dCA9IHRoaXMsIGFyZ3MgPSBhcmd1bWVudHM7XG4gICAgICAgICAgICB2YXIgbGF0ZXIgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB0aW1lb3V0ID0gbnVsbDtcbiAgICAgICAgICAgICAgICBpZiAoIWltbWVkaWF0ZSkgZnVuYy5hcHBseShjb250ZXh0LCBhcmdzKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICB2YXIgY2FsbE5vdyA9IGltbWVkaWF0ZSAmJiAhdGltZW91dDtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aW1lb3V0KTtcbiAgICAgICAgICAgIHRpbWVvdXQgPSBzZXRUaW1lb3V0KGxhdGVyLCB3YWl0KTtcbiAgICAgICAgICAgIGlmIChjYWxsTm93KSBmdW5jLmFwcGx5KGNvbnRleHQsIGFyZ3MpO1xuICAgICAgICB9O1xuICAgIH1cblxuICAgIHZhciB1cmxWYXJzID0gZ2V0VXJsVmFycygpO1xuICAgIHdpbmRvdy5pc0RlYnVnRW5hYmxlZCA9ICh1cmxWYXJzLmRlYnVnID8gdXJsVmFycy5kZWJ1ZyA9PT0gJ3RydWUnIDogZmFsc2UpO1xuXG4gICAgdmFyIHVzZXJBZ2VudCA9IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKTtcblxuICAgIHZhciBYV00gPSB7XG5cbiAgICAgICAgY2FjaGVCdXN0OiAwLFxuICAgICAgICBsYXN0SGFzaDogMCxcbiAgICAgICAgaW50ZXJ2YWxJZDogMCxcbiAgICAgICAgcm1DYWxsYmFjazogbnVsbCxcbiAgICAgICAgZGVmYXVsdERlbGF5OiA1MDAsXG4gICAgICAgIGhhc1Bvc3RNZXNzYWdlOiAod2luZG93Wydwb3N0TWVzc2FnZSddICE9PSB1bmRlZmluZWQpLFxuXG4gICAgICAgIF9zZXJpYWxpemVNZXNzYWdlVmFsdWU6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgIHZhbHVlID0gSlNPTi5zdHJpbmdpZnkodmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudCh2YWx1ZSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2VuZDogZnVuY3Rpb24obWVzc2FnZSwgdGFyZ2V0VXJsLCB0YXJnZXQpIHtcblxuICAgICAgICAgICAgbCgnWFdNIFNlbmQ6IFNlbmRpbmcgTWVzc2FnZS4nKTtcbiAgICAgICAgICAgIGwoJyAgdGFyZ2V0VXJsOiAnICsgdGFyZ2V0VXJsKTtcblxuICAgICAgICAgICAgdmFyIHNlbGYgPSBYV007XG5cbiAgICAgICAgICAgIGlmICghdGFyZ2V0VXJsKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBTZXJpYWxpemUgdGhlIG1lc3NhZ2UgaW50byBhIHN0cmluZ1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlICE9ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBhcnRzID0gW107XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgayBpbiBtZXNzYWdlKSB7XG4gICAgICAgICAgICAgICAgICAgIHBhcnRzLnB1c2goayArICc9JyArIHRoaXMuX3NlcmlhbGl6ZU1lc3NhZ2VWYWx1ZShtZXNzYWdlW2tdKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBwYXJ0cy5qb2luKCcmJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGwoJyAgbWVzc2FnZTogJyArIG1lc3NhZ2UpO1xuXG4gICAgICAgICAgICBpZiAoc2VsZi5oYXNQb3N0TWVzc2FnZSkge1xuICAgICAgICAgICAgICAgIC8vIFRoZSBicm93c2VyIHN1cHBvcnRzIHdpbmRvdy5wb3N0TWVzc2FnZSwgc28gY2FsbCBpdCB3aXRoIGEgdGFyZ2V0T3JpZ2luXG4gICAgICAgICAgICAgICAgLy8gc2V0IGFwcHJvcHJpYXRlbHksIGJhc2VkIG9uIHRoZSB0YXJnZXRVcmwgcGFyYW1ldGVyLlxuICAgICAgICAgICAgICAgIHRhcmdldCA9IHRhcmdldCB8fCBwYXJlbnQ7XG4gICAgICAgICAgICAgICAgdGFyZ2V0Wydwb3N0TWVzc2FnZSddKG1lc3NhZ2UsIHRhcmdldFVybC5yZXBsYWNlKCAvKFteOl0rOlxcL1xcL1teXFwvXSspLiovLCAnJDEnICkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAodGFyZ2V0VXJsKSB7XG4gICAgICAgICAgICAgICAgLy8gVGhlIGJyb3dzZXIgZG9lcyBub3Qgc3VwcG9ydCB3aW5kb3cucG9zdE1lc3NhZ2UsIHNvIHNldCB0aGUgbG9jYXRpb25cbiAgICAgICAgICAgICAgICAvLyBvZiB0aGUgdGFyZ2V0IHRvIHRhcmdldFVybCNtZXNzYWdlLiBBIGJpdCB1Z2x5LCBidXQgaXQgd29ya3MhIEEgY2FjaGVcbiAgICAgICAgICAgICAgICAvLyBidXN0IHBhcmFtZXRlciBpcyBhZGRlZCB0byBlbnN1cmUgdGhhdCByZXBlYXQgbWVzc2FnZXMgdHJpZ2dlciB0aGVcbiAgICAgICAgICAgICAgICAvLyBjYWxsYmFjay5cbiAgICAgICAgICAgICAgICB2YXIgdCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICAgIHZhciBjID0gKytzZWxmLmNhY2hlQnVzdDtcbiAgICAgICAgICAgICAgICB2YXIgdGFyZ2V0RnJhbWUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0YXJnZXQpOyAvLyB0YXJnZXQgaXMgdGhlIHdpbmRvdyBpZCBpbiB0aGlzIGNhc2VcbiAgICAgICAgICAgICAgICAvLyB0YXJnZXRXaW5kb3cubG9jYXRpb24gPSB0YXJnZXRVcmwucmVwbGFjZSggLyMuKiQvLCAnJyApICsgJyMnICsgdCArIGMgKyAnJicgKyBtZXNzYWdlO1xuICAgICAgICAgICAgICAgIGlmICh0YXJnZXRGcmFtZSkge1xuICAgICAgICAgICAgICAgICAgICB0YXJnZXRGcmFtZS5zZXRBdHRyaWJ1dGUoJ3NyYycsIHRhcmdldFVybC5yZXBsYWNlKCAvIy4qJC8sICcnICkgKyAnIycgKyB0ICsgYyArICcmJyArIG1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50LmxvY2F0aW9uID0gdGFyZ2V0VXJsLnJlcGxhY2UoIC8jLiokLywgJycgKSArICcjJyArIHQgKyBjICsgJyYnICsgbWVzc2FnZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGwoJ1hXTSBTZW5kOiBNZXNzYWdlIHNlbnQuJyk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVjZWl2ZTogZnVuY3Rpb24oY2FsbGJhY2ssIHNvdXJjZU9yaWdpbiwgZGVsYXkpIHtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgY2FsbGJhY2sgIT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICBlcnJvcignY2FsbGJhY2sgbXVzdCBiZSBhIGZ1bmN0aW9uJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodHlwZW9mIHNvdXJjZU9yaWdpbiAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICBlcnJvcignc291cmNlT3JpZ2luIG11c3QgYmUgYSBzdHJpbmcnKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbCgnWFdNIFJlY2VpdmU6IEluaXRpYWxpemUgcmVjZWl2ZXIuJyk7XG4gICAgICAgICAgICBsKCcgIGNhbGxiYWNrOiAnICsgKGNhbGxiYWNrLm5hbWUgPyBjYWxsYmFjay5uYW1lIDogJ0Fub255bW91cyBmdW5jdGlvbicpKTtcbiAgICAgICAgICAgIGwoJyAgc291cmNlT3JpZ2luOiAnICsgc291cmNlT3JpZ2luKTtcblxuICAgICAgICAgICAgdmFyIHNlbGYgPSBYV007XG5cbiAgICAgICAgICAgIGlmIChzZWxmLmhhc1Bvc3RNZXNzYWdlKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBTaW5jZSB0aGUgYnJvd3NlciBzdXBwb3J0cyB3aW5kb3cucG9zdE1lc3NhZ2UsIHRoZSBjYWxsYmFjayB3aWxsIGJlXG4gICAgICAgICAgICAgICAgLy8gYm91bmQgdG8gdGhlIGFjdHVhbCBldmVudCBhc3NvY2lhdGVkIHdpdGggd2luZG93LnBvc3RNZXNzYWdlLlxuXG4gICAgICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYucm1DYWxsYmFjaykge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gVW5iaW5kIHByZXZpb3VzIGNhbGxiYWNrXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAod2luZG93WydhZGRFdmVudExpc3RlbmVyJ10gKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93WydyZW1vdmVFdmVudExpc3RlbmVyJ10oJ21lc3NhZ2UnLCBzZWxmLnJtQ2FsbGJhY2ssIGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vSUU4IGRvZXNuJ3Qgc3VwcG9ydCByZW1vdmVFdmVudExpc3RlbmVyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93WydkZXRhY2hFdmVudCddKCdvbm1lc3NhZ2UnLCBzZWxmLnJtQ2FsbGJhY2spO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gQmluZCB0aGUgY2FsbGJhY2suIEEgcmVmZXJlbmNlIHRvIHRoZSBjYWxsYmFjayBpcyBzdG9yZWQgZm9yIGVhc2Ugb2YgdW5iaW5kaW5nXG4gICAgICAgICAgICAgICAgICAgIHNlbGYucm1DYWxsYmFjayA9IGZ1bmN0aW9uKGV2dCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gRW5zdXJlIHRoZSBldmVudCBpcyBvcmlnaW5hdGluZyBmcm9tIHRoZSBzb3VyY2UgZG9tYWluLCBhY2NvdW50aW5nXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBmb3Igc3ViZG9tYWlucyAoZXZ0Lm9yaWdpbiBtdXN0IGVuZCB3aXRoIGEgZG90IGFuZCB0aGUgc291cmNlT3JpZ2luIHN0cmluZykuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZ0Lm9yaWdpbiAhPT0gc291cmNlT3JpZ2luKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHN1YmRvbWFpblRlc3QgPSBuZXcgUmVnRXhwKCdbXFwvfFxcLl0nICsgc291cmNlT3JpZ2luICsgJyQnLCAnaScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc3ViZG9tYWluVGVzdC50ZXN0KGV2dC5vcmlnaW4pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGwoJ1hXTSBSZWNlaXZlOiBNZXNzYWdlIHJlY2VpdmVkIScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbCgnICBkYXRhOiAnICsgZXZ0LmRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbCgnICBzb3VyY2VPcmlnaW46ICcgKyBzb3VyY2VPcmlnaW4pO1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soZXZ0KTtcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAod2luZG93WydhZGRFdmVudExpc3RlbmVyJ10pIHtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93WydhZGRFdmVudExpc3RlbmVyJ10oJ21lc3NhZ2UnLCBzZWxmLnJtQ2FsbGJhY2ssIGZhbHNlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIC8vSUU4IGRvZXNuJ3Qgc3VwcG9ydCBhZGRFdmVudExpc3RlbmVyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvd1snYXR0YWNoRXZlbnQnXSgnb25tZXNzYWdlJywgc2VsZi5ybUNhbGxiYWNrKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgLy8gU2luY2UgdGhlIGJyb3dzZXIgc3Vja3MsIGEgcG9sbGluZyBsb29wIHdpbGwgYmUgc3RhcnRlZCwgYW5kIHRoZVxuICAgICAgICAgICAgICAgIC8vIGNhbGxiYWNrIHdpbGwgYmUgY2FsbGVkIHdoZW5ldmVyIHRoZSBsb2NhdGlvbi5oYXNoIGNoYW5nZXMuXG4gICAgICAgICAgICAgICAgbCgnWFdNIFJlY2VpdmU6IFN0YXJ0aW5nIHBvbGwuLi4nKTtcblxuICAgICAgICAgICAgICAgIGlmIChzZWxmLmludGVydmFsSWQpIHtcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChzZWxmLmludGVydmFsSWQpO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLmludGVydmFsSWQgPSBudWxsO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZGVsYXkgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIGRlbGF5ID0gc2VsZi5kZWZhdWx0RGVsYXk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgZGVsYXkgPSAoZGVsYXkgIT09IHVuZGVmaW5lZCA/IGRlbGF5IDogMjAwKTtcblxuICAgICAgICAgICAgICAgICAgICBzZWxmLmludGVydmFsSWQgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGhhc2ggPSBkb2N1bWVudC5sb2NhdGlvbi5oYXNoO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJlID0gL14jP1xcZCsmLztcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChoYXNoICE9PSBzZWxmLmxhc3RIYXNoICYmIHJlLnRlc3QoaGFzaCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLmxhc3RIYXNoID0gaGFzaDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgZGF0YSA9IGhhc2gucmVwbGFjZShyZSwgJycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGwoJ1hXTSBSZWNlaXZlOiBNZXNzYWdlIHJlY2VpdmVkIScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGwoJyAgZGF0YTogJyArIGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGwoJyAgc291cmNlT3JpZ2luOiAnICsgc291cmNlT3JpZ2luKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7IGRhdGE6IGRhdGEgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sIGRlbGF5KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEhlbHBlciBmdW5jdGlvbnMgdG8gbWFuYWdlIHRoZSBcInZpZXdwb3J0XCIgbWV0YSB0YWcuXG4gICAgICogVGhpcyBhbGxvd3MgdXMgdG8gZHluYW1pY2FsbHkgY29udHJvbCB0aGUgZGlzcGxheVxuICAgICAqIGFuZCBwbGFjZW1lbnQgb2YgdGhlIGlGcmFtZSBpbiBhIG1vYmlsZSBjb250ZXh0LlxuICAgICAqL1xuICAgIHZhciBNZXRhVGFnSGVscGVyID0ge1xuXG4gICAgICAgIHNhdmVkVmlld3BvcnRDb250ZW50OiAnJyxcblxuICAgICAgICBzZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbCgnT3B0aW1pemluZyB2aWV3cG9ydCBtZXRhIHRhZyBmb3IgbW9iaWxlJyk7XG5cbiAgICAgICAgICAgIC8vIFNhdmUgb2ZmIHRoZSBjdXJyZW50IHZpZXdwb3J0IG1ldGEgdGFnIGNvbnRlbnRcbiAgICAgICAgICAgIHRoaXMuc2F2ZWRWaWV3cG9ydENvbnRlbnQgPSB0aGlzLl9nZXRFbGVtZW50KCkuZ2V0QXR0cmlidXRlKCdjb250ZW50Jyk7XG5cbiAgICAgICAgICAgIC8vIEFkZCBtb2JpbGUtb3B0aW1pemVkIHNldHRpbmdzXG4gICAgICAgICAgICB2YXIgY29udGVudFBhaXJzID0gdGhpcy5fZXhwbG9kZVBhaXJzKHRoaXMuc2F2ZWRWaWV3cG9ydENvbnRlbnQpO1xuICAgICAgICAgICAgY29udGVudFBhaXJzWyd3aWR0aCddID0gJ2RldmljZS13aWR0aCc7XG4gICAgICAgICAgICBjb250ZW50UGFpcnNbJ21heGltdW0tc2NhbGUnXSA9ICcxLjAnO1xuICAgICAgICAgICAgY29udGVudFBhaXJzWyd1c2VyLXNjYWxhYmxlJ10gPSAnbm8nO1xuICAgICAgICAgICAgdGhpcy5fZ2V0RWxlbWVudCgpLnNldEF0dHJpYnV0ZSgnY29udGVudCcsIHRoaXMuX2pvaW5QYWlycyhjb250ZW50UGFpcnMpKTtcbiAgICAgICAgfSxcblxuICAgICAgICByZXN0b3JlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGwoJ1Jlc3RvcmluZyB2aWV3cG9ydCBtZXRhIHRhZycpO1xuICAgICAgICAgICAgdGhpcy5fZ2V0RWxlbWVudCgpLnNldEF0dHJpYnV0ZSgnY29udGVudCcsIHRoaXMuc2F2ZWRWaWV3cG9ydENvbnRlbnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIF9nZXRFbGVtZW50OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ21ldGFbbmFtZT12aWV3cG9ydF0nKTtcbiAgICAgICAgICAgIGlmICghZWwpIHtcbiAgICAgICAgICAgICAgICBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ21ldGEnKTtcbiAgICAgICAgICAgICAgICBlbC5zZXRBdHRyaWJ1dGUoJ25hbWUnLCAndmlld3BvcnQnKTtcbiAgICAgICAgICAgICAgICBlbC5zZXRBdHRyaWJ1dGUoJ2NvbnRlbnQnLCAnaW5pdGlhbC1zY2FsZT0xLjAnKTtcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKGVsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBlbDtcbiAgICAgICAgfSxcblxuICAgICAgICBfam9pblBhaXJzOiBmdW5jdGlvbihrZXllZCl7XG4gICAgICAgICAgICB2YXIgcGFpcnMgPSBbXTtcbiAgICAgICAgICAgIGZvciAodmFyIGtleSBpbiBrZXllZCkge1xuICAgICAgICAgICAgICAgIHBhaXJzLnB1c2goa2V5ICsgJz0nICsga2V5ZWRba2V5XSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gcGFpcnMuam9pbignLCAnKTtcbiAgICAgICAgfSxcblxuICAgICAgICBfZXhwbG9kZVBhaXJzOiBmdW5jdGlvbihtZXRhU3RyaW5nKXtcbiAgICAgICAgICAgIHZhciBwYWlycyA9IG1ldGFTdHJpbmcuc3BsaXQoJywnKTtcbiAgICAgICAgICAgIHZhciBvYmogPSB7fTtcbiAgICAgICAgICAgIHBhaXJzLmZvckVhY2goZnVuY3Rpb24ocGFpcikge1xuICAgICAgICAgICAgICAgIHBhaXIgPSBwYWlyLnRyaW0oKTtcbiAgICAgICAgICAgICAgICB2YXIga3YgPSBwYWlyLnNwbGl0KCc9Jyk7XG4gICAgICAgICAgICAgICAgb2JqW2t2WzBdXSA9IGt2WzFdO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIHJldHVybiBvYmo7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIEhlbGxvU2lnbiA9IHtcblxuICAgICAgICBWRVJTSU9OOiByZXF1aXJlKCcuLi9wYWNrYWdlLmpzb24nKS52ZXJzaW9uLFxuICAgICAgICBJRlJBTUVfV0lEVEhfUkFUSU86IDAuOCxcbiAgICAgICAgREVGQVVMVF9XSURUSDogOTAwLFxuICAgICAgICBERUZBVUxUX0hFSUdIVDogOTAwLFxuICAgICAgICBNSU5fSEVJR0hUOiA0ODAsXG4gICAgICAgIHdyYXBwZXI6IG51bGwsXG4gICAgICAgIGlmcmFtZTogbnVsbCxcbiAgICAgICAgb3ZlcmxheTogbnVsbCxcbiAgICAgICAgY2FuY2VsQnV0dG9uOiBudWxsLFxuICAgICAgICBjbGllbnRJZDogbnVsbCxcbiAgICAgICAgaXNPbGRJRTogKC9tc2llICg4fDd8Nnw1KS9naS50ZXN0KHVzZXJBZ2VudCkpLFxuICAgICAgICBpc0ZGOiAoL2ZpcmVmb3gvZ2kudGVzdCh1c2VyQWdlbnQpKSxcbiAgICAgICAgaXNPcGVyYTogKC9vcGVyYS9naS50ZXN0KHVzZXJBZ2VudCkpLFxuICAgICAgICBpc01vYmlsZTogKC9hbmRyb2lkfHdlYm9zfGlwaG9uZXxpcGFkfGlwb2R8YmxhY2tiZXJyeXxpZW1vYmlsZXxvcGVyYSBtaW5pL2kudGVzdCh1c2VyQWdlbnQpKSxcbiAgICAgICAgYmFzZVVybDogJ2h0dHBzOi8vd3d3LmhlbGxvc2lnbi5jb20nLFxuICAgICAgICBjZG5CYXNlVXJsOiAnaHR0cHM6Ly9zMy5hbWF6b25hd3MuY29tL2Nkbi5oZWxsb2ZheC5jb20nLFxuICAgICAgICBYV006IFhXTSxcblxuICAgICAgICBDVUxUVVJFUzoge1xuICAgICAgICAgICAgRU5fVVM6ICdlbl9VUycsXG4gICAgICAgICAgICBGUl9GUjogJ2ZyX0ZSJyxcbiAgICAgICAgICAgIERFX0RFOiAnZGVfREUnLFxuICAgICAgICAgICAgU1ZfU0U6ICdzdl9TRScsXG4gICAgICAgICAgICBaSF9DTjogJ3poX0NOJyxcbiAgICAgICAgICAgIERBX0RLOiAnZGFfREsnLFxuICAgICAgICAgICAgTkxfTkw6ICdubF9OTCcsXG4gICAgICAgICAgICBFU19FUzogJ2VzX0VTJyxcbiAgICAgICAgICAgIEVTX01YOiAnZXNfTVgnLFxuICAgICAgICAgICAgUFRfQlI6ICdwdF9CUicsXG4gICAgICAgICAgICBQTF9QTDogJ3BsX1BMJyxcbiAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc3VwcG9ydGVkQ3VsdHVyZXMgPSBbdGhpcy5FTl9VUywgdGhpcy5GUl9GUiwgdGhpcy5ERV9ERSwgdGhpcy5TVl9TRSwgdGhpcy5aSF9DTiwgdGhpcy5EQV9ESywgdGhpcy5OTF9OTCwgdGhpcy5FU19FUywgdGhpcy5FU19NWCwgdGhpcy5QVF9CUiwgdGhpcy5QTF9QTF07XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0uaW5pdCgpLFxuXG4gICAgICAgIGlzRGVidWdFbmFibGVkOiB3aW5kb3cuaXNEZWJ1Z0VuYWJsZWQsXG5cbiAgICAgICAgLy8gUFVCTElDIEVWRU5UU1xuICAgICAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgLy8gLSBlcnJvciAgICAgICAgICAgICAgICAgICAgICAgICAgQW4gZXJyb3Igb2NjdXJyZWQgaW4gdGhlIGlGcmFtZVxuICAgICAgICAvLyAtIHNpZ25hdHVyZV9yZXF1ZXN0X3NpZ25lZCAgICAgICBUaGUgc2lnbmF0dXJlIHJlcXVlc3Qgd2FzIHNpZ25lZFxuICAgICAgICAvLyAtIHNpZ25hdHVyZV9yZXF1ZXN0X2NhbmNlbGVkICAgICBUaGUgdXNlciBjbG9zZWQgdGhlIGlGcmFtZSBiZWZvcmUgY29tcGxldGluZ1xuXG5cbiAgICAgICAgLy8gVEhFU0UgRVZFTlQgQ09ERVMgQVJFIEFDVFVBTExZIFVTRUQgSU4gVFdPIFBMQUNFU1xuICAgICAgICAvLyBJRiBZT1UgQ0hBTkdFIFRIRU0gTUFLRSBTVVJFIFRPIENIQU5HRSBUSEUgT1RIRVJTXG4gICAgICAgIC8vIElOIEhGQUNUSU9OUy5QSFAgVE8gU1RBWSBDT05TSVNURU5ULlxuICAgICAgICBFVkVOVF9TSUdORUQ6ICdzaWduYXR1cmVfcmVxdWVzdF9zaWduZWQnLFxuICAgICAgICBFVkVOVF9ERUNMSU5FRDogJ3NpZ25hdHVyZV9yZXF1ZXN0X2RlY2xpbmVkJyxcbiAgICAgICAgRVZFTlRfQ0FOQ0VMRUQ6ICdzaWduYXR1cmVfcmVxdWVzdF9jYW5jZWxlZCcsXG4gICAgICAgIEVWRU5UX1JFQVNTSUdORUQ6ICdzaWduYXR1cmVfcmVxdWVzdF9yZWFzc2lnbmVkJyxcbiAgICAgICAgRVZFTlRfU0VOVDogJ3NpZ25hdHVyZV9yZXF1ZXN0X3NlbnQnLFxuICAgICAgICBFVkVOVF9URU1QTEFURV9DUkVBVEVEOiAndGVtcGxhdGVfY3JlYXRlZCcsXG4gICAgICAgIEVWRU5UX0VSUk9SOiAnZXJyb3InLFxuXG5cbiAgICAgICAgLy8gIC0tLS0gIFBVQkxJQyBNRVRIT0RTICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gICAgICAgIGluaXQ6IGZ1bmN0aW9uKGFwcENsaWVudElkKSB7XG4gICAgICAgICAgICB0aGlzLmNsaWVudElkID0gYXBwQ2xpZW50SWQ7XG4gICAgICAgIH0sXG5cbiAgICAgICAgb3BlbjogZnVuY3Rpb24ocGFyYW1zKSB7XG5cbiAgICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgICAgICAgLy8gUEFSQU1FVEVSUzpcbiAgICAgICAgICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgIC8vIC0gdXJsICAgICAgICAgICAgICAgICAgICAgIFN0cmluZy4gVGhlIHVybCB0byBvcGVuIGluIHRoZSBjaGlsZCBmcmFtZVxuICAgICAgICAgICAgLy8gLSByZWRpcmVjdFVybCAgICAgICAgICAgICAgU3RyaW5nLiBXaGVyZSB0byBnbyBhZnRlciB0aGUgc2lnbmF0dXJlIGlzIGNvbXBsZXRlZFxuICAgICAgICAgICAgLy8gLSBhbGxvd0NhbmNlbCAgICAgICAgICAgICAgQm9vbGVhbi4gV2hldGhlciBhIGNhbmNlbCBidXR0b24gc2hvdWxkIGJlIGRpc3BsYXllZCAoZGVmYXVsdCA9IHRydWUpXG4gICAgICAgICAgICAvLyAtIG1lc3NhZ2VMaXN0ZW5lciAgICAgICAgICBGdW5jdGlvbi4gQSBsaXN0ZW5lciBmb3IgWC13aW5kb3cgbWVzc2FnZXMgY29taW5nIGZyb20gdGhlIGNoaWxkIGZyYW1lXG4gICAgICAgICAgICAvLyAtIHVzZXJDdWx0dXJlICAgICAgICAgICAgICBIZWxsb1NpZ24uQ1VMVFVSRS4gT25lIG9mIHRoZSBIZWxsb1NpZ24uQ1VMVFVSRVMuc3VwcG9ydGVkQ3VsdHVyZXMgKGRlZmF1bHQgPSBIZWxsb1NpZ24uQ1VMVFVSRVMuRU5fVVMpXG4gICAgICAgICAgICAvLyAtIGRlYnVnICAgICAgICAgICAgICAgICAgICBCb29sZWFuLiBXaGVuIHRydWUsIGRlYnVnZ2luZyBzdGF0ZW1lbnRzIHdpbGwgYmUgd3JpdHRlbiB0byB0aGUgY29uc29sZSAoZGVmYXVsdCA9IGZhbHNlKVxuICAgICAgICAgICAgLy8gLSBza2lwRG9tYWluVmVyaWZpY2F0aW9uICAgQm9vbGVhbi4gV2hlbiB0cnVlLCBkb21haW4gdmVyaWZpY2F0aW9uIHN0ZXAgd2lsbCBiZSBza2lwcGVkIGlmIGFuZCBvbmx5IGlmIHRoZSBTaWduYXR1cmUgUmVxdWVzdCB3YXMgY3JlYXRlZCB3aXRoIHRlc3RfbW9kZT0xIChkZWZhdWx0ID0gZmFsc2UpXG4gICAgICAgICAgICAvLyAtIGNvbnRhaW5lciAgICAgICAgICAgICAgICBET00gZWxlbWVudCB0aGF0IHdpbGwgY29udGFpbiB0aGUgaWZyYW1lIG9uIHRoZSBwYWdlIChkZWZhdWx0ID0gZG9jdW1lbnQuYm9keSlcbiAgICAgICAgICAgIC8vIC0gaGVpZ2h0ICAgICAgICAgICAgICAgICAgIEhlaWdodCBvZiB0aGUgaUZyYW1lIChvbmx5IGFwcGxpY2FibGUgd2hlbiBhIGNvbnRhaW5lciBpcyBzcGVjaWZpZWQpXG4gICAgICAgICAgICAvLyAtIGhpZGVIZWFkZXIgICAgICAgICAgICAgICBCb29sZWFuLiBXaGVuIHRydWUsIHRoZSBoZWFkZXIgd2lsbCBiZSBoaWRkZW4gKGRlZmF1bHQgPSBmYWxzZSkuIFRoaXMgaXMgb25seSBmdW5jdGlvbmFsIGZvciBjdXN0b21lcnMgd2l0aCBlbWJlZGRlZCBicmFuZGluZyBlbmFibGVkLlxuICAgICAgICAgICAgLy8gLSByZXF1ZXN0ZXIgICAgICAgICAgICAgICAgU3RyaW5nLiBUaGUgZW1haWwgb2YgdGhlIHBlcnNvbiBpc3N1aW5nIGEgc2lnbmF0dXJlIHJlcXVlc3QuIFJlcXVpcmVkIGZvciBhbGxvd2luZyAnTWUgKyBPdGhlcnMnIHJlcXVlc3RzXG4gICAgICAgICAgICAvLyAtIHdoaXRlTGFiZWxpbmdPcHRpb25zICAgICBPYmplY3QuIEFuIGFzc29jaWF0aXZlIGFycmF5IHRvIGJlIHVzZWQgdG8gY3VzdG9taXplIHRoZSBhcHAncyBzaWduZXIgcGFnZVxuICAgICAgICAgICAgLy8gLSBoZWFsdGhDaGVja1RpbWVvdXRNcyAgICAgSW50ZWdlci4gVGhlIG51bWJlciBvZiBtaWxsaXNlY29uZHMgdG8gd2FpdCBmb3IgYSByZXNwb25zZSBmcm9tIHRoZSBpZnJhbWUuIElmIG5vIHJlc3BvbnNlIGFmdGVyIHRoYXQgdGltZSB0aGUgaWZyYW1lIHdpbGwgYmUgY2xvc2VkLiAxNTAwMCBtaWxsaXNlY29uZHMgaXMgcmVjb21tZW5kZWQuXG4gICAgICAgICAgICAvLyAtIGZpbmFsQnV0dG9uVGV4dCAgICAgICAgICBTdHJpbmcuIFRoZSB0ZXh0IHRvIHVzZSBmb3IgdGhlIGZpbmFsIHNlbmQvY29udGludWUgYnV0dG9uIGlmIHlvdSdkIGxpa2UgdG8gb3ZlcnJpZGUgaXQncyBkZWZhdWx0IHRleHQuIE1heSBvbmx5IGJlIHNldCB0byBcIlNlbmRcIiBvciBcIkNvbnRpbnVlXCIuXG5cbiAgICAgICAgICAgIHZhciByZWRpcmVjdFVybCA9IHRoaXMuc2FmZVVybChwYXJhbXNbJ3JlZGlyZWN0VXJsJ10pO1xuICAgICAgICAgICAgdmFyIG1lc3NhZ2VMaXN0ZW5lciA9IHBhcmFtc1snbWVzc2FnZUxpc3RlbmVyJ107XG4gICAgICAgICAgICB2YXIgZnJhbWVVcmwgPSB0aGlzLnNhZmVVcmwocGFyYW1zWyd1cmwnXSk7XG4gICAgICAgICAgICB0aGlzLmhlYWx0aENoZWNrVGltZW91dE1zID0gcGFyYW1zWydoZWFsdGhDaGVja1RpbWVvdXRNcyddO1xuXG4gICAgICAgICAgICBpZiAoIWZyYW1lVXJsKSB7XG4gICAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ011c3QgcHJvdmlkZSBhcmd1bWVudHMgdG8gb3BlbigpJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgcGFyYW1zWydkZWJ1ZyddICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgIHRoaXMuaXNEZWJ1Z0VuYWJsZWQgPSAocGFyYW1zWydkZWJ1ZyddID09PSB0cnVlIHx8IHBhcmFtc1snZGVidWcnXSA9PSAndHJ1ZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXJhbXNbJ3NraXBEb21haW5WZXJpZmljYXRpb24nXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNraXBEb21haW5WZXJpZmljYXRpb24gPSAocGFyYW1zWydza2lwRG9tYWluVmVyaWZpY2F0aW9uJ10gPT09IHRydWUgfHwgcGFyYW1zWydza2lwRG9tYWluVmVyaWZpY2F0aW9uJ10gPT0gJ3RydWUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0eXBlb2YgcGFyYW1zWydoaWRlSGVhZGVyJ10gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5oaWRlSGVhZGVyID0gKHBhcmFtc1snaGlkZUhlYWRlciddID09PSB0cnVlIHx8IHBhcmFtc1snaGlkZUhlYWRlciddID09ICd0cnVlJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodHlwZW9mIHBhcmFtc1snZmluYWxCdXR0b25UZXh0J10gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5maW5hbEJ1dHRvblRleHQgPSBwYXJhbXNbJ2ZpbmFsQnV0dG9uVGV4dCddO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXJhbXNbJ3doaXRlTGFiZWxpbmdPcHRpb25zJ10gPT09ICdvYmplY3QnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy53aGl0ZUxhYmVsaW5nT3B0aW9ucyA9IEpTT04uc3RyaW5naWZ5KHBhcmFtc1snd2hpdGVMYWJlbGluZ09wdGlvbnMnXSk7XG4gICAgICAgICAgICAgICAgdGhpcy53aGl0ZUxhYmVsaW5nT3B0aW9ucyA9IHRoaXMud2hpdGVMYWJlbGluZ09wdGlvbnMucmVwbGFjZSgvIy9nLCAnJyk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBwYXJhbXNbJ3doaXRlTGFiZWxpbmdPcHRpb25zJ10gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgbChcIkludmFsaWQgd2hpdGUgbGFiZWxpbmcgb3B0aW9ucyBzdXBwbGllZCwgb3B0aW9uIHdpbGwgYmUgaWdub3JlZDogXCIgKyBwYXJhbXNbJ3doaXRlTGFiZWxpbmdPcHRpb25zJ10pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5pc0luUGFnZSA9IChwYXJhbXNbJ2NvbnRhaW5lciddICE9PSB1bmRlZmluZWQpO1xuICAgICAgICAgICAgdGhpcy5jb250YWluZXIgPSBwYXJhbXNbJ2NvbnRhaW5lciddIHx8IGRvY3VtZW50LmJvZHk7XG5cbiAgICAgICAgICAgIC8vIFZhbGlkYXRlIHBhcmFtZXRlcnNcbiAgICAgICAgICAgIGlmICh0aGlzLmlzSW5QYWdlICYmIHBhcmFtc1snaGVpZ2h0J10gIT09IHVuZGVmaW5lZCAmJiAoaXNOYU4ocGFyc2VJbnQocGFyYW1zWydoZWlnaHQnXSwgMTApKSB8fCBwYXJhbXNbJ2hlaWdodCddIDw9IDApKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGlGcmFtZSBoZWlnaHQgKCcgKyBwYXJhbXNbJ2hlaWdodCddICsgJykgaXQgbXVzdCBiZSBhIHZhbGlkIHBvc2l0aXZlIG51bWJlcicpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsKCdPcGVuaW5nIEhlbGxvU2lnbiBlbWJlZGRlZCBpRnJhbWUgd2l0aCB0aGUgZm9sbG93aW5nIHBhcmFtczonKTtcbiAgICAgICAgICAgIGwocGFyYW1zKTtcblxuICAgICAgICAgICAgaWYgKCFmcmFtZVVybCkge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTm8gdXJsIHNwZWNpZmllZCcpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgdXNlckN1bHR1cmUgPSB0eXBlb2YgcGFyYW1zWyd1c2VyQ3VsdHVyZSddID09PSAndW5kZWZpbmVkJyA/IHRoaXMuQ1VMVFVSRVMuRU5fVVMgOiBwYXJhbXNbJ3VzZXJDdWx0dXJlJ107XG4gICAgICAgICAgICBpZiAodGhpcy5pbkFycmF5KHVzZXJDdWx0dXJlLCB0aGlzLkNVTFRVUkVTLnN1cHBvcnRlZEN1bHR1cmVzKSA9PT0gLTEpIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgdXNlckN1bHR1cmUgc3BlY2lmaWVkOiAnICsgdXNlckN1bHR1cmUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmcmFtZVVybCArPSAoZnJhbWVVcmwuaW5kZXhPZignPycpID4gMCA/ICcmJyA6ICc/Jyk7XG4gICAgICAgICAgICBpZiAocmVkaXJlY3RVcmwpIHtcbiAgICAgICAgICAgICAgICBmcmFtZVVybCArPSAncmVkaXJlY3RfdXJsPScgKyBlbmNvZGVVUklDb21wb25lbnQocmVkaXJlY3RVcmwpICsgJyYnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZnJhbWVVcmwgKz0gJ3BhcmVudF91cmw9JyArIGVuY29kZVVSSUNvbXBvbmVudChkb2N1bWVudC5sb2NhdGlvbi5ocmVmLnJlcGxhY2UoL1xcPy4qLywgJycpKSArICcmJztcbiAgICAgICAgICAgIGZyYW1lVXJsICs9ICh0aGlzLnNraXBEb21haW5WZXJpZmljYXRpb24gPT09IHRydWUgPyAnc2tpcF9kb21haW5fdmVyaWZpY2F0aW9uPTEmJyA6ICcnKTtcbiAgICAgICAgICAgIGZyYW1lVXJsICs9ICdjbGllbnRfaWQ9JyArIHRoaXMuY2xpZW50SWQgKyAnJic7XG4gICAgICAgICAgICBmcmFtZVVybCArPSAodHlwZW9mIHBhcmFtc1sncmVxdWVzdGVyJ10gIT09ICd1bmRlZmluZWQnID8gJ3JlcXVlc3Rlcj0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHBhcmFtc1sncmVxdWVzdGVyJ10pICsgJyYnIDogJycpO1xuICAgICAgICAgICAgZnJhbWVVcmwgKz0gJ3VzZXJfY3VsdHVyZT0nICsgdXNlckN1bHR1cmU7XG4gICAgICAgICAgICBpZiAodGhpcy5pc0RlYnVnRW5hYmxlZCkge1xuICAgICAgICAgICAgICAgIGZyYW1lVXJsICs9ICcmZGVidWc9dHJ1ZSc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5oaWRlSGVhZGVyKSB7XG4gICAgICAgICAgICAgICAgZnJhbWVVcmwgKz0gJyZoaWRlSGVhZGVyPXRydWUnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMud2hpdGVMYWJlbGluZ09wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICBmcmFtZVVybCArPSAnJndoaXRlX2xhYmVsaW5nX29wdGlvbnM9JyArIGVuY29kZVVSSSh0aGlzLndoaXRlTGFiZWxpbmdPcHRpb25zKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0aGlzLmZpbmFsQnV0dG9uVGV4dCkge1xuICAgICAgICAgICAgICAgIGZyYW1lVXJsICs9ICcmZmluYWxfYnV0dG9uX3RleHQ9JyArIHRoaXMuZmluYWxCdXR0b25UZXh0O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmcmFtZVVybCArPSAnJmpzX3ZlcnNpb249JyArIHRoaXMuVkVSU0lPTjtcblxuICAgICAgICAgICAgdmFyIG9yaWdpbiA9IGZyYW1lVXJsLnJlcGxhY2UoLyhbXjpdKzpcXC9cXC9bXlxcL10rKS4qLywgJyQxJyk7XG4gICAgICAgICAgICB2YXIgd2luZG93RGltcyA9IHRoaXMuZ2V0V2luZG93RGltZW5zaW9ucyhwYXJhbXNbJ2hlaWdodCddKTtcbiAgICAgICAgICAgIHZhciBzdHlsZXMgPSB7XG4gICAgICAgICAgICAgICAgJ292ZXJsYXknOiB7XG4gICAgICAgICAgICAgICAgICAgICdwb3NpdGlvbic6ICdmaXhlZCcsXG4gICAgICAgICAgICAgICAgICAgICd0b3AnOiAnMHB4JyxcbiAgICAgICAgICAgICAgICAgICAgJ2xlZnQnOiAnMHB4JyxcbiAgICAgICAgICAgICAgICAgICAgJ2JvdHRvbSc6ICcwcHgnLFxuICAgICAgICAgICAgICAgICAgICAncmlnaHQnOiAnMHB4JyxcbiAgICAgICAgICAgICAgICAgICAgJ3otaW5kZXgnOiA5OTk3LFxuICAgICAgICAgICAgICAgICAgICAnZGlzcGxheSc6ICdibG9jaycsXG4gICAgICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyMyMjInLFxuICAgICAgICAgICAgICAgICAgICAnb3BhY2l0eSc6IDAuNCxcbiAgICAgICAgICAgICAgICAgICAgJy1raHRtbC1vcGFjaXR5JzogMC40LFxuICAgICAgICAgICAgICAgICAgICAnLW1vei1vcGFjaXR5JzogMC40LFxuICAgICAgICAgICAgICAgICAgICAnZmlsdGVyJzogJ2FscGhhKG9wYWNpdHk9NDApJyxcbiAgICAgICAgICAgICAgICAgICAgJy1tcy1maWx0ZXInOiAncHJvZ2lkOkRYSW1hZ2VUcmFuc2Zvcm0uTWljcm9zb2Z0LkFscGhhKE9wYWNpdHk9NDApJ1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgJ3dyYXBwZXInOiB0aGlzLmlzSW5QYWdlID8ge30gOiB7XG4gICAgICAgICAgICAgICAgICAgICdwb3NpdGlvbic6ICdhYnNvbHV0ZScsXG4gICAgICAgICAgICAgICAgICAgICd0b3AnOiB3aW5kb3dEaW1zLnRvcCxcbiAgICAgICAgICAgICAgICAgICAgJ2xlZnQnOiB3aW5kb3dEaW1zLmxlZnQsXG4gICAgICAgICAgICAgICAgICAgICd6LWluZGV4JzogOTk5OFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgJ2lmcmFtZSc6IHRoaXMuaXNJblBhZ2UgPyB7fSA6IHtcbiAgICAgICAgICAgICAgICAgICAgJ21hcmdpbic6ICcxcHgnLFxuICAgICAgICAgICAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjRkZGJyxcbiAgICAgICAgICAgICAgICAgICAgJ3otaW5kZXgnOiA5OTk4XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAnY2FuY2VsQnV0dG9uJzoge1xuICAgICAgICAgICAgICAgICAgICAncG9zaXRpb24nOiAnYWJzb2x1dGUnLFxuICAgICAgICAgICAgICAgICAgICAndG9wJzogJy0xM3B4JyxcbiAgICAgICAgICAgICAgICAgICAgJ3JpZ2h0JzogJy0xM3B4JyxcbiAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzMwcHgnLFxuICAgICAgICAgICAgICAgICAgICAnaGVpZ2h0JzogJzMwcHgnLFxuICAgICAgICAgICAgICAgICAgICAnYmFja2dyb3VuZC1pbWFnZSc6ICd1cmwoJyArIHRoaXMuY2RuQmFzZVVybCArICcvY3NzL2ZhbmN5Ym94L2ZhbmN5Ym94LnBuZyknLFxuICAgICAgICAgICAgICAgICAgICAnYmFja2dyb3VuZC1wb3NpdGlvbic6ICctNDBweCAwcHgnLFxuICAgICAgICAgICAgICAgICAgICAnY3Vyc29yJzogJ3BvaW50ZXInLFxuICAgICAgICAgICAgICAgICAgICAnei1pbmRleCc6IDk5OTlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICB2YXIgcmVzaXplSUZyYW1lID0gZnVuY3Rpb24gX3Jlc2l6ZUlGcmFtZSgpIHtcbiAgICAgICAgICAgICAgICBpZiAoc2VsZi5pZnJhbWUpIHtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgZGltcyA9IHt9O1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLmlzTW9iaWxlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaW1zID0gc2VsZi5nZXRNb2JpbGVEaW1lbnNpb25zKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaW1zID0gc2VsZi5nZXRXaW5kb3dEaW1lbnNpb25zKCk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBzZWxmLndyYXBwZXIuc3R5bGVbJ3RvcCddID0gZGltcy50b3A7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYud3JhcHBlci5zdHlsZVsnbGVmdCddID0gZGltcy5sZWZ0O1xuICAgICAgICAgICAgICAgICAgICBzZWxmLndyYXBwZXIuc3R5bGVbJ3dpZHRoJ10gPSBkaW1zLndpZHRoU3RyaW5nO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLmlmcmFtZS5zdHlsZVsnaGVpZ2h0J10gPSBkaW1zLmhlaWdodFN0cmluZztcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5pZnJhbWUuc3R5bGVbJ3dpZHRoJ10gPSBkaW1zLndpZHRoU3RyaW5nO1xuXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaWYgKHRoaXMuaXNJblBhZ2UpIHtcbiAgICAgICAgICAgICAgICAvLyBBZGp1c3QgdGhlIGlGcmFtZSBzdHlsZSB0byBmaXQgdGhlIGluLXBhZ2UgY29udGFpbmVyXG4gICAgICAgICAgICAgICAgc3R5bGVzWyd3cmFwcGVyJ11bJ3dpZHRoJ10gPSAnMTAwJSc7XG4gICAgICAgICAgICAgICAgc3R5bGVzWyd3cmFwcGVyJ11bJ2hlaWdodCddID0gd2luZG93RGltcy5oZWlnaHRTdHJpbmc7XG4gICAgICAgICAgICAgICAgc3R5bGVzWydpZnJhbWUnXVsnd2lkdGgnXSA9ICcxMDAlJztcbiAgICAgICAgICAgICAgICBzdHlsZXNbJ2lmcmFtZSddWydoZWlnaHQnXSA9IHdpbmRvd0RpbXMuaGVpZ2h0U3RyaW5nO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snaWZyYW1lJ11bJ2JvcmRlciddID0gJ25vbmUnO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snaWZyYW1lJ11bJ2JveC1zaGFkb3cnXSA9ICdub25lJztcbiAgICAgICAgICAgICAgICBzdHlsZXNbJ2NhbmNlbEJ1dHRvbiddWydkaXNwbGF5J10gPSAnbm9uZSc7XG5cbiAgICAgICAgICAgICAgICAvLyBUaGlzIGlzIGFuIGlPUyBoYWNrLiAgQXBwYXJlbnRseSBpT1MgaWdub3JlcyB3aWR0aHMgc2V0XG4gICAgICAgICAgICAgICAgLy8gd2l0aCBhIG5vbi1waXhlbCB2YWx1ZSwgd2hpY2ggbWVhbnMgaUZyYW1lcyBnZXQgZXhwYW5kZWRcbiAgICAgICAgICAgICAgICAvLyB0byB0aGUgZnVsbCB3aWR0aCBvZiB0aGVpciBjb250ZW50LiAgU2V0dGluZyBhIHBpeGVsXG4gICAgICAgICAgICAgICAgLy8gdmFsdWUgYW5kIHRoZW4gdXNpbmcgYG1pbi13aWR0aGAgaXMgdGhlIHdvcmthcm91bmQgZm9yXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5cbiAgICAgICAgICAgICAgICAvLyBTZWU6ICBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzIzMDgzNDYyL2hvdy10by1nZXQtYW4taWZyYW1lLXRvLWJlLXJlc3BvbnNpdmUtaW4taW9zLXNhZmFyaVxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzTW9iaWxlKSB7XG4gICAgICAgICAgICAgICAgICAgIHN0eWxlc1snaWZyYW1lJ11bJ3dpZHRoJ10gPSAnMXB4JztcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVzWydpZnJhbWUnXVsnbWluLXdpZHRoJ10gPSAnMTAwJSc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAodGhpcy5pc01vYmlsZSkge1xuICAgICAgICAgICAgICAgIHZhciBtb2JpbGVEaW1zID0gdGhpcy5nZXRNb2JpbGVEaW1lbnNpb25zKCk7XG4gICAgICAgICAgICAgICAgLy8gQWRqdXN0IHRoZSBpRnJhbWUgc3R5bGUgdG8gZml0IHRoZSB3aG9sZSBzY3JlZW5cbiAgICAgICAgICAgICAgICBzdHlsZXNbJ3dyYXBwZXInXVsncG9zaXRpb24nXSA9ICdhYnNvbHV0ZSc7XG4gICAgICAgICAgICAgICAgc3R5bGVzWyd3cmFwcGVyJ11bJ3RvcCddID0gJzAnO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snd3JhcHBlciddWydsZWZ0J10gPSAnMCc7XG4gICAgICAgICAgICAgICAgc3R5bGVzWyd3cmFwcGVyJ11bJ3dpZHRoJ10gPSBtb2JpbGVEaW1zLndpZHRoU3RyaW5nO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snd3JhcHBlciddWydoZWlnaHQnXSA9IG1vYmlsZURpbXMuaGVpZ2h0U3RyaW5nO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snaWZyYW1lJ11bJ3Bvc2l0aW9uJ10gPSAnYWJzb2x1dGUnO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snaWZyYW1lJ11bJ3RvcCddID0gMDtcbiAgICAgICAgICAgICAgICBzdHlsZXNbJ2lmcmFtZSddWydsZWZ0J10gPSAwO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snaWZyYW1lJ11bJ3dpZHRoJ10gPSBtb2JpbGVEaW1zLndpZHRoU3RyaW5nO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snaWZyYW1lJ11bJ2hlaWdodCddID0gbW9iaWxlRGltcy5oZWlnaHRTdHJpbmc7XG4gICAgICAgICAgICAgICAgc3R5bGVzWydpZnJhbWUnXVsnYm9yZGVyJ10gPSAnbm9uZSc7XG4gICAgICAgICAgICAgICAgc3R5bGVzWydpZnJhbWUnXVsnYm94LXNoYWRvdyddID0gJ25vbmUnO1xuICAgICAgICAgICAgICAgIHN0eWxlc1snY2FuY2VsQnV0dG9uJ11bJ2Rpc3BsYXknXSA9ICdub25lJztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gQnVpbGQgb3ZlcmxheVxuICAgICAgICAgICAgaWYgKCF0aGlzLmlzSW5QYWdlKSB7XG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLm92ZXJsYXkpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVybGF5ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlcmxheS5zZXRBdHRyaWJ1dGUoJ2lkJywgJ2hzRW1iZWRkZWRPdmVybGF5Jyk7XG4gICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5vdmVybGF5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5vdmVybGF5LnNldEF0dHJpYnV0ZSgnc3R5bGUnLCAnZGlzcGxheTogYmxvY2s7Jyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIEJ1aWxkIHRoZSB3cmFwcGVyXG4gICAgICAgICAgICBpZiAoIXRoaXMud3JhcHBlcikge1xuICAgICAgICAgICAgICAgIHRoaXMud3JhcHBlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICAgICAgICAgIHRoaXMud3JhcHBlci5zZXRBdHRyaWJ1dGUoJ2lkJywgJ2hzRW1iZWRkZWRXcmFwcGVyJyk7XG5cbiAgICAgICAgICAgICAgICAvLyBIYWNrLiAgV2UgbmVlZCB0aGlzIG9uIG1vYmlsZSBiZWZvcmUgd2UgaW5zZXJ0IHRoZSBET01cbiAgICAgICAgICAgICAgICAvLyBlbGVtZW50LCBvdGhlcndpc2UgdGhlIG1vZGFsIGFwcGVhcnMgYWJvdmUgdGhlIGZvbGRcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5pc01vYmlsZSkge1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgMCk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpcy5jb250YWluZXIuYXBwZW5kQ2hpbGQodGhpcy53cmFwcGVyKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCF0aGlzLmlzSW5QYWdlKSB7XG4gICAgICAgICAgICAgICAgLy8gV2hlbiB0aGUgd2luZG93IGlzIHJlc2l6ZWQsIGFsc28gcmVzaXplIHRoZSBpZnJhbWUgaWYgbmVjZXNzYXJ5XG4gICAgICAgICAgICAgICAgLy8gTk9URTogT25seSBkbyB0aGlzIHdoZW4gdGhlIGlGcmFtZSBpcyBkaXNwbGF5ZWQgYXMgYSBwb3B1cCwgaXQgZG9lcyBub3QgcmVhbGx5IG1ha2Ugc2Vuc2Ugd2hlbiBpdCdzIGluLXBhZ2VcbiAgICAgICAgICAgICAgICAvLyBBbHNvIHVzZWQgZm9yIG5ldyBtb2JpbGUgdXhcbiAgICAgICAgICAgICAgICB3aW5kb3cub25yZXNpemUgPSByZXNpemVJRnJhbWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIEJ1aWxkIHRoZSBpRnJhbWVcbiAgICAgICAgICAgIGlmICghdGhpcy5pZnJhbWUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmlmcmFtZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xuICAgICAgICAgICAgICAgIHRoaXMuaWZyYW1lLnNldEF0dHJpYnV0ZSgnaWQnLCAnaHNFbWJlZGRlZEZyYW1lJyk7XG4gICAgICAgICAgICAgICAgdGhpcy53cmFwcGVyLmFwcGVuZENoaWxkKHRoaXMuaWZyYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuaWZyYW1lLnNldEF0dHJpYnV0ZSgnc3JjJywgZnJhbWVVcmwpO1xuICAgICAgICAgICAgdGhpcy5pZnJhbWUuc2V0QXR0cmlidXRlKCdzY3JvbGxpbmcnLCAnbm8nKTsgLy8gVGhpcyBuZWVkcyB0byBzdGF5IGFzICdubycgb3IgZWxzZSBpUGFkcywgZXRjLiBnZXQgYnJva2VuXG4gICAgICAgICAgICB0aGlzLmlmcmFtZS5zZXRBdHRyaWJ1dGUoJ2ZyYW1lYm9yZGVyJywgJzAnKTtcbiAgICAgICAgICAgIHRoaXMuaWZyYW1lLnNldEF0dHJpYnV0ZSgnaGVpZ2h0Jywgd2luZG93RGltcy5oZWlnaHRSYXcpO1xuXG4gICAgICAgICAgICAvLyBUT0RPOiBEZXRlY3RpbmcgJ2VtYmVkZGVkU2lnbicgaW4gdGhlIGZyYW1lVXJsIGlzIGEgaGFjay4gQ2xlYW5cbiAgICAgICAgICAgIC8vIHRoaXMgdXAgb25jZSB0aGUgZW1iZWRkZWQgY2xvc2UgYnV0dG9uIGhhcyBiZWVuIGltcGxlbWVudGVkIGZvclxuICAgICAgICAgICAgLy8gZW1iZWRkZWQgcmVxdWVzdGluZyBhbmQgdGVtcGxhdGVzLlxuICAgICAgICAgICAgaWYgKGZyYW1lVXJsLmluZGV4T2YoJ2VtYmVkZGVkU2lnbicpID09PSAtMSkge1xuICAgICAgICAgICAgICBpZiAoIXRoaXMuaXNJblBhZ2UgJiYgKHBhcmFtc1snYWxsb3dDYW5jZWwnXSA9PT0gdHJ1ZSB8fCBwYXJhbXNbJ2FsbG93Q2FuY2VsJ10gPT09IHVuZGVmaW5lZCkgJiYgIXRoaXMuY2FuY2VsQnV0dG9uKSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLmNhbmNlbEJ1dHRvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcbiAgICAgICAgICAgICAgICAgIHRoaXMuY2FuY2VsQnV0dG9uLnNldEF0dHJpYnV0ZSgnaWQnLCAnaHNFbWJlZGRlZENhbmNlbCcpO1xuICAgICAgICAgICAgICAgICAgdGhpcy5jYW5jZWxCdXR0b24uc2V0QXR0cmlidXRlKCdocmVmJywgJ2phdmFzY3JpcHQ6OycpO1xuICAgICAgICAgICAgICAgICAgdGhpcy5jYW5jZWxCdXR0b24ub25jbGljayA9IGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gQ2xvc2UgaUZyYW1lXG4gICAgICAgICAgICAgICAgICAgICAgSGVsbG9TaWduLmNsb3NlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gU2VuZCAnY2FuY2VsJyBtZXNzYWdlXG4gICAgICAgICAgICAgICAgICAgICAgaWYgKG1lc3NhZ2VMaXN0ZW5lcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBsKCdSZXBvcnRpbmcgY2FuY2VsYXRpb24nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZUxpc3RlbmVyKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdldmVudCc6IEhlbGxvU2lnbi5FVkVOVF9DQU5DRUxFRFxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgdGhpcy53cmFwcGVyLmFwcGVuZENoaWxkKHRoaXMuY2FuY2VsQnV0dG9uKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmICghcGFyYW1zWydhbGxvd0NhbmNlbCddICYmIHRoaXMuY2FuY2VsQnV0dG9uKSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLndyYXBwZXIucmVtb3ZlQ2hpbGQodGhpcy5jYW5jZWxCdXR0b24pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIEFkZCBpbmxpbmUgc3R5bGluZ1xuICAgICAgICAgICAgZm9yICh2YXIgayBpbiBzdHlsZXMpIHtcbiAgICAgICAgICAgICAgICB2YXIgZWwgPSB0aGlzW2tdO1xuICAgICAgICAgICAgICAgIGlmIChlbCkge1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpIGluIHN0eWxlc1trXSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbC5zdHlsZVtpXSA9IHN0eWxlc1trXVtpXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBJZ25vcmUgLSBleGNlcHRpb25zIGdldCB0aHJvd24gd2hlbiB0aGUgZ2l2ZW4gc3R5bGUgaXMgbm90IHN1cHBvcnRlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGwoZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5jYW5jZWxCdXR0b24gJiYgKHRoaXMuaXNGRiB8fCB0aGlzLmlzT3BlcmEpKSB7XG4gICAgICAgICAgICAgICAgLy8gRmlyZWZveCBpcyB3ZWlyZCB3aXRoIGJnIGltYWdlc1xuICAgICAgICAgICAgICAgIHZhciBzID0gdGhpcy5jYW5jZWxCdXR0b24uZ2V0QXR0cmlidXRlKCdzdHlsZScpO1xuICAgICAgICAgICAgICAgIHMgKz0gKHMgPyAnOyAnIDogJycpO1xuICAgICAgICAgICAgICAgIHMgKz0gJ2JhY2tncm91bmQtaW1hZ2U6ICcgKyBzdHlsZXMuY2FuY2VsQnV0dG9uWydiYWNrZ3JvdW5kLWltYWdlJ10gKyAnOyAnO1xuICAgICAgICAgICAgICAgIHMgKz0gJ2JhY2tncm91bmQtcG9zaXRpb246ICcgKyBzdHlsZXMuY2FuY2VsQnV0dG9uWydiYWNrZ3JvdW5kLXBvc2l0aW9uJ10gKyAnOyc7XG4gICAgICAgICAgICAgICAgdGhpcy5jYW5jZWxCdXR0b24uc2V0QXR0cmlidXRlKCdzdHlsZScsIHMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNJblBhZ2UgJiYgIXRoaXMuaXNNb2JpbGUpIHtcbiAgICAgICAgICAgICAgICAvLyBSdW4gcmVzaXplSUZyYW1lIHRvIG1ha2Ugc3VyZSBpdCBmaXRzIGJlc3QgZnJvbSB0aGUgYmVnaW5uaW5nXG4gICAgICAgICAgICAgICAgcmVzaXplSUZyYW1lKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmlzTW9iaWxlICYmIHdpbmRvdyA9PT0gd2luZG93LnRvcCkge1xuICAgICAgICAgICAgICAgIC8vIE9ubHkgc2V0IHRoZSBtZXRhIHRhZ3MgZm9yIHRoZSB0b3Agd2luZG93XG4gICAgICAgICAgICAgICAgTWV0YVRhZ0hlbHBlci5zZXQoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuaXNNb2JpbGUgJiYgIXRoaXMuaXNJblBhZ2UpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmZpeElmcmFtZSA9IGRlYm91bmNlKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgMCk7XG4gICAgICAgICAgICAgICAgfSwgMTAwMCk7XG4gICAgICAgICAgICAgICAgdGhpcy5maXhJZnJhbWUoKTtcbiAgICAgICAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5maXhJZnJhbWUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBDbG9zZSB0aGUgaWZyYW1lIGlmIHBhZ2UgZmFpbHMgdG8gaW5pdGlhbGl6ZSB3aXRoaW4gMTUgc2Vjb25kc1xuICAgICAgICAgICAgaWYgKHRoaXMuaGVhbHRoQ2hlY2tUaW1lb3V0TXMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9oZWFsdGhDaGVja1RpbWVvdXRIYW5kbGUgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgbWVzc2FnZSA9ICdTaWduZXIgcGFnZSBmYWlsZWQgdG8gaW5pdGlhbGl6ZSB3aXRoaW4gJyArIHNlbGYuaGVhbHRoQ2hlY2tUaW1lb3V0TXMgKyAnIG1pbGxpc2Vjb25kcy4nXG4gICAgICAgICAgICAgICAgICAgIHNlbGYucmVwb3J0RXJyb3IobWVzc2FnZSwgZG9jdW1lbnQubG9jYXRpb24uaHJlZik7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuY2xvc2UoKTtcbiAgICAgICAgICAgICAgICB9LCB0aGlzLmhlYWx0aENoZWNrVGltZW91dE1zKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gU3RhcnQgbGlzdGVuaW5nIGZvciBtZXNzYWdlcyBmcm9tIHRoZSBpRnJhbWVcbiAgICAgICAgICAgIFhXTS5yZWNlaXZlKGZ1bmN0aW9uIF9wYXJlbnRXaW5kb3dDYWxsYmFjayhldnQpe1xuICAgICAgICAgICAgICAgIHZhciBzb3VyY2UgPSBldnQuc291cmNlIHx8ICdoc0VtYmVkZGVkRnJhbWUnO1xuXG4gICAgICAgICAgICAgICAgaWYgKGV2dC5kYXRhID09PSAnaW5pdGlhbGl6ZScpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGYuaGVhbHRoQ2hlY2tUaW1lb3V0TXMpIGNsZWFyVGltZW91dChzZWxmLl9oZWFsdGhDaGVja1RpbWVvdXRIYW5kbGUpO1xuICAgICAgICAgICAgICAgICAgICAvLyByZW1vdmUgY29udGFpbmVyIGZyb20gcGF5bG9hZCB0byBwcmV2ZW50IGNpcmN1bGFyIHJlZmVyZW5jZSBlcnJvclxuICAgICAgICAgICAgICAgICAgICB2YXIgcGF5bG9hZCA9IE9iamVjdC5hc3NpZ24oe30sIHBhcmFtcyk7XG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBwYXlsb2FkLmNvbnRhaW5lcjtcbiAgICAgICAgICAgICAgICAgICAgWFdNLnNlbmQoSlNPTi5zdHJpbmdpZnkoeyB0eXBlOiAnZW1iZWRkZWRDb25maWcnLCBwYXlsb2FkOiBwYXlsb2FkIH0pLCBldnQub3JpZ2luLCBzb3VyY2UpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZ0LmRhdGEgPT0gJ2Nsb3NlJykge1xuICAgICAgICAgICAgICAgICAgICAvLyBDbG9zZSBpRnJhbWVcbiAgICAgICAgICAgICAgICAgICAgSGVsbG9TaWduLmNsb3NlKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKG1lc3NhZ2VMaXN0ZW5lcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZUxpc3RlbmVyKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZXZlbnQnOiBIZWxsb1NpZ24uRVZFTlRfQ0FOQ0VMRURcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChldnQuZGF0YSA9PSAnZGVjbGluZScpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gQ2xvc2UgaUZyYW1lXG4gICAgICAgICAgICAgICAgICAgIEhlbGxvU2lnbi5jbG9zZSgpO1xuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlTGlzdGVuZXIoe1xuICAgICAgICAgICAgICAgICAgICAgICAgJ2V2ZW50JzogSGVsbG9TaWduLkVWRU5UX0RFQ0xJTkVEXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZ0LmRhdGEgPT0gJ3JlYXNzaWduJykge1xuICAgICAgICAgICAgICAgICAgbWVzc2FnZUxpc3RlbmVyKHtcbiAgICAgICAgICAgICAgICAgICAgJ2V2ZW50JzogSGVsbG9TaWduLkVWRU5UX1JFQVNTSUdORURcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZ0LmRhdGEgPT0gJ3VzZXItZG9uZScpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gQ2xvc2UgaUZyYW1lXG4gICAgICAgICAgICAgICAgICAgIEhlbGxvU2lnbi5jbG9zZSgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGV2dC5kYXRhID09PSAnc3RyaW5nJyAmJiBldnQuZGF0YS5pbmRleE9mKCdoZWxsbzonKSA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAvLyBIZWxsbyBtZXNzYWdlIC0gRXh0cmFjdCB0b2tlbiBhbmQgc2VuZCBpdCBiYWNrXG4gICAgICAgICAgICAgICAgICAgIHZhciBwYXJ0cyA9IGV2dC5kYXRhLnNwbGl0KCc6Jyk7XG4gICAgICAgICAgICAgICAgICAgIHZhciB0b2tlbiA9IHBhcnRzWzFdO1xuICAgICAgICAgICAgICAgICAgICBYV00uc2VuZCgnaGVsbG9iYWNrOicgKyB0b2tlbiwgZnJhbWVVcmwsIHNvdXJjZSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChtZXNzYWdlTGlzdGVuZXIgJiYgZXZ0LmRhdGEgJiYgdHlwZW9mIGV2dC5kYXRhID09PSAnc3RyaW5nJykge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIEZvcndhcmQgdG8gbWVzc2FnZSBjYWxsYmFja1xuICAgICAgICAgICAgICAgICAgICB2YXIgZXZlbnREYXRhID0ge307XG4gICAgICAgICAgICAgICAgICAgIHZhciBwLCBwYWlycyA9IGV2dC5kYXRhLnNwbGl0KCcmJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gUmVjdXJzaXZlIGhlbHBlciBmdW5jdGlvbiB0byBkZXNlcmlhbGl6ZSB0aGUgZXZlbnQgZGF0YS5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGRlc2VyaWFsaXplRXZlbnREYXRhID0gZnVuY3Rpb24oc3RyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgb2JqID0gc3RyO1xuICAgICAgICAgICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBTYWZlbHkgcGFyc2UgdGhlIHN0cmluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9iaiA9IEpTT04ucGFyc2Uoc3RyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9iaiA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIga2V5IGluIG9iaikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JqW2tleV0gPSBwYXJzZUpzb24ob2JqW2tleV0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkgeyAvKiBpZ25vcmUgKi8gfVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG9iajtcbiAgICAgICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpPTA7IGk8cGFpcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHAgPSBwYWlyc1tpXS5zcGxpdCgnPScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHAubGVuZ3RoID09PSAyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhW3BbMF1dID0gZGVzZXJpYWxpemVFdmVudERhdGEoZGVjb2RlVVJJQ29tcG9uZW50KHBbMV0pKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlTGlzdGVuZXIoZXZlbnREYXRhKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCBvcmlnaW4pO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNsb3NlOiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgLy8gUmVzZXQgdmlld3BvcnQgc2V0dGluZ3NcbiAgICAgICAgICAgIGlmICh0aGlzLmlzTW9iaWxlICYmIHdpbmRvdyA9PT0gd2luZG93LnRvcCkge1xuICAgICAgICAgICAgICAgIE1ldGFUYWdIZWxwZXIucmVzdG9yZSgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsKCdDbG9zaW5nIEhlbGxvU2lnbiBlbWJlZGRlZCBpRnJhbWUnKTtcbiAgICAgICAgICAgIC8vIENsb3NlIHRoZSBjaGlsZCBpZnJhbWUgZnJvbSB0aGUgcGFyZW50IHdpbmRvd1xuICAgICAgICAgICAgaWYgKHRoaXMuaWZyYW1lKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmNhbmNlbEJ1dHRvbikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLndyYXBwZXIucmVtb3ZlQ2hpbGQodGhpcy5jYW5jZWxCdXR0b24pO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbmNlbEJ1dHRvbiA9IG51bGw7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMuX2ZhZGVPdXRJRnJhbWUoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuaXNNb2JpbGUpIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5maXhJZnJhbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG5cbiAgICAgICAgLy8gIC0tLS0gIFBSSVZBVEUgTUVUSE9EUyAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuXG4gICAgICAgIF9mYWRlT3V0SUZyYW1lOiBmdW5jdGlvbiBfZmFkZU91dElGcmFtZShjdXJyZW50T3BhY2l0eSkge1xuICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgaWYgKHNlbGYuaWZyYW1lKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFjdXJyZW50T3BhY2l0eSkge1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50T3BhY2l0eSA9IDEuMDtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50T3BhY2l0eSAtPSAwLjE7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHNlbGYuaWZyYW1lLnN0eWxlLm9wYWNpdHkgPSBjdXJyZW50T3BhY2l0eTtcbiAgICAgICAgICAgICAgICBzZWxmLmlmcmFtZS5zdHlsZS5maWx0ZXIgPSAnYWxwaGEob3BhY2l0eT0nICsgcGFyc2VJbnQoY3VycmVudE9wYWNpdHkgKiAxMDAsIDEwKSArICcpJztcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudE9wYWNpdHkgPD0gMC4wKSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuaWZyYW1lLnN0eWxlLm9wYWNpdHkgPSAwO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLmlmcmFtZS5zdHlsZS5maWx0ZXIgPSAnYWxwaGEob3BhY2l0eT0wKSc7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuaWZyYW1lLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dChhbmltYXRpb25UaW1lcik7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLm92ZXJsYXkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuY29udGFpbmVyLnJlbW92ZUNoaWxkKHNlbGYub3ZlcmxheSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgc2VsZi5jb250YWluZXIucmVtb3ZlQ2hpbGQoc2VsZi53cmFwcGVyKTtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi53cmFwcGVyLnJlbW92ZUNoaWxkKHNlbGYuaWZyYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5vdmVybGF5ID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5pZnJhbWUgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICBzZWxmLndyYXBwZXIgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZhciBhbmltYXRpb25UaW1lciA9IHNldFRpbWVvdXQoKGZ1bmN0aW9uKGN1cnJlbnRPcGFjaXR5KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuX2ZhZGVPdXRJRnJhbWUoY3VycmVudE9wYWNpdHkpO1xuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH0pKGN1cnJlbnRPcGFjaXR5KSwgMTApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlcG9ydEVycm9yOiBmdW5jdGlvbihlcnJvck1lc3NhZ2UsIHBhcmVudFVybCkge1xuICAgICAgICAgICAgWFdNLnNlbmQoe1xuICAgICAgICAgICAgICAgICdldmVudCc6IEhlbGxvU2lnbi5FVkVOVF9FUlJPUixcbiAgICAgICAgICAgICAgICAnZGVzY3JpcHRpb24nOiBlcnJvck1lc3NhZ2VcbiAgICAgICAgICAgIH0sIHBhcmVudFVybCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZW5zdXJlUGFyZW50RG9tYWluOiBmdW5jdGlvbihkb21haW5OYW1lLCBwYXJlbnRVcmwsIHRva2VuLCBza2lwRG9tYWluVmVyaWZpY2F0aW9uLCBjYWxsYmFjaykge1xuXG4gICAgICAgICAgICAvLyBkb21haW5OYW1lOiAgRG9tYWluIHRvIG1hdGNoIGFnYWluc3QgdGhlIHBhcmVudCB3aW5kb3cgbG9jYXRpb25cbiAgICAgICAgICAgIC8vIHBhcmVudFVybDogICBVcmwgb2YgdGhlIHBhcmVudCB3aW5kb3cgdG8gY2hlY2sgKHByb3ZpZGVkIHRvIHVzIGJ1dCBub3QgcmVsaWFibGUpXG4gICAgICAgICAgICAvLyBjYWxsYmFjazogICAgTWV0aG9kIHRvIGNhbGwgd2l0aCB0aGUgcmVzdWx0LCBpdCBzaG91bGQgdGFrZSBvbmx5IG9uZSBib29sZWFuIHBhcmFtZXRlci5cblxuICAgICAgICAgICAgaWYgKHdpbmRvdy50b3AgPT0gd2luZG93KSB7XG4gICAgICAgICAgICAgICAgLy8gTm90IGluIGFuIGlGcmFtZSwgbm8gbmVlZCB0byBnbyBmdXJ0aGVyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIHRva2VuICE9PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICAgIGVycm9yKCdUb2tlbiBub3Qgc3VwcGxpZWQgYnkgSGVsbG9TaWduLiBQbGVhc2UgY29udGFjdCBzdXBwb3J0LicpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBjYWxsYmFjayAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIGVycm9yKCdDYWxsYmFjayBub3Qgc3VwcGxpZWQgYnkgSGVsbG9TaWduLiBQbGVhc2UgY29udGFjdCBzdXBwb3J0LicpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICBpZiAoc2tpcERvbWFpblZlcmlmaWNhdGlvbiA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIHZhciB3YXJuaW5nTXNnID0gJ0RvbWFpbiB2ZXJpZmljYXRpb24gaGFzIGJlZW4gc2tpcHBlZC4gQmVmb3JlIHJlcXVlc3RpbmcgYXBwcm92YWwgZm9yIHlvdXIgYXBwLCBwbGVhc2UgYmUgc3VyZSB0byB0ZXN0IGRvbWFpbiB2ZXJpZmljYXRpb24gYnkgc2V0dGluZyBza2lwRG9tYWluVmVyaWZpY2F0aW9uIHRvIGZhbHNlLic7XG4gICAgICAgICAgICAgICAgbCh3YXJuaW5nTXNnKTtcbiAgICAgICAgICAgICAgICBhbGVydCh3YXJuaW5nTXNnKTtcbiAgICAgICAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIFN0YXJ0cyB3YWl0aW5nIGZvciB0aGUgaGVsbG8gYmFjayBtZXNzYWdlXG4gICAgICAgICAgICAgICAgWFdNLnJlY2VpdmUoZnVuY3Rpb24gX2Vuc3VyZVBhcmVudERvbWFpbkNhbGxiYWNrKGV2dCl7XG4gICAgICAgICAgICAgICAgICAgIGlmIChldnQuZGF0YS5pbmRleE9mKCdoZWxsb2JhY2s6JykgPT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwYXJ0cyA9IGV2dC5kYXRhLnNwbGl0KCc6Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdmFsaWQgPSAocGFydHNbMV0gPT0gdG9rZW4pO1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2sodmFsaWQpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSwgZG9tYWluTmFtZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIFNlbmQgaGVsbG8gbWVzc2FnZVxuICAgICAgICAgICAgWFdNLnNlbmQoJ2hlbGxvOicgKyB0b2tlbiwgcGFyZW50VXJsKTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZXRXaW5kb3dEaW1lbnNpb25zOiBmdW5jdGlvbihjdXN0b21IZWlnaHQpIHtcbiAgICAgICAgICAgIHZhciBzY3JvbGxYID0gZ2V0U2Nyb2xsWCgpO1xuICAgICAgICAgICAgdmFyIHNjcm9sbFkgPSBnZXRTY3JvbGxZKCk7XG4gICAgICAgICAgICB2YXIgd2luZG93V2lkdGgsIHdpbmRvd0hlaWdodDtcblxuICAgICAgICAgICAgaWYgKHRoaXMuaXNPbGRJRSkge1xuICAgICAgICAgICAgICAgIHdpbmRvd1dpZHRoICAgPSBkb2N1bWVudC5ib2R5LmNsaWVudFdpZHRoO1xuICAgICAgICAgICAgICAgIHdpbmRvd0hlaWdodCAgPSBkb2N1bWVudC5ib2R5LmNsaWVudEhlaWdodDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgd2luZG93V2lkdGggICA9IHdpbmRvdy5pbm5lcldpZHRoO1xuICAgICAgICAgICAgICAgIHdpbmRvd0hlaWdodCAgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgaGVpZ2h0ID0gdGhpcy5pc0luUGFnZSAmJiBjdXN0b21IZWlnaHQgPyBjdXN0b21IZWlnaHQgOiBNYXRoLm1heCh0aGlzLk1JTl9IRUlHSFQsIHdpbmRvd0hlaWdodCAtIDYwKTtcblxuICAgICAgICAgICAgdmFyIHdpZHRoID0gTWF0aC5taW4odGhpcy5ERUZBVUxUX1dJRFRILCB3aW5kb3dXaWR0aCAqIHRoaXMuSUZSQU1FX1dJRFRIX1JBVElPKTtcblxuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAnd2lkdGhTdHJpbmcnOiAgd2lkdGggKyAncHgnLFxuICAgICAgICAgICAgICAgICdoZWlnaHRTdHJpbmcnOiBoZWlnaHQgKyAncHgnLFxuICAgICAgICAgICAgICAgICdoZWlnaHRSYXcnOiAgICBoZWlnaHQsXG4gICAgICAgICAgICAgICAgJ3Njcm9sbFgnOiAgICAgIHNjcm9sbFgsXG4gICAgICAgICAgICAgICAgJ3Njcm9sbFknOiAgICAgIHNjcm9sbFksXG4gICAgICAgICAgICAgICAgJ3RvcCcgOiAgICAgICAgIE1hdGgubWF4KDAsIHNjcm9sbFkgKyBwYXJzZUludCgod2luZG93SGVpZ2h0IC0gaGVpZ2h0KSAvIDIsIDEwKSkgKyAncHgnLFxuICAgICAgICAgICAgICAgICdsZWZ0JzogICAgICAgICBNYXRoLm1heCgwLCBwYXJzZUludCgod2luZG93V2lkdGggLSB0aGlzLkRFRkFVTFRfV0lEVEgpIC8gMiwgMTApKSArICdweCdcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0TW9iaWxlRGltZW5zaW9uczogZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHZhciBkaW1zO1xuXG4gICAgICAgICAgICB2YXIgc2NyZWVuV2lkdGggPSBzY3JlZW4ud2lkdGg7XG4gICAgICAgICAgICB2YXIgc2NyZWVuSGVpZ2h0ID0gc2NyZWVuLmhlaWdodDtcbiAgICAgICAgICAgIHZhciB3aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xuICAgICAgICAgICAgdmFyIHdpbmRvd0hlaWdodCA9IHdpbmRvdy5pbm5lckhlaWdodDtcblxuICAgICAgICAgICAgdmFyIGlzUG9ydHJhaXQgPSB3aW5kb3dIZWlnaHQgPiB3aW5kb3dXaWR0aDtcblxuICAgICAgICAgICAgaWYgKGlzUG9ydHJhaXQpIHtcbiAgICAgICAgICAgICAgICBkaW1zID0ge1xuICAgICAgICAgICAgICAgICAgICAnd2lkdGhTdHJpbmcnOiBzY3JlZW5XaWR0aCArICdweCcsXG4gICAgICAgICAgICAgICAgICAgICdoZWlnaHRTdHJpbmcnOiAnMTAwJSdcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBMYW5kc2NhcGVcbiAgICAgICAgICAgICAgICBkaW1zID0ge1xuICAgICAgICAgICAgICAgICAgICAnd2lkdGhTdHJpbmcnOiB3aW5kb3dXaWR0aCArICdweCcsXG4gICAgICAgICAgICAgICAgICAgICdoZWlnaHRTdHJpbmcnOiAnMTAwJSdcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gQWx3YXlzIGZpbGwgc2NyZWVuIG9uIG1vYmlsZVxuICAgICAgICAgICAgZGltcy50b3AgPSAnMCc7XG4gICAgICAgICAgICBkaW1zLmxlZnQgPSAnMCc7XG4gICAgICAgICAgICByZXR1cm4gZGltcztcbiAgICAgICAgfSxcblxuICAgICAgICBpbkFycmF5OiBmdW5jdGlvbih2LCBhcnJheSkge1xuICAgICAgICAgICAgaWYgKHRoaXMuaGFzSlF1ZXJ5KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICQuaW5BcnJheSh2LCBhcnJheSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChhcnJheSkge1xuICAgICAgICAgICAgICAgIGZvciAodmFyIGk9MDsgaTxhcnJheS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBpZiAoYXJyYXlbaV0gPT0gdikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gLTE7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2FmZVVybDogZnVuY3Rpb24odXJsKSB7XG4gICAgICAgICAgICBpZiAodXJsKSB7XG4gICAgICAgICAgICAgICAgdHJ5IHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBTZWN1cml0eTogcmVtb3ZlIHNjcmlwdCB0YWdzIGZyb20gVVJMcyBiZWZvcmUgcHJvY2Vzc2luZ1xuICAgICAgICAgICAgICAgICAgICB1cmwgPSB1cmwucmVwbGFjZSgvPC9nLCBcIiZsdDtcIik7XG4gICAgICAgICAgICAgICAgICAgIHVybCA9IHVybC5yZXBsYWNlKC8+L2csIFwiJmd0O1wiKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBIVE1MLURlY29kZSB0aGUgZ2l2ZW4gdXJsIGlmIG5lY2Vzc2FyeSwgYnkgcmVuZGVyaW5nIHRvIHRoZSBwYWdlXG4gICAgICAgICAgICAgICAgICAgIHZhciBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICAgICAgICAgICAgICBlbC5pbm5lckhUTUwgPSB1cmw7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkZWNvZGVkVXJsID0gZWwuaW5uZXJUZXh0O1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIEZhbGwgYmFjayB0byBqdXN0IHJlcGxhY2luZyAnJmFtcDsnIGluIGNhc2Ugb2YgZmFpbHVyZVxuICAgICAgICAgICAgICAgICAgICBpZiAoIWRlY29kZWRVcmwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybCA9IHVybC5yZXBsYWNlKC9cXCZhbXBcXDsvZywgJyYnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybCA9IGRlY29kZWRVcmw7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgbCgnQ291bGQgbm90IGRlY29kZSB1cmw6ICcgKyBlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gdXJsO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFdyYXBwZXIgdGhhdCB3aWxsIGVuc3VyZSBhbiBlcnJvciBtZXNzYWdlIGlzIGRpc3BsYXllZCwgZWl0aGVyIGluIGNvbnNvbGUubG9nXG4gICAgICogb3IgYXMgYSBicm93c2VyIGFsZXJ0LlxuICAgICAqIEBwYXJhbSBtZXNzYWdlIFN0cmluZyBlcnJvciBtZXNzYWdlXG4gICAgICovXG5cbiAgICBmdW5jdGlvbiBlcnJvcihtZXNzYWdlKSB7XG4gICAgICAgIGlmICh0eXBlb2YgbWVzc2FnZSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIGlmICh3aW5kb3cuY29uc29sZSAmJiBjb25zb2xlLmxvZykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKG1lc3NhZ2UpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBhbGVydChtZXNzYWdlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEN1c3RvbSB3cmFwcGVyIHRoYXQgY29uZGl0aW9uYWxseSBsb2dzIG1lc3NhZ2VzIHRvIGNvbnNvbGUubG9nLlxuICAgICAqIEBwYXJhbSBtZXNzYWdlT2JqIFN0cmluZyBvciBPYmplY3QgdG8gbG9nXG4gICAgICovXG4gICAgZnVuY3Rpb24gbChtZXNzYWdlT2JqKSB7XG4gICAgICAgIGlmIChIZWxsb1NpZ24uaXNEZWJ1Z0VuYWJsZWQgJiYgdHlwZW9mIG1lc3NhZ2VPYmogIT09ICd1bmRlZmluZWQnICYmXG4gICAgICAgICAgICB3aW5kb3cuY29uc29sZSAmJiBjb25zb2xlLmxvZykge1xuICAgICAgICAgICAgY29uc29sZS5sb2cobWVzc2FnZU9iaik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiAgR2V0dGVyIGZ1bmN0aW9ucyBmb3IgZGV0ZXJtaW5pbmcgc2Nyb2xsIHBvc2l0aW9uIHRoYXQgd29yayBvbiBhbGxcbiAgICAgKiAgYnJvd3NlcnMuXG4gICAgICovXG5cbiAgICBmdW5jdGlvbiBnZXRTY3JvbGxYKCkge1xuICAgICAgICByZXR1cm4gX3N1cHBvcnRQYWdlT2Zmc2V0KCkgPyB3aW5kb3cucGFnZVhPZmZzZXQgOiBfaXNDU1MxQ29tcGF0KCkgPyBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsTGVmdCA6IGRvY3VtZW50LmJvZHkuc2Nyb2xsTGVmdDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBnZXRTY3JvbGxZKCkge1xuICAgICAgICByZXR1cm4gX3N1cHBvcnRQYWdlT2Zmc2V0KCkgPyB3aW5kb3cucGFnZVlPZmZzZXQgOiBfaXNDU1MxQ29tcGF0KCkgPyBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wIDogZG9jdW1lbnQuYm9keS5zY3JvbGxUb3A7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gX2lzQ1NTMUNvbXBhdCgpIHtcbiAgICAgICAgcmV0dXJuICgoZG9jdW1lbnQuY29tcGF0TW9kZSB8fCAnJykgPT09ICdDU1MxQ29tcGF0Jyk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gX3N1cHBvcnRQYWdlT2Zmc2V0KCkge1xuICAgICAgICByZXR1cm4gd2luZG93LnBhZ2VYT2Zmc2V0ICE9PSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgLy8gQWxzbyBhZGQgSGVsbG9TaWduIGFzIGdsb2JhbCB2YXJpYWJsZSBmb3IgQU1EIGltcGxlbWVudGF0aW9ucy5cbiAgICAvLyBUaGlzIHdpbGwgcHJldmVudCBvbGRlciBpbnRlZ3JhdGlvbnMgZnJvbSBicmVha2luZy5cbiAgICBpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XG4gICAgICB3aW5kb3cuSGVsbG9TaWduID0gSGVsbG9TaWduO1xuICAgIH1cblxuICAgIC8vIEV4cG9ydCB0aGUgSFMgb2JqZWN0XG4gICAgbW9kdWxlLmV4cG9ydHMgPSBIZWxsb1NpZ247XG5cbn0pKCk7XG4iXSwic291cmNlUm9vdCI6IiJ9
