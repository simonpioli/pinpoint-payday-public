<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTimeImmutable();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('question')->truncate();
        DB::table('question')->insert([
            [
                'lender_type_id' => 1,
                'question' => 'I felt trapped in a cycle of high interest debt',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'The lender did not make clear to me this loan could adversley effect my credit',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'I was suffering with an addiction problem such as alcohol or drugs',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'I could not afford every day items such as nappies, clothing and food',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'The lender encouraged me to borrow more money',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'The lender did not refer me for debt advice',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'The lender did not carry out income and expenditure checks',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'This has left me feeling stressed/depressed',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'This has caused me relationship problems',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'question' => 'This made me incur bank charges, fees and/or interest',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'The lender should have seen from my credit record that I had recently missed payments',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'The lender should have seen from my credit record that I was in a debt management plan/ IVA',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'The lender did not ask me about my expenses in enough detail.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'I was on a low income/My only income was my pension/My only income was from benefits.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'I have health problems and get ESA/PIP/DLA but the lender did not ask me if I had any extra expenses because of this.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'The lender did not ask me for proof of my income or expenses.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'When I topped up the loan the lender did not ask me if my circumstances had changed',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'question' => 'When I topped up the loan  they ignored the fact that I had made several payments late to them',
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
