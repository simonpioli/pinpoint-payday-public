<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTimeImmutable();
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('lender_type')->truncate();
        DB::table('lender_type')->insert([
            ['name' => 'Payday Loans'],
            ['name' => 'Guarantor Loans']
        ]);
        DB::table('lender')->truncate();
        DB::table('lender')->insert([
            [
                'lender_type_id' => 1,
                'name' => 'Quick Quid',
                'reference' => 100020,
                'email' => 'complaint@quickquid.co.uk',
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],

            [
                'lender_type_id' => 1,
                'name' => 'Sunny',
                'reference' => 100021,
                'email' => 'help@sunny.co.uk',
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'The Money Shop',
                'reference' => 100022,
                'email' => 'Crwork@themoneyshop.co.uk',
                'special_notes' => null,
                'active' => 0,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'Payday Express',
                'reference' => 100023,
                'email' => 'Crwork@themoneyshop.co.uk',
                'special_notes' => null,
                'active' => 0,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'Payday UK',
                'reference' => 100024,
                'email' => 'Crwork@themoneyshop.co.uk',
                'special_notes' => null,
                'active' => 0,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'Wage Day Advance',
                'reference' => 100025,
                'email' => 'complaints@wagedayadvance.co.uk',
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'My Jar',
                'reference' => 100026,
                'email' => 'complaints@myjar.com',
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => '247 MoneyBox',
                'reference' => 100027,
                'email' => 'customer.services@247moneybox.com',
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'Peachy',
                'reference' => 100030,
                'email' => 'complaints@peachy.co.uk',
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'Piggybank',
                'reference' => 100029,
                'email' => 'complaints@piggy-bank.co.uk',
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'Satsuma',
                'reference' => 100028,
                'email' => 'onlinecustomercare@satsuma-loans.co.uk',
                'special_notes' => null,
                'active' => 0,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 1,
                'name' => 'Wonga',
                'reference' => 100019,
                'email' => 'customercomplaints@wonga.com',
                'special_notes' => null,
                'active' => 0,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'Amigo',
                'reference' => 100863,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'Bamboo',
                'reference' => 100864,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'Buddy Loans',
                'reference' => 100865,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'Consollo',
                'reference' => 100877,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'George Banco',
                'reference' => 100866,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'Guarantormyloan',
                'reference' => 100868,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'LendFair',
                'reference' => 100871,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'Suco',
                'reference' => 100872,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'TFS',
                'reference' => 100873,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => 'UKCredit',
                'reference' => 100875,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'lender_type_id' => 2,
                'name' => '1 plus 1',
                'reference' => 100876,
                'email' => null,
                'special_notes' => null,
                'active' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
