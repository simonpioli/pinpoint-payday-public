<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLenderTypeColumnToLenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('lender', function (Blueprint $table) {
            $table->integer('lender_type_id')->unsigned()->after('id');
            $table->foreign('lender_type_id')->references('id')->on('lender_type');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('lender', function (Blueprint $table) {
            $table->dropForeign(['lender_type_id']);
            $table->dropColumn('lender_type_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
