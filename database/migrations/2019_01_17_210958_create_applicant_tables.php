<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title', 5)->nullable(false);
            $table->string('first_name')->nullable(false);
            $table->string('last_name')->nullable(false);
            $table->date('date_of_birth')->nullable(false);
            $table->string('email')->nullable(false);
            $table->string('telephone', 15)->nullable(true);
            $table->boolean('iva_bankrupt')->nullable(false);
            $table->boolean('t_and_c')->nullable(false);
            $table->boolean('signed')->nullable(false)->default(false);
            $table->timestamps();
        });

        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('applicant_id');
            $table->boolean('primary');
            $table->string('postcode', 15)->nullable(false);
            $table->string('address_line_1')->nullable(false);
            $table->string('address_line_2')->nullable(true);
            $table->string('town')->nullable(false);
            $table->string('county')->nullable(false);
            $table->timestamps();

            $table->foreign('applicant_id')
                ->references('id')
                ->on('applicant')
                ->onDelete('cascade');
        });

        Schema::create('lender', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45)->nullable(false);
            $table->string('email', 100)->nullable(true);
            $table->text('special_notes')->nullable(true);
            $table->timestamps();
        });

        Schema::create('question', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question')->nullable(false);
            $table->timestamps();
        });

        Schema::create('applicant_lender', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('applicant_id');
            $table->unsignedInteger('lender_id');
            $table->string('number_loans', 5)->nullable(false);
            $table->boolean('previous_claim')->nullable(false);
            $table->integer('first_year')->nullable(false);
            $table->integer('last_year')->nullable(false);
            $table->boolean('pre_screen')->nullable(true);
            $table->timestamps();

            $table->foreign('applicant_id')->references('id')->on('applicant');
            $table->foreign('lender_id')->references('id')->on('lender');
        });

        Schema::create('applicant_lender_question', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('applicant_lender_id');
            $table->unsignedInteger('question_id');
            $table->timestamps();

            $table->foreign('applicant_lender_id')
                ->references('id')
                ->on('applicant_lender');
            $table->foreign('question_id')->references('id')->on('question');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_lender_question');
        Schema::dropIfExists('applicant_lender');
        Schema::dropIfExists('question');
        Schema::dropIfExists('lender');
        Schema::dropIfExists('address');
        Schema::dropIfExists('applicant');

    }
}
