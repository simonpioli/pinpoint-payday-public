<?php

namespace App\Console\Commands;

use App\Events\GenerateDocuments;
use App\Models\Applicant;
use App\Services\ProclaimXml;
use \Exception;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class ExportRecentNoDocsApplicants extends Command
{
    /** @var ProclaimXml */
    private $proclaimXml;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'applicants:export:nodocs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exports and uploads all applicants without documents, not yet exported';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->proclaimXml = new ProclaimXml();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): bool
    {
        // Fetch applicants
        $applicants = $this->collectRecentValidApplicants();
        if ($applicants->count() === 0) {
            logger()->info('No applicants to export');
            return false;
        }

        // Build XML
        try {
            foreach ($applicants as $applicant) {
                if ($applicant->signature_request_id === null) {
                    event(new GenerateDocuments($applicant));
                }

                $xml = $this->proclaimXml->generateXmlObject($applicant, false, false);
            }
        } catch (Exception $e) {
            logger()->error('Unable to generate XML files');
            report($e);
            return false;
        }

        return true;
    }

    /**
     * @return Collection
     */
    private function collectRecentValidApplicants(): Collection
    {
        $applicants = Applicant::where([
            ['iva_bankrupt', 0],
            ['t_and_c', 1],
            ['signed', 1],
            ['exported_at', null]
        ])->get();

        return $applicants;
    }
}
