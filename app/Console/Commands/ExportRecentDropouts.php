<?php

namespace App\Console\Commands;

use App\Mail\DropoutReport;
use App\Models\Applicant;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection as ArrayCollection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use League\Csv\CannotInsertRecord;
use League\Csv\Writer;
use \TypeError;

class ExportRecentDropouts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'applicants:report:dropouts {--all : Export all dropouts, including those already exported}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reports all recent applicants that dropped out before signing';

    /** @var Carbon */
    protected $now;

    /** @var string */
    protected $localFileLocation;

    /** @var string */
    protected $fileName;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->now = Carbon::now();
        $this->localFileLocation = Storage::disk('local')
                ->getAdapter()
                ->getPathPrefix() . config('app.csvExportLocation');

        $this->fileName = 'dropouts-' . $this->now->format('Y-m-d_H-i');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Initialise the file
        touch($this->localFileLocation . $this->fileName . '.csv');

        // Decide what to collect based on flags passed to command
        $where = [
            ['iva_bankrupt', 0],
            ['t_and_c', 1],
            ['signed', 0],
            ['exported_at', null],
        ];

        if (!$this->option('all')) {
            $where[] = ['reported_at', null];
        }

        // Fetch Applicants
        $rawApplicantsQuery = Applicant::where($where)->orderBy('created_at', 'asc');

        $droppedOutApplicants = $this->collectDroppedOutApplicants($rawApplicantsQuery->get());
        if ($droppedOutApplicants->count() === 0) {
            logger()->info('No applicants to export');
            return false;
        }

        // Initiate CSV
        try {
            $csv = Writer::createFromPath($this->localFileLocation . $this->fileName . '.csv');
            $csv->insertOne(array_keys($droppedOutApplicants->first()));
        } catch (CannotInsertRecord $e) {
            logger()->error('Unable to initiate CSV');
            report($e);
            return false;
        }

        // Add records to CSV
        try {
            $csv->insertAll($droppedOutApplicants->toArray());
        } catch (TypeError $e) {
            logger()->error('Not an array of applicants');
            report($e);
            return false;
        } catch (CannotInsertRecord $e) {
            logger()->error('Unable to add record to CSV');
            report($e);
            return false;
        }

        $report = Storage::disk('local')->exists(config('app.csvExportLocation') . $this->fileName . '.csv');

        // Email exported
        if ($report) {
            Mail::to(config('mail.report_to'))
                ->send(new DropoutReport(
                    $this->now,
                    $this->localFileLocation . $this->fileName . '.csv'
                ));
        }

        // Set applicants as reported
        if (!$this->option('all')) {
            $rawApplicantsQuery->update(['reported_at' => $this->now]);
        }

        return true;
    }

    /**
     * @param EloquentCollection $rawApplicants
     * @return ArrayCollection
     */
    private function collectDroppedOutApplicants(EloquentCollection $rawApplicants): ArrayCollection
    {
        $applicants = $rawApplicants->map(function ($applicant) {
            switch ($applicant->identity_result) {
                case null:
                    $decision = 'Not checked';
                    break;
                case -1:
                    $decision = 'Reject';
                    break;
                case 0:
                    $decision = 'Refer';
                    break;
                case 1:
                    $decision = 'Accept';
                    break;
                default:
                    $decision = '';
                    break;
            }

            $returnApplicant = [
                'date' => $applicant->created_at,
                'title' => $applicant->title,
                'first_name' => $applicant->first_name,
                'last_name' => $applicant->last_name,
                'date_of_birth' => $applicant->date_of_birth,
                'email' => $applicant->email,
                'telephone' => $applicant->telephone,
                'source' => $applicant->source,
                'campaign' => $applicant->campaign,
                'id_result' => $decision,
                'id_reason' => $applicant->identity_reason,
                'address_line_1' => '',
                'address_line_2' => '',
                'town' => '',
                'county' => '',
                'postcode' => '',
                'dropped_page' => '',
            ];

            try {
                $address = $applicant->address()->where('primary', 1)->firstOrFail();
                $returnApplicant['address_line_1'] = $address->address_line_1;
                $returnApplicant['address_line_2'] = $address->address_line_2;
                $returnApplicant['town'] = $address->town;
                $returnApplicant['county'] = $address->county;
                $returnApplicant['postcode'] = $address->postcode;
            } catch (ModelNotFoundException $e) {
                // Do nothing
            }

            switch (true) {
                case ($applicant->identity_result !== 1):
                    $returnApplicant['dropped_page'] = 'Identity Check';
                    break;

                case ($applicant->applicantLender()->count() === 0):
                    $returnApplicant['dropped_page'] = 'Lenders';
                    break;

                case ($applicant->confirmed === 0):
                    $returnApplicant['dropped_page'] = 'Confirmation';
                    break;

                case ($applicant->signed === 0):
                    $returnApplicant['dropped_page'] = 'Documents';
                    break;
            }

            return $returnApplicant;
        });

        return $applicants;
    }
}
