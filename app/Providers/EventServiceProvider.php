<?php

namespace App\Providers;

use App\Events\ApplicationComplete;
use App\Events\GenerateDocuments;
use App\Listeners\SendLetterOfAuthorityEmail;
use App\Listeners\SendTermsEmail;
use App\Listeners\XmlGenerationListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ApplicationComplete::class => [
            XmlGenerationListener::class,
        ],
        GenerateDocuments::class => [
            SendLetterOfAuthorityEmail::class,
            SendTermsEmail::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
