<?php

namespace App\Services;

use App\Exceptions\DocumentsNotReady;
use App\Exceptions\ZipError;
use App\Models\Address;
use App\Models\Applicant;
use App\Models\ApplicantLender;
use App\Models\ApplicantLenderQuestion;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Http\File;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use LogicException;
use RuntimeException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use \Exception;
use \SimpleXMLElement;
use \ZipArchive;

class ProclaimXml
{
    private $helloSign;

    /** @var \Illuminate\Config\Repository|string */
    private $localFileLocation;

    /** @var string */
    private $remoteFileLocation;

    /** @var string */
    private $fileName;

    /** @var string */
    private $documentExportLocation;

    public function __construct()
    {
        $this->helloSign = new HelloSign();
        $this->localFileLocation = config('app.xmlExportLocation');
        $this->remoteFileLocation = App::environment(['local', 'staging']) ? App::environment() . '/' : '/';
        $this->documentExportLocation = Storage::disk('local')
                ->getAdapter()
                ->getPathPrefix() . config('app.documentExportLocation');
    }

    /**
     * @param Applicant $applicant
     * @param bool $uploadToFtp
     * @param bool $retrieveDocs
     * @return bool
     */
    public function generateXmlObject(Applicant $applicant, $uploadToFtp = true, $retrieveDocs = true)
    {
        // File Setup
        $this->fileName = $applicant->id . '_' . Carbon::now()->format('Y-m-d_H-i');

        // Retrieve addresses and lender/question data for applicant
        $primaryAddress = $applicant->address()->where('primary', '=', true)->first();
        $addresses = $applicant->address()->where('primary', '=', false)->get();
        $applicantLenders = $applicant->applicantLender()->with(['applicantLenderQuestion.question', 'lender'])->get();

        // Initialise XML
        $xml = new SimpleXMLElement('<Root/>');

        // Client
        $client = $xml->addChild('Client');
        $client->addChild('Client_Title', $applicant->title);
        $client->addChild('Client_FirstName', $applicant->first_name);
        $client->addChild('Client_LastName', $applicant->last_name);
        $client->addChild('Client_Dob', $applicant->date_of_birth->format('d/m/Y'));
        $client->addChild('Client_EmailAddress', $applicant->email);
        $client->addChild('Client_TelephoneNumber', $applicant->telephone);

        $xml->addChild('Source', htmlspecialchars($applicant->source));
        $xml->addChild('Campaign', htmlspecialchars($applicant->campaign));

        $xml->addChild('Marketing', ($applicant->marketing === 1 ? 'Yes' : 'No'));

        // ID Check
        $id = $xml->addChild('IdentityCheck');

        switch ($applicant->identity_result) {
            case null:
                $decision = 'Not checked';
                break;
            case -1:
                $decision = 'Reject';
                break;
            case 0:
                $decision = 'Refer';
                break;
            case 1:
                $decision = 'Accept';
                break;
            default:
                $decision = '';
                break;
        }

        // TODO: Store Experian Reference and add to XML?
        $id->addChild('Decision', $decision);
        $id->addChild('Reason', $applicant->identity_reason);

        // Client Primary Address
        $this->addressToXml($client->addChild('Current_Address'), $primaryAddress);

        // Client Other Addresses
        $previousAddresses = $client->addChild('PreviousAddresses');
        foreach ($addresses as $address) {
            $this->addressToXml(
                $previousAddresses->addChild('Address'),
                $address
            );
        }

        // Lenders
        $lendersXml = $xml->addChild('Lenders');
        foreach ($applicantLenders as $applicantLender) {
            $this->applicantLenderToXml($lendersXml->addChild('Lender'), $applicantLender);
        }

        if ($retrieveDocs) {
            // Document(s)
            $documents = false;
            if ($applicant->signature_request_id !== null) {
                $zipName = $this->documentExportLocation . $this->fileName . '.zip';
                $documents = $this->fetchDocumentsFromHelloSign($applicant, $zipName);
            }

            if (!$documents) {
                return false;
            }

            $documentsXml = $xml->addChild('Docs');
            $pdfs = $this->pdfsToBase64($this->documentExportLocation . $this->fileName);
            foreach ($pdfs as $pdf) {
                $docXml = $documentsXml->addChild('Doc');
                $docXml->addChild('Doc_name', $pdf['filename']);
                $docXml->addChild('Base64', $pdf['base64']);
                $docXml->addChild('File_Suffix', 'pdf');
            }
        }

        // Save XML to local disk
        try {
            $this->exportXmlToStorage($xml);
        } catch (LogicException $e) {
            logger()->error('Unable to write XML file to storage');
            report($e);
            return false;
        } catch (Exception $e) {
            logger()->error('Unable to write XML file to storage');
            report($e);
            return false;
        }

        if ($uploadToFtp) {
            // Save XML to FTP site
            try {
                $this->uploadXmlToFtpSite();
            } catch (FileNotFoundException $e) {
                logger()->error('Unable to find XML file to upload');
                report($e);
                return false;
            } catch (RuntimeException $e) {
                logger()->error('Unable to upload XML file to FTP site');
                report($e);
                return false;
            } catch (Exception $e) {
                logger()->error('Unable to upload XML file to FTP site');
                report($e);
                return false;
            }
        }

        $applicant->exported_at = Carbon::now();
        $applicant->save();

        return true;
    }

    /**
     * @param SimpleXMLElement $xml
     * @return bool
     */
    public function exportXmlToStorage(SimpleXMLElement $xml): bool
    {
        return Storage::put($this->localFileLocation . $this->fileName . '.xml', $xml->asXML());
    }

    /**
     * @return bool
     */
    private function uploadXmlToFtpSite(): bool
    {
        return Storage::disk('ftp')->putFileAs(
            $this->remoteFileLocation,
            new File(storage_path('app') . '/' . $this->localFileLocation . $this->fileName . '.xml'),
            $this->fileName . '.xml'
        );
    }

    /**
     * @param SimpleXMLElement $xml
     * @param Address $address
     * @return SimpleXMLElement
     */
    private function addressToXml(SimpleXMLElement $xml, Address $address): SimpleXMLElement
    {
        // Concatenate the Experian fields into a single line for the purposes of importing to ProClaim
        $line1 = !empty($address->flat_no) ? $address->flat_no . ', ' : '';
        $line1 .= !empty($address->building_no_name) ? $address->building_no_name . ', ' : '';
        $line1 .= $address->address_line_1;

        $xml->addChild('Client_Add_1', htmlspecialchars($line1));
        $xml->addChild('Client_Add_2', htmlspecialchars($address->address_line_2));
        $xml->addChild('Client_Add_3', htmlspecialchars($address->town));
        $xml->addChild('Client_PostCode', $address->postcode);

        return $xml;
    }

    /**
     * @param SimpleXMLElement $xml
     * @param ApplicantLender $applicantLender
     * @return SimpleXMLElement
     */
    private function applicantLenderToXml(SimpleXMLElement $xml, ApplicantLender $applicantLender): SimpleXMLElement
    {
        $xml->addChild('Lender_Name', $applicantLender->lender->name);
        $xml->addChild('Lender_Reference', $applicantLender->lender->reference);
        $xml->addChild('Lender_ValidClaim', ($applicantLender->pre_screen === 1 ? 'true' : 'false')); // Don't want to apply a mutator for this in the Model due to the front-end
        $xml->addChild('Loans', $applicantLender->number_loans);
        $xml->addChild('Claims', ($applicantLender->previous_claim === 1 ? 'Yes' : 'No'));
        $xml->addChild('First_Year', $applicantLender->first_year);
        if ($applicantLender->last_year !== 0) {
            $xml->addChild('Last_Year', $applicantLender->last_year);
        }
        foreach ($applicantLender->applicantLenderQuestion as $key => $alq) {
            $xml = $this->applicantLenderQuestionToXml($xml, $alq, $key + 1);
        }

        return $xml;
    }

    /**
     * @param SimpleXMLElement $xml
     * @param ApplicantLenderQuestion $applicantLenderQuestion
     * @param int $number
     * @return SimpleXMLElement
     */
    private function applicantLenderQuestionToXml(
        SimpleXMLElement $xml,
        ApplicantLenderQuestion $applicantLenderQuestion,
        $number): SimpleXMLElement
    {
        $question = $xml->addChild('Question_' . $number, 'true');
        $question->addAttribute('question', $applicantLenderQuestion->question->question);
        return $xml;
    }

    /**
     * @param Applicant $applicant
     * @param string $zipName
     * @return bool|string
     */
    private function fetchDocumentsFromHelloSign(Applicant $applicant, $zipName)
    {
        $documents = $this->helloSign->saveSignedDocumentsFromSignatureId(
            $applicant->signature_request_id,
            $applicant->email,
            $zipName
        );

        try {
            $documents = $this->extractPdfsFromZip($zipName, $this->documentExportLocation . $this->fileName);
        } catch (ErrorException $e) {
            throw new ZipError('Document Zip corrupt for ' . $applicant->email);
        }

        return $documents;
    }

    /**
     * @param string $source
     * @param string $destination
     * @return bool
     */
    private function extractPdfsFromZip($source, $destination): bool
    {
        $zip = new ZipArchive;
        if ($zip->open($source)) {
            return $zip->extractTo($destination);
        }

        return false;
    }

    /**
     * @param string $source
     * @return array
     */
    private function pdfsToBase64($source): array
    {
        $files = [];
        $directoryContents = array_diff(scandir($source), ['..', '.']); // Diff to drop the Linux dot crap

        foreach ($directoryContents as $fileName) {
            $files[] = [
                'filename' => $fileName,
                 'base64' => base64_encode(file_get_contents($source . '/' . $fileName))
            ];
        }

        return $files;
    }
}
