<?php

namespace App\Services\Experian\Models;

use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\XmlRoot(name="searchResponse", namespace="http://corpwsdl2.oneninetwo", prefix="ns1")
 */
class SearchResponse
{

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("searchReturn")
     */
    public $searchReturn;

    /**
     * @return string
     */
    public function getSearchReturn(): string
    {
        return $this->searchReturn;
    }

    /**
     * @param string $searchReturn
     * @return SearchResponse
     */
    public function setSearchReturn(string $searchReturn): SearchResponse
    {
        $this->searchReturn = $searchReturn;
        return $this;
    }
}
