<?php


namespace App\Services\Experian;

use App\Models\Address;
use App\Models\Applicant;
use App\Services\Experian\Models\SearchResponse;
use Carbon\Carbon;
use DMT\Soap\Serializer\SoapDeserializationVisitorFactory;
use DMT\Soap\Serializer\SoapMessageEventSubscriber;
use DMT\Soap\Serializer\SoapSerializationVisitorFactory;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Exception;
use GuzzleHttp\Client;
use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use SimpleXMLElement;

class ProveId
{
    const CURRENT_ADDRESS = '1';

    const NOT_CURRENT_ADDRESS = '0';

    const LINKED_PREVIOUS_ADDRESS = '2';

    const LINKED_FORWARDING_ADDRESS = '3';

    /** @var string  */
    private $secret;

    /** @var string */
    private $publicKey;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var string */
    private $signature;

    /** @var string */
    private $wrapper;

    /** @var string */
    private $body;

    /** @var Client */
    private $client;

    private $serializer;

    public function __construct()
    {
        $this->secret = config('services.experian.secret');
        $this->publicKey = config('services.experian.publicKey');
        $this->username = config('services.experian.username');
        $this->password = config('services.experian.password');

        $this->generateSignature();

        $this->generateWrapper();

        $this->client = new Client([
            'base_uri' => config('services.experian.url')
        ]);

        $this->serializer = $this->initializeSerializer();
    }

    /**
     * @return SerializerInterface
     */
    private function initializeSerializer(): SerializerInterface
    {
        // TODO: Refactor when Doctrine updated to 2.0
        AnnotationRegistry::registerLoader('class_exists');

        $builder = SerializerBuilder::create()
            ->setSerializationVisitor('soap', new SoapSerializationVisitorFactory())
            ->setDeserializationVisitor('soap', new SoapDeserializationVisitorFactory())
            ->configureListeners(
                function (EventDispatcher $dispatcher) {
                    $dispatcher->addSubscriber(new SoapMessageEventSubscriber());
                }
            );

        return $builder->build();
    }

    /**
     * @return $this
     */
    private function generateSignature(): ProveId
    {
        $nowObj = new Carbon();
        $now = $nowObj->format('U');

        $hmac = base64_encode(
            hash_hmac(
                'sha256',
                $this->username . $this->password . $now,
                $this->secret,
                true
            )
        );
        $this->signature = $hmac . '_' . $now . '_' . $this->publicKey;
        return $this;
    }

    /**
     * @return $this
     */
    private function generateWrapper(): ProveId
    {
        $this->wrapper = '
            <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:head="http://xml.proveid.experian.com/xsd/Headers" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cor="http://corpwsdl2.oneninetwo">
                <soapenv:Header>
                    <head:Signature>{signature}</head:Signature>
                </soapenv:Header>
                <soapenv:Body>
                    <cor:search soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                        <xml xsi:type="xsd:string" xmlns:xs="http://www.w3.org/2000/XMLSchema-instance"><![CDATA[
                            {body}
                        ]]></xml>
                    </cor:search>
                </soapenv:Body>
            </soapenv:Envelope>';

        return $this;
    }

    /**
     * @param Applicant $applicant
     * @param Address $suppliedAddress
     * @return $this
     */
    private function generateBody(Applicant $applicant, Address $suppliedAddress): ProveId
    {
        $search = new SimpleXMLElement('<Search/>');

        $auth = $search->addChild('Authentication');
        $auth->addChild('Username', $this->username);
        $auth->addChild('Password', $this->password);

        // Country details
        $search->addChild('CountryCode', 'GBR');

        // Personal details
        $person = $search->addChild('Person');
        $name = $person->addChild('Name');
        $name->addChild('Title', strtoupper($applicant->title));
        $name->addChild('Forename', $applicant->first_name);
        $name->addChild('Surname', $applicant->last_name);
        $person->addChild('DateOfBirth', $applicant->date_of_birth->format('Y-m-d'));

        // Address
        $addresses = $search->addChild('Addresses');
        $address = $addresses->addChild('Address');
        $address->addAttribute('Current', self::CURRENT_ADDRESS); // 'Current' Address

        $subPremiseValue = null;
        $premiseValue = $suppliedAddress->building_no_name;
        if ($suppliedAddress->flat_no) {
            switch (true) {
                case str_contains($suppliedAddress->flat_no, 'Flat ') &&
                    preg_match('/[0-9]+[A-Za-z]?/', $suppliedAddress->building_no_name):
                    $premiseValue = str_replace('Flat ', '', $suppliedAddress->flat_no) . ' ' . $suppliedAddress->building_no_name;
                    break;

                case preg_match('/[0-9]+/', $suppliedAddress->flat_no) &&
                    preg_match('/[A-Za-z ]+/', $suppliedAddress->building_no_name):
                default:
                    $subPremiseValue = $suppliedAddress->flat_no;
                    break;
            }
        }
        if (!empty($subPremiseValue)) {
            $address->addChild('SubPremise', $suppliedAddress->flat_no);
        }
        $address->addChild('Premise', $premiseValue);
        $address->addChild('Postcode', $suppliedAddress->postcode);
        $address->addChild('CountryCode', 'GBR'); // We only work in the UK

        // Phone number
        if ($applicant->telephone) {
            $search
                ->addChild('Telephones')
                ->addChild('Telephone')
                ->addChild('Number', $applicant->telephone);
        }

        $search->addChild(
            'YourReference',
            str_replace(['@', '.'], ['_', '_'], $applicant->email) . '_' . $applicant->id
        );

        $searchOptions = $search->addChild('SearchOptions');
        $searchOptions->addChild('ProductCode', 'ProveID_KYC');
        $searchOptions->addChild('DecisionCode', 'PPDM');

        $searchXml = dom_import_simplexml($search);

        $body = str_replace('{signature}', $this->signature, $this->wrapper);
        $this->body = str_replace('{body}', $searchXml->ownerDocument->saveHTML(), $body);

        return $this;
    }

    /**
     * @param Applicant $applicant
     * @param Address $address
     * @return SimpleXMLElement
     */
    public function search(Applicant $applicant, Address $address): SimpleXMLElement
    {
        $this->generateBody($applicant, $address);
        $xml = new SimpleXMLElement('<Search />');

        try {
            $response = $this->client->post(
                config('services.experian.searchEndpoint'),
                [
                    'body' => $this->body,
                    'headers' => [
                        'Content-Type' => 'text/xml',
                        'SOAPAction' => 'Search'
                    ]
                ]
            );

            $content = $this->serializer->deserialize((string) $response->getBody(), SearchResponse::class, 'soap');
            $xml = new SimpleXMLElement($content->getSearchReturn());
        } catch (Exception $e) {
            $xml->addAttribute('Type', 'Error');
            $xml->addChild('message', $e->getMessage());
            logger()->error($e->getMessage());
            report($e);
        }

        return $xml;
    }
}
