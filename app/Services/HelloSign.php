<?php

namespace App\Services;

use App\Exceptions\DocumentsNotReady;
use App\Models\Applicant;
use Carbon\Carbon;
use HelloSign\BaseException;
use HelloSign\Client;
use HelloSign\EmbeddedResponse;
use HelloSign\EmbeddedSignatureRequest;
use HelloSign\Signature;
use HelloSign\TemplateSignatureRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Config\Repository;

class HelloSign
{
    /** @var Client */
    private $client;

    /** @var Repository|string */
    public $clientId;

    const TANDC_FIELD_FULL_NAME = 't_c_name';

    const LOA_FIELD_FULL_NAME = 'loa_full_name';
    const LOA_FIELD_DOB = 'loa_dob';
    const LOA_FIELD_ADDRESS = 'loa_address';
    const LOA_FIELD_ADDRESS_POSTCODE = 'loa_address_postcode';
    const LOA_FIELD_SIGNATURE_FULL_NAME = 'loa_sig_full_name';

    public function __construct()
    {
        $this->client = new Client(config('services.hellosign.key'));
        $this->clientId = config('services.hellosign.clientId');
    }

    /**
     * @param array $documents
     * @param Applicant $applicant
     * @return bool|Signature
     */
    private function createSignatureRequest($documents, Applicant $applicant)
    {
        $response = false;
        try {
            $request = new TemplateSignatureRequest();
            if (App::environment(['local', 'staging'])) {
                $request->enableTestMode();
            }
            foreach ($documents as $document) {
                $request->setTemplateId(config('services.hellosign.documents.' . $document));
            }
            $fullName = $applicant->first_name . ' ' . $applicant->last_name;
            $address = $applicant->address->first();

            // Concatenate all the address fields into a single line for the purposes of generating the documents
            $formAddress = !empty($address->flat_no) ? $address->flat_no . ', ' : '';
            $formAddress .= !empty($address->building_no_name) ? $address->building_no_name . ', ' : '';
            $formAddress .= $address->address_line_1;
            $formAddress .= !empty($address->address_line_2) ? ', ' . $address->address_line_2 : '';
            $formAddress .= ', ' . $address->town;

            $request->setSigner('Client', $applicant->email, $fullName);
            $request->setCustomFieldValue(self::LOA_FIELD_FULL_NAME, $fullName);
            $request->setCustomFieldValue(self::LOA_FIELD_DOB, $applicant->date_of_birth->format('d/m/Y'));
            $request->setCustomFieldValue(self::LOA_FIELD_ADDRESS, $formAddress);
            $request->setCustomFieldValue(self::LOA_FIELD_ADDRESS_POSTCODE, $address->postcode);
            $request->setCustomFieldValue(self::LOA_FIELD_SIGNATURE_FULL_NAME, $fullName); // FullName - Signature Section LOA
            $request->setCustomFieldValue(self::TANDC_FIELD_FULL_NAME, $fullName); // FullName - Signature Section T&C
            $embedded_request = new EmbeddedSignatureRequest($request, $this->clientId);
            if (App::environment(['local', 'staging'])) {
                $embedded_request->enableTestMode();
            }
            $signatureRequest = $this->client->createEmbeddedSignatureRequest($embedded_request);
            $signatures = $signatureRequest->getSignatures();
            $applicant->signature_request_id = $signatureRequest->getId();
            $applicant->save();
            $response = $signatures[0];
        } catch (BaseException $e) {
            logger()->error('Unable to generate Signature Request ID');
            report($e);
        }
        return $response;
    }

    /**
     * @param Signature $signature
     * @return bool|EmbeddedResponse
     */
    private function getSignRequestUrl($signature)
    {
        $signatureUrl = false;
        if (!$signature) {
            return $signatureUrl;
        }
        $signatureRequestId = $signature->getId();
        try {
            $signatureUrl = $this->client->getEmbeddedSignUrl($signatureRequestId);
            if (App::environment(['local', 'staging'])) {
                $signatureUrl->enableTestMode();
            }
        } catch (BaseException $e) {
            logger()->error('Unable to generate Signature URL');
            report($e);
        }

        return $signatureUrl;
    }

    /**
     * @param Applicant $applicant
     * @return bool|EmbeddedResponse
     */
    public function generateSignatureUrlForApplicant(Applicant $applicant)
    {
        $requestId = $this->createSignatureRequest(['combined'], $applicant);
        return $this->getSignRequestUrl($requestId);
    }

    /**
     * @param string $signatureRequestId
     * @param string $email
     * @param string|null $destination
     * @throws DocumentsNotReady
     * @return bool|string
     */
    public function saveSignedDocumentsFromSignatureId($signatureRequestId, $email, $destination = null)
    {
        $response = false;
        try {
            $signatureRequest = $this->client->getSignatureRequest($signatureRequestId);
            if (!$signatureRequest->isComplete()) {
                throw new DocumentsNotReady('Documents for signature request ' . $signatureRequestId . ' (' . $email . ') not ready');
                return $response;
            } else {
                $response = $this->client->getFiles($signatureRequestId, $destination, 'zip');
                return $response;
            }
        } catch (BaseException $e) {
            logger()->error('Unable to get requested documents');
            report($e);
        }

        return $response;
    }
}
