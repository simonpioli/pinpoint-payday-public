<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;

trait ApplicantCheckTrait
{
    /**
     * @param Request $request
     * @param $applicantId
     * @return bool
     */
    private function checkGuid(Request $request, $applicantId)
    {
        $valid = false;
        if ($applicantId && $request->session()->has('guid') && ($applicantId == $request->session()->get('guid'))) {
            $valid = true;
        }
        return $valid;
    }
}
