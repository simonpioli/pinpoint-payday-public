<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\ApplicantCheckTrait;
use App\Models\Applicant;
use App\Services\HelloSign;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class DocumentSignatureController extends BaseController
{
    use ApplicantCheckTrait;

    /**
     * @param Request $request
     * @param $applicantId
     * @return JsonResponse
     */
    public function __invoke(Request $request, $applicantId)
    {
        if (!$this->checkGuid($request, $applicantId)) {
            return JsonResponse::create(['error' => 'Unable to validate Applicant'], 401);
        }

        $helloSign = [];
        $applicant = Applicant::withPrimaryAddress($applicantId)->first();
        $helloSignClient = new HelloSign();
        $signatureUrl = $helloSignClient->generateSignatureUrlForApplicant($applicant);
        if ($signatureUrl) {
            $helloSign = [
                'signUrl' => $signatureUrl->getSignUrl(),
                'clientId' => $helloSignClient->clientId,
                'testMode' => $signatureUrl->test_mode
            ];
        }
        return JsonResponse::create($helloSign);
    }
}
