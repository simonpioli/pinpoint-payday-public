<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\ApplicantCheckTrait;
use App\Models\Applicant;
use App\Services\Experian\ProveId;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;

class IdentityCheckController extends BaseController
{
    use ApplicantCheckTrait;

    const MAX_ATTEMPTS = 3;

    /** @var ProveId */
    private $proveId;

    public function __construct()
    {
        $this->proveId = new ProveId();
    }

    /**
     * @param Request $request
     * @param $applicantId
     * @return JsonResponse
     */
    public function __invoke(Request $request, $applicantId)
    {
        if (!$this->checkGuid($request, $applicantId)) {
            return JsonResponse::create([
                'Error' => [
                    'Message' =>'Unable to validate Applicant'
                ]
            ], 401);
        }

        $applicant = Applicant::where('id', $applicantId)->with('address')->first();
        /** @var Collection $addresses */
        $addresses = $applicant->address;

        if ($applicant->check_attempts >= $addresses->count()) {
            return JsonResponse::create([
                'Error' => [
                    'Message' => 'Too many attempts'
                ]
            ], 401);
        }

        foreach ($addresses as $address) {
            $xmlResponse = $this->proveId->search($applicant, $address);

            if (!$xmlResponse) {
                return JsonResponse::create([
                    'Error' => [
                        'Message' => 'No response from ID Check'
                    ]
                ], 500);
            }

            if (!empty($xmlResponse->Result->Summary->DecisionMatrix)) { // Decision in response?
                $response = JsonResponse::create([
                    'Decision' => [
                        'Outcome' => $xmlResponse->Result->Summary->DecisionMatrix->Decision->Outcome->__toString(),
                        'Reason' => $xmlResponse->Result->Summary->DecisionMatrix->Decision->Reason->__toString()
                    ]
                ]);

                if ($xmlResponse->attributes()->Type == 'Error') {
                    $response->setStatusCode(500);
                }

                if ($xmlResponse->Result->Summary->DecisionMatrix->Decision->Outcome->__toString() == '1') {
                    $applicant->check_attempts = $applicant->check_attempts + 1;
                    break;
                }
            } else { // Assume error
                return JsonResponse::create([
                    'Error' => [
                        'Code' => $xmlResponse->ErrorCode,
                        'Message' => $xmlResponse->ErrorMessage
                    ]
                ], 500);
            }
        }

        return $response;
    }
}
