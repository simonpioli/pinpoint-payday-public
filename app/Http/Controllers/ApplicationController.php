<?php

namespace App\Http\Controllers;

use App\Events\ApplicationComplete;
use App\Http\Controllers\Traits\ApplicantCheckTrait;
use App\Http\Requests\StoreApplicant;
use App\Http\Requests\StoreApplicantLenders;
use App\Models\Address;
use App\Models\Applicant;
use App\Models\ApplicantLender;
use App\Models\ApplicantLenderQuestion;
use App\Models\Lender;
use App\Models\LenderType;
use App\Models\Question;
use Carbon\Carbon;
use Guid;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Spatie\Referer\Referer;

class ApplicationController extends BaseController
{
    use ApplicantCheckTrait, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @param string $applicantId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function startApplicant(Request $request, $applicantId = null)
    {
        if (!$applicantId && $request->session()->has('applicant')) {
            $request->session()->remove('lenders');
            $request->session()->remove('applicant');
        } elseif ($applicantId != null && !$this->checkGuid($request, $applicantId)) {
            return redirect()->route('application.applicant.get');
        }

        $formData = [];
        if ($request->session()->hasOldInput()) {
            $formData = $request->session()->getOldInput();
        } elseif ($request->session()->has('applicant')) {
            $formData = $request->session()->get('applicant');
        }

        $formYears = [];
        $today = Carbon::now();
        $latestYear = clone $today;
        $latestYear->subYears(18);
        $earliestYear = clone $today;
        $earliestYear->subYears(100);
        for ($year = (int) $latestYear->format('Y'); $year > $earliestYear->format('Y'); $year--) {
            $formYears[] = $year;
        }

        return view('form.start', [
            'applicantId' => $applicantId,
            'formYears' => $formYears,
            'formData' => $formData
        ]);
    }

    /**
     * @param StoreApplicant $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createApplicant(StoreApplicant $request)
    {
        // Validate
        $validatedApplicant = $request->validated();

        // Save Applicant
        $applicantModel = Applicant::firstOrNew([
            'id' => $validatedApplicant['id']
        ]);

        if (!$applicantModel->exists) {
            $guid = Guid::create();
            $applicantModel->id = $guid;
            session(['guid' => $applicantModel->id]);
        }
        $applicantModel->fill($validatedApplicant);

        // Track Referrer and IP Address
        $referer = explode('///', app(Referer::class)->get());

        $campaign = !empty($referer[1]) ? $referer[1] : '';
        $campaign .= !empty($referer[2]) ? ' / ' . $referer[2] : '';
        $applicantModel->source = $referer[0];
        $applicantModel->campaign = $campaign;
        $applicantModel->client_ip = $request->getClientIp();

        $applicantModel->save();

        // Save Addresses

        // If we have addresses from this application, delete them
        if ($applicantModel->address()->count() > 0) {
            $oldAddresses = $applicantModel->address()->get();
            foreach ($oldAddresses as $oldAddress) {
                $oldAddress->delete();
            }
        }
        $primaryAddressModel = new Address($validatedApplicant['primary-address']);
        $primaryAddressModel->primary = true;
        $applicantModel->address()->save($primaryAddressModel);
        if (!empty($validatedApplicant['addresses'])) {
            foreach ($validatedApplicant['addresses'] as $address) {
                $addresses[] = new Address($address);
            }
            $applicantModel->address()->saveMany($addresses);
        }

        session(['applicant' => $validatedApplicant]);

        return redirect()->route(
            'application.identity.get',
            ['applicant' => $applicantModel->id]
        );
    }

    /**
     * @param Request $request
     * @param string $applicantId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function startIdentityCheck(Request $request, $applicantId)
    {
        if (!$this->checkGuid($request, $applicantId)) {
            return redirect()->route('application.applicant.get');
        }

        try {
            $applicant = Applicant::findOrFail($applicantId);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('application.applicant.get');
        }

        return view('form.identity-start', [
            'applicant' => $applicant
        ]);
    }

    public function postIdentity(Request $request)
    {
        $applicantId = $request->input('applicant_id');
        if (!$this->checkGuid($request, $applicantId)) {
            return redirect()->route('application.applicant.get');
        }

        try {
            $applicant = Applicant::findOrFail($applicantId);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('application.applicant.get');
        }

        $applicant->identity_result = $request->input('identity_result');
        $applicant->identity_reason = $request->input('identity_reason');
        $applicant->save();

        if (in_array($applicant->identity_result, ['-1', '0'])) {
            // Fail
            return redirect()->route(
                'application.applicant.get',
                ['applicant' => $applicant->id]
            );
        } else {
            // Pass
            return redirect()->route(
                'application.lenders.get',
                ['applicant' => $applicant->id]
            );
        }
    }

    /**
     * @param Request $request
     * @param string $applicantId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function startLenders(Request $request, $applicantId)
    {
        if (!$this->checkGuid($request, $applicantId)) {
            return redirect()->route('application.applicant.get');
        }

        $applicant = Applicant::find($applicantId);
        $lenderTypes = LenderType::with([
            'lenders' => function ($query) {
                $query->where('active', 1);
            },
            'questions'
        ])->get();
        $questions = Question::all();
        $formData = '';
        if ($request->session()->hasOldInput()) {
            $formData = json_encode($request->session()->getOldInput());
        } elseif ($request->session()->has('lenders')) {
            $formData = json_encode($request->session()->get('lenders'));
        }

        return view('form.lenders', [
            'applicant' => $applicant,
            'lenderTypes' => $lenderTypes,
            'questions' => $questions,
            'formData' => $formData,
        ]);
    }

    /**
     * @param StoreApplicantLenders $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function populateApplicantLenders(StoreApplicantLenders $request)
    {
        $applicantId = $request->session()->get('guid');
        if (!$applicantId) {
            return redirect()->route('application.applicant.get');
        }

        try {
            $applicant = Applicant::findOrFail($applicantId);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('application.applicant.get');
        }

        $formData = $request->validated();

        foreach ($formData['lenders'] as $key => $lender) {
            $applicantLender = ApplicantLender::firstOrNew([
                'applicant_id' => $applicant->id,
                'lender_id' => $lender['lender_id']
            ]);
            $applicantLender->fill($lender);
            $applicant->applicantLender()->save($applicantLender);
            foreach ($lender['questions'] as $qKey => $questionId) {
                $applicantLenderQuestion = ApplicantLenderQuestion::firstOrNew([
                    'applicant_lender_id' => $applicantLender->id,
                    'question_id' => $questionId
                ]);
                $applicantLender->applicantLenderQuestion()->save($applicantLenderQuestion);
            }
        }

        session(['lenders' => $formData]);

        return redirect()->route(
            'application.confirmation.get',
            ['applicant' => $applicant->id]
        );
    }

    /**
     * @param Request $request
     * @param string $applicantId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getConfirmation(Request $request, $applicantId)
    {
        if (!$this->checkGuid($request, $applicantId)) {
            return redirect()->route('application.applicant.get');
        }

        try {
            $applicant = Applicant::findOrFail($applicantId);

            $primaryAddress = $applicant->address()->where('primary', '=', true)->first();
            $addresses = $applicant->address()->where('primary', '=', false)->get();
            $applicantLenders = $applicant->applicantLender()->with(['applicantLenderQuestion.question', 'lender'])->get();
        } catch (ModelNotFoundException $e) {
            return redirect()->route('application.applicant.get');
        }

        return view('form.confirmation', [
            'applicant' => $applicant,
            'primaryAddress' => $primaryAddress,
            'addresses' => $addresses,
            'applicantLenders' => $applicantLenders
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postConfirmation(Request $request)
    {
        $applicantId = $request->session()->get('guid');
        if (!$applicantId) {
            return redirect()->route('application.applicant.get');
        }

        try {
            $applicant = Applicant::findOrFail($applicantId);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('application.applicant.get');
        }

        $formData = $request->all();

        $applicant->confirmed = $formData['confirm'];
        $applicant->save();

        return redirect()->route(
            'application.documents.get',
            ['applicant' => $applicant->id]
        );
    }

    /**
     * @param Request $request
     * @param string $applicantId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function startDocuments(Request $request, $applicantId)
    {
        if (!$this->checkGuid($request, $applicantId)) {
            return redirect()->route('application.applicant.get');
        }

        try {
            $applicant = Applicant::findOrFail($applicantId);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('application.applicant.get');
        }

        return view('form.documents', [
            'applicant' => $applicant,
            'today' => new Carbon
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function signForm(Request $request)
    {
        $applicantId = $request->session()->get('guid');
        if (!$applicantId) {
            return redirect()->route('application.applicant.get');
        }

        try {
            $applicant = Applicant::findOrFail($applicantId);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('application.applicant.get');
        }
        $applicant->signed = true;
        $applicant->update($request->all());

        $applicantLenders = $applicant->applicantLender()->with(['lender'])->get();
        $lenderData = $applicantLenders->map(function ($applicantLender, $key) {
            return [
                'id' => $applicantLender->lender_id,
                'name' => $applicantLender->lender->name,
                'quantity' => (int) $applicantLender->number_loans,
                'item_price' => 1
            ];
        });

        // Trigger event to export XML and upload to Proclaim
        event(new ApplicationComplete($applicant));

        $request->session()->flush();

        return view('form.complete', [
            'lenders' => $lenderData
        ]);
    }
}
