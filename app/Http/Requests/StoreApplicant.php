<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class StoreApplicant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'nullable',
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'date_of_birth.day' => 'required|numeric',
            'date_of_birth.month' => 'required|numeric',
            'date_of_birth.year' => [
                'required',
                'numeric',
            ],
            'date_of_birth' => [
                function ($att, $value, $fail) {
                    $date = Carbon::createFromDate($value['year'], $value['month'], $value['day']);
                    $max = Carbon::now()->subYears(18);
                    if ($date->greaterThan($max)) {
                        $fail('You must be over 18 to apply for a claim.');
                    }
                }
            ],
            'email' => 'required|email',
            'telephone' => 'required',
            'primary-address' => 'required',
            'addresses' => 'nullable',
            'source' => 'nullable|string',
            'campaign' => 'nullable|string',
            'marketing' => 'nullable',
            'iva_bankrupt' => [
                'required',
                'boolean',
                function ($att, $value, $fail) {
                    if ($value === '1') {
                        $fail('Unfortunately we cannot process your claim if you have been declared bankrupt or have been in an IVA.');
                    }
                }
            ],
            't_and_c' => 'required|boolean'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Please select your title',
            'first_name.required'  => 'Please enter your first name',
            'last_name.required' => 'Please enter your last name',
            'date_of_birth.day.required' => 'Please enter the day for your date of birth',
            'date_of_birth.month.required' => 'Please select the month for your date of birth',
            'date_of_birth.year.required' => 'Please enter the year for your date of birth',
            'date_of_birth.day.numeric' => 'Please enter a valid day for your date of birth',
            'date_of_birth.month.numeric' => 'Please select a valid month for your date of birth',
            'date_of_birth.year.numeric' => 'Please enter a valid year for your date of birth',
            'email.required' => 'Please enter your email address',
            'email.email' => 'Please enter a valid email address. E.g. example@emailaddress.com',
            'telephone.required' => 'Please enter your telephone number',
            'primary-address.required' => 'Please enter your address',
            'iva_bankrupt.required' => 'Please answer the question regarding bankruptcy and IVAs',
            'iva_bankrupt.boolean' => 'Please provide a valid answer to the question regarding bankruptcy and IVAs',
            't_and_c.required' => 'Please accept the Terms & Conditions to continue'
        ];
    }
}
