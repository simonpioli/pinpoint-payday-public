<?php

namespace App\Http\Requests;

use App\Models\Lender;
use Illuminate\Foundation\Http\FormRequest;

class StoreApplicantLenders extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'lenders' => 'required|array|min:1'
        ];

        foreach ($this->request->get('lenders') as $key => $lender) {
            $lenderObject = Lender::find($lender['lender_id']);
            $lenderName = $lenderObject->name;
            if ($lenderObject->lender_type_id === 1) {
                $rules['lenders.' . $key . '.first_year'] = 'required';
                $rules['lenders.' . $key . '.last_year'] = 'required';
                $rules['lenders.' . $key . '.number_loans'] = 'required';
                $rules['lenders.' . $key . '.previous_claim'] = [
                    'required',
                    function ($att, $value, $fail) use ($lenderName) {
                        if ($value === '1') {
                            $fail($lenderName . ' - Unfortunately we don’t believe you have a case against this lender. Please remove them from your application to continue.');
                        }
                    }
                ];
                $rules['lenders.' . $key . '.questions'] = 'required|array|min:1';
            } elseif ($lenderObject->lender_type_id === 2) {
                $rules['lenders.' . $key . '.first_year'] = 'required';
                $rules['lenders.' . $key . '.number_loans'] = 'required';
                $rules['lenders.' . $key . '.previous_claim'] = [
                    'required',
                    function ($att, $value, $fail) use ($lenderName) {
                        if ($value === '1') {
                            $fail($lenderName . ' - Unfortunately we don’t believe you have a case against this lender. Please remove them from your application to continue.');
                        }
                    }
                ];
                $rules['lenders.' . $key . '.questions'] = 'required|array|min:1';
            }

        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'lenders.required' => 'Please add at least 1 lender to your application',
            'lenders.array' => 'Please add at least 1 lender to your application',
            'lenders.min:1' => 'Please add at least 1 lender to your application'
        ];

        foreach ($this->request->get('lenders') as $key => $lender) {
            $lenderObject = Lender::find($lender['lender_id']);
            $lenderName = $lenderObject->name;
            if ($lenderObject->lender_type_id === 1) {
                $messages['lenders.' . $key . '.first_year.' . 'required'] = $lenderName . ' - Please supply the year of your first loan with this lender';
                $messages['lenders.' . $key . '.last_year.' . 'required'] = $lenderName . ' - Please supply the year of your last loan with this lender';
                $messages['lenders.' . $key . '.number_loans.' . 'required'] = $lenderName . ' - Please tell us how many loans you took out with this lender';
                $messages['lenders.' . $key . '.previous_claim.' . 'required'] = $lenderName . ' - Please state whether you have previously claimed against this lender';
                $messages['lenders.' . $key . '.questions.' . 'required'] = $lenderName . ' - Please select at least 1 statement that applies to this lender';
                $messages['lenders.' . $key . '.questions.' . 'array'] = $lenderName . ' - Please select at least 1 statement that applies to this lender';
                $messages['lenders.' . $key . '.questions.' . 'min'] = $lenderName . ' - Please select at least 1 statement that applies to this lender';
            } elseif ($lenderObject->lender_type_id === 2) {
                $messages['lenders.' . $key . '.first_year.' . 'required'] = $lenderName . ' - Please supply the year of your loan with this lender';
                $messages['lenders.' . $key . '.number_loans.' . 'required'] = $lenderName . ' - Please tell us how many loans you took out with this lender';
                $messages['lenders.' . $key . '.previous_claim.' . 'required'] = $lenderName . ' - Please state whether you have previously claimed against this lender';
                $messages['lenders.' . $key . '.questions.' . 'required'] = $lenderName . ' - Please select at least 1 statement that applies to this lender';
                $messages['lenders.' . $key . '.questions.' . 'array'] = $lenderName . ' - Please select at least 1 statement that applies to this lender';
                $messages['lenders.' . $key . '.questions.' . 'min'] = $lenderName . ' - Please select at least 1 statement that applies to this lender';
            }
        }

        return $messages;
    }
}
