<?php

namespace App\Referer;

use Illuminate\Http\Request;
use Spatie\Referer\Source;

class UtmCampaign implements Source
{
    /**
     * @param Request $request
     * @return string
     */
    public function getReferer(Request $request):string
    {
        $source = '';
        if ($request->get('utm_campaign') && $request->get('utm_source')) {
            $source = '[' . config('app.url') . ']';
            $source .= '///' . $request->get('utm_source') ?? '';
            $source .= '///' . $request->get('utm_campaign') ?? '';
        }
        return $source;
    }
}
