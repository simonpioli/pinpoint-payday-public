<?php

namespace App\Listeners;

use Illuminate\Http\File;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PDF;

class DocumentGenerationListener
{

    /** @var string */
    protected $localFileLocation = '';

    /** @var string */
    protected $remoteFileLocation = '';

    /** @var array */
    protected $fileNames = [];

    /** @var string */
    protected $emailView = '';

    public function __construct()
    {
        $this->localFileLocation = config('app.documentExportLocation');
        $this->remoteFileLocation = App::environment(['local', 'staging']) ? App::environment() . '/' : '/';
    }

    /**
     * @param string $view
     * @param array $data
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function generateDocument($view, $data = [])
    {
        $config = ['instanceConfigurator' => function ($mpdf) {
            $mpdf->setAutoTopMargin = 'stretch';
            $mpdf->setAutoBottomMargin = 'stretch';
        }];
        $pdf = PDF::loadView($view, $data, [], $config);
        return $pdf->output();
    }

    /**
     * @param Mailable $email
     * @return mixed
     */
    protected function emailDocument($email)
    {
        $toSend = Mail::to(config('app.documentEmailTo'));
        if (config('app.documentEmailCc')) {
            $toSend->cc(config('app.documentEmailCc'));
        }
        return $toSend->send($email);
    }

    /**
     * @param $document
     * @param $fileName
     * @return bool
     */
    protected function writeDocumentToLocalStorage($document, $fileName): bool
    {
        return Storage::put($this->localFileLocation . $fileName, $document);
    }

    protected function writeDocumentToFtpSite($fileName): bool
    {
        return Storage::disk('ftp')->putFileAs(
            $this->remoteFileLocation,
            new File(storage_path('app') . '/' . $this->localFileLocation . $fileName),
            $fileName
        );
    }
}
