<?php

namespace App\Listeners;

use App\Events\ApplicationComplete;
use App\Mail\ApplicantDocuments;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTermsEmail extends DocumentGenerationListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'toc-generator';

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handle the event.
     *
     * @param ApplicationComplete $event
     * @return bool
     */
    public function handle(ApplicationComplete $event)
    {
        $uploaded = false;

        $applicant = $event->applicant;
        $fileName = 'Terms - ' . $applicant->first_name . ' ' . $applicant->last_name . ' - ' . $applicant->signed_date->format('Y-m-d') . '.pdf';
        $document = $this->generateDocument('letters.tac', ['applicant' => $applicant]);
        $saved = $this->writeDocumentToLocalStorage($document, $fileName);

        if ($saved) {
            $uploaded = $this->writeDocumentToFtpSite($fileName);
        }

        return $uploaded;
    }


}
