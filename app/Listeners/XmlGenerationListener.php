<?php

namespace App\Listeners;

use App\Events\ApplicationComplete;
use App\Exceptions\DocumentsNotReady;
use App\Services\ProclaimXml;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class XmlGenerationListener implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ApplicationComplete $event
     * @return bool
     */
    public function handle(ApplicationComplete $event)
    {
        try {
            $xmlGenerator = new ProclaimXml();
            return $xmlGenerator->generateXmlObject($event->applicant);
        } catch (DocumentsNotReady $e) {
            if ($this->attempts() < $this->tries) {
                $this->release(300);
            } else {
                throw $e;
            }
        }

    }

    public function failed(ApplicationComplete $event, $exception)
    {
        dump($exception);
        report($exception);
    }
}
