<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class DropoutReport extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    private $date;

    /**
     * Create a new message instance.
     *
     * @param $report
     * @return void
     */
    public function __construct(Carbon $date, $report)
    {
        $this->date = $date->format('d/m/Y');
        $this->to(config('mail.report_to'));
        $this->subject = 'Dropout Report ' . $this->date;
        $this->attach($report, [
            'mime' => 'text/csv'
        ]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.reports.dropouts')->with([
            'date' => $this->date
        ]);
    }
}
