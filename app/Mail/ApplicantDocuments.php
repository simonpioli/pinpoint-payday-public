<?php

namespace App\Mail;

use App\Models\Applicant;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplicantDocuments extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    public $view;

    /** @var Applicant */
    public $applicant;

    /** @var array */
    public $pdfs = [];

    /**
     * Create a new message instance.
     *
     * @param string $view
     * @param string $subject
     * @param Applicant $applicant
     * @param array $pdfs
     * @return void
     */
    public function __construct($view, $subject, Applicant $applicant, $files = [], $folder = '')
    {
        $this->view = $view;
        $this->subject = $subject;
        $this->applicant = $applicant;
        foreach ($files as $i => $file) {
            $this->pdfs[] = storage_path('app') . '/' . $folder . $file;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        foreach ($this->pdfs as $i => $pdf) {
            $this->attach($pdf, [
                'mime' => 'application/pdf'
            ]);
        }

        return $this->from($this->applicant)
            ->view($this->view);
    }
}
