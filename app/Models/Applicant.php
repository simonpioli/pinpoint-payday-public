<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $title
 * @property string $first_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $email
 * @property string $telephone
 * @property boolean $iva_bankrupt
 * @property boolean $t_and_c
 * @property boolean $marketing
 * @property boolean $confirmed
 * @property integer $identity_result
 * @property string $identity_reason
 * @property integer $check_attempts
 * @property string $signature_request_id
 * @property string $signature_id
 * @property boolean $signed
 * @property string $signed_date
 * @property string $source
 * @property string $campaign
 * @property string $client_ip
 * @property string $created_at
 * @property string $updated_at
 * @property string $exported_at
 * @property string $reported_at
 * @property Address[] $addresses
 * @property ApplicantLender[] $applicantLenders
 */
class Applicant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applicant';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /** @var array */
    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'date_of_birth',
        'email',
        'telephone',
        'iva_bankrupt',
        't_and_c',
        'marketing',
        'confirmed',
        'identity_result',
        'identity_reason',
        'check_attempts',
        'signature_request_id',
        'signature_id',
        'signed',
        'signed_date',
        'source',
        'campaign',
        'client_ip',
        'created_at',
        'updated_at',
        'exported_at',
        'reported_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_of_birth',
        'signed_date',
        'created_at',
        'updated_at',
        'exported_at',
        'reported_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function address()
    {
        return $this->hasMany('App\Models\Address', 'applicant_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantLender()
    {
        return $this->hasMany('App\Models\ApplicantLender', 'applicant_id');
    }

    /**
     * @param array $value
     * @uses Carbon
     */
    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = Carbon::create($value['year'], $value['month'], $value['day']);
    }

    /**
     * @param string $value
     * @uses Carbon
     */
    public function setSignedDateAttribute($value)
    {
        $this->attributes['signed_date'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    /**
     * @param Builder $query
     * @param string $applicantId
     * @return Builder
     */
    public function scopeWithPrimaryAddress($query, $applicantId)
    {
        return $query->where('id', $applicantId)->with(['address' => function ($query) {
            $query->where('primary', 1);
        }]);
    }
}
