<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $applicants_lenders_id
 * @property int $questions_id
 * @property string $created_at
 * @property string $updated_at
 * @property ApplicantLender $applicantsLender
 * @property Question $question
 */
class ApplicantLenderQuestion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applicant_lender_question';

    /**
     * @var array
     */
    protected $fillable = ['applicant_lender_id', 'question_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicantsLender()
    {
        return $this->belongsTo('App\Models\ApplicantLender', 'applicant_lender_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question', 'question_id');
    }
}
