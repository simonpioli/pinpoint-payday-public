<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $applicants_id
 * @property boolean $primary
 * @property string $postcode
 * @property string $flat_no
 * @property string $building_no_name
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $town
 * @property string $county
 * @property string $created_at
 * @property string $updated_at
 * @property Applicant $applicant
 */
class Address extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'address';

    /**
     * Set default values
     *
     * @var array
     */
    protected $attributes = [
        'primary' => false
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'applicant_id',
        'primary',
        'postcode',
        'flat_no',
        'building_no_name',
        'address_line_1',
        'address_line_2',
        'town',
        'county',
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id', 'id');
    }
}
