<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $lender_type_id
 * @property int $reference
 * @property string $name
 * @property string $email
 * @property string $special_notes
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 * @property ApplicantLender[] $applicantLender
 */
class Lender extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lender';

    /**
     * @var array
     */
    protected $fillable = ['reference', 'name', 'email', 'special_notes', 'active', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lenderType()
    {
        return $this->belongsTo('App\Models\LenderType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantLender()
    {
        return $this->hasMany('App\Models\ApplicantLender', 'lender_id');
    }
}
