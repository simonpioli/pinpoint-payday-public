<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $question
 * @property string $created_at
 * @property string $updated_at
 * @property ApplicantLenderQuestion[] $applicantLenderQuestion
 */
class Question extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'question';

    /**
     * @var array
     */
    protected $fillable = ['question', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantLenderQuestion()
    {
        return $this->hasMany('App\Models\ApplicantLenderQuestion', 'questions_id');
    }
}
