<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property Lender[] $lenders
 * @property Question[] $questions
 */
class LenderType extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'lender_type';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lenders()
    {
        return $this->hasMany('App\Models\Lender');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }
}
