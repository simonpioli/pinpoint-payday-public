<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $applicants_id
 * @property int $lenders_id
 * @property string $number_loans
 * @property boolean $previous_claim
 * @property int $first_year
 * @property int $last_year
 * @property boolean $pre_screen
 * @property string $created_at
 * @property string $updated_at
 * @property Applicant $applicant
 * @property Lender $lender
 * @property ApplicantLenderQuestion[] $applicantLenderQuestion
 */
class ApplicantLender extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applicant_lender';

    /**
     * @var array
     */
    protected $fillable = [
        'applicant_id',
        'lender_id',
        'number_loans',
        'previous_claim',
        'first_year',
        'last_year',
        'pre_screen',
        'created_at',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function applicant()
    {
        return $this->belongsTo('App\Models\Applicant', 'applicant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lender()
    {
        return $this->belongsTo('App\Models\Lender', 'lender_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicantLenderQuestion()
    {
        return $this->hasMany('App\Models\ApplicantLenderQuestion', 'applicant_lender_id');
    }
}
