<?php

return [

    /*
     * The key that will be used to remember the referer in the session.
     */
    'session_key' => 'referer',

    /*
     * The sources used to determine the referer.
     */
    'sources' => [
        App\Referer\UtmCampaign::class,
        Spatie\Referer\Sources\RequestHeader::class,
    ],
];
