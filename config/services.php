<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'hellosign' => [
        'key' => env('HELLOSIGN_API_KEY'),
        'clientId' => env('HELLOSIGN_CLIENT_ID'),
        'documents' => [
            'combined' => 'ef359c514ada054e129078d712467d89b00ffdb1',
        ],
    ],

    'experian' => [
        'url' => env('EXPERIAN_API_URL'),
        'searchEndpoint' => env('EXPERIAN_API_SEARCH_ENDPOINT'),
        'secret' => env('EXPERIAN_SECRET'),
        'publicKey' => env('EXPERIAN_PUBLIC_KEY'),
        'username' => env('EXPERIAN_USERNAME'),
        'password' => env('EXPERIAN_PASSWORD'),
    ]
];
