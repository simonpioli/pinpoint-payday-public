<?php

return [
    'mode' => 'utf-8',
    'format' => 'A4',
    'author' => 'PinPoint Solutions Ltd',
    'subject' => '',
    'keywords' => '',
    'creator' => 'Laravel Pdf',
    'display_mode' => 'fullpage',
    'tempDir' => storage_path('app/tmp'),
    'font_path' => base_path('resources/fonts/'),
    'font_data' => [
        'roboto' => [
            'R'  => 'Roboto-Regular.ttf',    // regular font
            'B'  => 'Roboto-Bold.ttf',       // optional: bold font
        ],
        'giveyouglory' => [
            'R' => 'GiveYouGlory.ttf'
        ]
    ],
    'default_font' => 'roboto',
];
